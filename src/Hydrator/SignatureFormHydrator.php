<?php

namespace UnicaenSignature\Hydrator;

use Laminas\Hydrator\HydratorInterface;
use UnicaenSignature\Entity\Db\Signature;
use UnicaenSignature\Entity\Db\SignatureObserver;
use UnicaenSignature\Entity\Db\SignatureRecipient;

class SignatureFormHydrator implements HydratorInterface
{
    private array $marksOptions;

    /**
     * @param array $marksOptions
     */
    public function __construct(array $marksOptions)
    {
        $this->marksOptions = $marksOptions;
    }

    /**
     * @param array $data
     * @param Signature $object
     */
    public function hydrate(array $data, $object)
    {
        // Path
        $path = '';
        if( array_key_exists('path', $data) ){
            $err = intval($data['path']['error']);
            if( $err == 0 ){
                // TODO Passer en paramètre l'emplacement des fichiers temporaire
                $tmpName = '/tmp/'. uniqid('signature-file');
                if( move_uploaded_file($data['path']['tmp_name'], $tmpName) ){
                    $path = $tmpName;
                } else {
                    throw new \Exception('Impossible de déplacer le fichier temporaire');
                }
            } else {
                throw new \Exception("Erreur d'upload  '$err'");
            }
        }

        // Destinataires
        $emails = [];
        $postedEmails = explode(',', $data['emails']);
        foreach ($postedEmails as $email) {
            $sr = new SignatureRecipient();
            $sr->setSignature($object);
            $sr->setStatus(Signature::STATUS_SIGNATURE_DRAFT);
            $sr->setEmail($email);
            $emails[] = $sr;
        }

        // Observateurs
        $observers = [];
        $postedObservers = explode(',', $data['observers']);
        foreach ($postedObservers as $email) {
            $so = new SignatureObserver();
            $so->setSignature($object);
            $so->setEmail($email);
            $observers[] = $so;
        }

        $object->setType($data['type'])
            ->setLabel($data['label'])
            ->setAllSignToComplete($data['allSignToComplete'] == '1')
            ->setDescription($data['description'])
            ->setDocumentPath($path)
            ->setRecipients($emails)
            ->setObservers($observers)
        ;

        return $object;
    }

    /**
     * @param Signature $object
     * @return array
     */
    public function extract(object $object): array
    {
        return [
            'id' => $object->getId(),
            'path' => $object->getDocumentPath(),
            'emails' => $object->getRecipients(),
            'label' => $object->getLabel(),
            'description' => $object->getDescription(),
        ];
    }
}