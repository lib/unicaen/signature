<?php

namespace UnicaenSignature\Strategy\Notification;

use Monolog\Logger;
use UnicaenSignature\Strategy\Notification\INotificationStrategy;

class NotificationLogDebugStrategy implements INotificationStrategy
{
    private $logger;

    /**
     * @param Logger $logger
     */
    public function __construct($logger)
    {
        $this->logger = $logger;
    }


    public function sendNotification(string $email, string $subject, string $message): void
    {
        $this->logger->debug("[Notification Debug] subject:'$subject' --- '$message'");
    }
}