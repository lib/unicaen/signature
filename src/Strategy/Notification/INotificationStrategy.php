<?php

namespace UnicaenSignature\Strategy\Notification;

use Signature;

interface INotificationStrategy
{
    public function sendNotification(string $email, string $subject, string $message): void;
}