<?php

namespace UnicaenSignature\Strategy\Notification;

use Psr\Container\ContainerInterface;
use UnicaenSignature\Service\LoggerService;

class NotificationLogDebugStrategyFactory
{
    public function __invoke(ContainerInterface $container): NotificationLogDebugStrategy
    {
        $s = new NotificationLogDebugStrategy($container->get(LoggerService::class));
        return $s;
    }
}