<?php

namespace UnicaenSignature\Strategy\Letterfile;

use Psr\Container\ContainerInterface;
use UnicaenSignature\Entity\Db\Signature;
use UnicaenSignature\Service\LoggerService;
use UnicaenSignature\Service\SignatureConfigurationService;
use UnicaenSignature\Strategy\Letterfile\Tools\ResponseInfos;
use UnicaenSignature\Exception\SignatureException;

interface ILetterfileStrategy
{
    const STATUS_REFUSED = "refused";
    const STATUS_COMPLETED = "completed";
    const STATUS_PENDING = "pending";
    const STATUS_DELETED = "deleted";

    /**
     * Supression de la procédure du signature.
     *
     * @param Signature $signature
     * @return void
     * @throws SignatureException
     */
    public function deleteSignature(Signature $signature): void;

    /**
     * Retourne la dernière version du document.
     *
     * @param Signature $signature
     * @return void
     * @throws SignatureException
     */
    public function getLastDocumentSignature(Signature $signature): string;

    /**
     * Retourne le status de la signature (Code de statut normalisé).
     *
     * @param Signature $signature
     * @return string
     * @throws SignatureException
     */
    public function getSignatureStatus(Signature $signature): string;

    /**
     * Retourne les informations détaillées sur l'état d'avancement de la signature.
     * format [
     *   'status' => 'Status normalizé',
     *   'status' => 'Status normalizé',
     * ]
     *
     * @param Signature $signature
     * @return null|array
     * @throws SignatureException
     */
    public function getSignatureInfos(Signature $signature): ResponseInfos;



    /**
     * Envoi de la signature au parapheur.
     *
     * @param Signature $signature
     * @return void
     * @throws SignatureException
     */
    public function sendSignature(Signature $signature): void;


    /**
     * Retourne les niveaux de signature pris en charge par le parapheur.
     *
     * @return string[]
     */
    public function getLevels(): array;

    /**
     * Retourne la CLEF UNIQUE du parapheur
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Retourne le nom d'usage du parpheur.
     *
     * @return string
     */
    public function getLabel(): string;

    /**
     * Retourne la description du parapheur.
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Retourne TRUE si c'est le parapheur par défaut.
     *
     * @return bool
     */
    public function isDefault(): bool;

    /**
     * Méthode pour vérifier le bon fonctionnement du parapheur.
     *
     * @return void
     */
    public function checkAccess(): void;

    /**
     * Configuration issue du fichier signature.local.php
     *
     * @param array $config
     * @return $this
     */
    public function setConfig(array $config): self;

    ///////////////////////////////////////////////////////////////////// LOGGER (Optionnel si vous ne l'utilisez pas)

    /**
     * @param LoggerService $loggerService
     * @return void
     */
    public function setLoggerService(LoggerService $loggerService): void;

    /**
     * @param ContainerInterface $sc
     * @return void
     */
    public function setServiceContainer(ContainerInterface $sc): void;

    /**
     * @param SignatureConfigurationService $signatureConfigurationService
     * @return void
     */
    public function setSignatureConfigurationService(SignatureConfigurationService $signatureConfigurationService): void;
}