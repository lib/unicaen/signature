<?php

namespace UnicaenSignature\Strategy\Letterfile\Tools;

use UnicaenSignature\Entity\Db\Signature;

/**
 *
 */
class ResponseInfos
{
    // Clef utilisée pour ranger les commentaires sans auteurs
    const COMMENT_NO_AUTHOR = 'unknow';

    private string $status_remote;
    private ?int $status = null;

    private bool $allSignToComplete;

    private bool $forceRefuse = false;

    private ?\DateTime $dateDone = null;

    /**
     * Emails des destinataires impliqués
     *
     * @var string[]
     */
    private array $recipients;

    /**
     * Emails des destinataires qui ont répondu
     *
     * @var string[]
     */
    private array $recipients_giving_an_answer;

    /**
     * Emails des destinataires qui ont validé
     *
     * @var string[]
     */
    private array $recipients_accepted;

    /**
     * Date de signature envoyée par le parapheur (EMAIL => DateTime)
     * @var array
     */
    private array $recipients_response_dates;

    /**
     * Emails des destinataires qui ont rejeté
     *
     * @var string[]
     */
    private array $recipients_refused;

    /**
     * Emails des destinataires qui n'ont pas répondu
     *
     * @var string[]
     */
    private array $recipients_none;

    /**
     * Emails des destinataires qui n'ont pas répondu
     *
     * @var string[]
     */
    private array $comments;

    /**
     * Messages de refus
     * @var array
     */
    private array $refusedComments;

    public function __construct()
    {
        $this->allSignToComplete = true;
        $this->recipients = array();
        $this->recipients_giving_an_answer = array();
        $this->recipients_accepted = array();
        $this->recipients_response_dates = array();
        $this->recipients_refused = array();
        $this->recipients_none = array();
        $this->comments = array();
        $this->refusedComments = array();
    }

    /**
     * @param \DateTime|null $dateDone
     * @return $this
     */
    public function setDateDone(?\DateTime $dateDone): self
    {
        $this->dateDone = $dateDone;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatusRemote(): string
    {
        return $this->status_remote;
    }

    /**
     * @return bool
     */
    public function isForceRefuse(): bool
    {
        return $this->forceRefuse;
    }

    /**
     * @param bool $forceRefuse
     * @return ResponseInfos
     */
    public function setForceRefuse(bool $forceRefuse): self
    {
        $this->forceRefuse = $forceRefuse;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAllSignToComplete(): bool
    {
        return $this->allSignToComplete;
    }

    /**
     * @return string[]
     */
    public function getRecipients(): array
    {
        return $this->recipients;
    }

    /**
     * @return string[]
     */
    public function getRecipientsGivingAnAnswer(): array
    {
        return $this->recipients_giving_an_answer;
    }

    /**
     * @return string[]
     */
    public function getRecipientsAccepted(): array
    {
        return $this->recipients_accepted;
    }

    /**
     * @return string[]
     */
    public function getRecipientsRefused(): array
    {
        return $this->recipients_refused;
    }

    /**
     * @return string[]
     */
    public function getRecipientsNone(): array
    {
        return $this->recipients_none;
    }

    /**
     * @return string[]
     */
    public function getComments(): array
    {
        return $this->comments;
    }

    /**
     * @param int $status
     * @return ResponseInfos
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param string $status_remote
     * @return ResponseInfos
     */
    public function setStatusRemote(string $status_remote): self
    {
        $this->status_remote = $status_remote;
        return $this;
    }

    /**
     * @param bool $allSignToComplete
     * @return ResponseInfos
     */
    public function setAllSignToComplete(bool $allSignToComplete): self
    {
        $this->allSignToComplete = $allSignToComplete;
        return $this;
    }

    public function addRecipient(string $email): void
    {
        if (!in_array($email, $this->recipients)) {
            $this->recipients[] = $email;
        }
    }

    public function addAccepted(string $email, ?string $comment = null, \DateTime $date = new \DateTime()): void
    {
        $this->addRecipient($email);
        if (!in_array($email, $this->recipients_accepted)) {
            $this->recipients_accepted[] = $email;
            $this->recipients_response_dates[$email] = $date;
            if ($comment) {
                $this->addComment($email, $comment, $date);
            }
        }
    }

    public function addRefused(string $email, ?string $comment = null, \DateTime $date = new \DateTime()): void
    {
        $this->addRecipient($email);
        if (!in_array($email, $this->recipients_refused)) {
            $this->recipients_refused[] = $email;
            $this->recipients_response_dates[$email] = $date;
            if ($comment !== null) {
                $this->addCommentRefused($email, $comment);
            }
        }
    }

    public function addNone(string $email): void
    {
        $this->addRecipient($email);
        if (!in_array($email, $this->recipients_none)) {
            $this->recipients_none[] = $email;
        }
    }

    public function addComment(?string $email, string $comment, ?\DateTime $date = null): void
    {
        $this->comment($email, $date, $comment, $this->comments);
    }

    public function addCommentRefused(?string $email, string $comment, ?\DateTime $date = null): void
    {
        $this->comment($email, $date, $comment, $this->refusedComments);
    }

    /**
     * @param string|null $email
     * @param \DateTime|null $date
     * @param string $comment
     * @param array $comments
     * @return void
     */
    private function comment(?string $email, ?\DateTime $date, string $comment, array &$comments): void
    {
        $key = $email !== null ? $email : self::COMMENT_NO_AUTHOR;
        $date = $date !== null ? $date : new \DateTime();

        $dateStr = $date->format('Y-m-d H:i:s');

        if (!array_key_exists($key, $comments)) {
            $comments[$key] = [];
        }

        $comments[$key][] = [
            'date' => $dateStr,
            'text' => $comment
        ];
    }

    /**
     * Applati un tableau de commentaire pour retourner une chaine
     *
     * @param array $comments
     * @return string|null
     */
    private function flatComments(array $comments): ?string
    {
        if (count($comments) === 0) {
            return null;
        }
        else {
            $separator = ' / ';
            $out = [];
            foreach ($comments as $comment) {
                $out[] = $comment['text'];
            }
            return implode($separator, $out);
        }
    }

    /**
     * @param string $email
     * @return array|null
     */
    public function getCommentsBy(string $email = self::COMMENT_NO_AUTHOR): ?array
    {
        return array_key_exists($email, $this->comments) ? $this->comments[$email] : null;
    }

    public function getCommentsByFlat(string $email = self::COMMENT_NO_AUTHOR): ?string
    {
        $comments = $this->getCommentsBy($email);
        if ($comments !== null) {
            return $this->flatComments($comments);
        }
        return null;
    }

    public function getRefusedComments(): array
    {
        return $this->refusedComments;
    }

    /**
     * Retourne la liste des commentaires de refus de la personne.
     *
     * @param string $mailAuthor
     * @return array|null
     */
    public function getRefusedCommentsBy(string $mailAuthor = self::COMMENT_NO_AUTHOR): ?array
    {
        if (array_key_exists($mailAuthor, $this->refusedComments)) {
            return $this->refusedComments[$mailAuthor];
        }
        else {
            return null;
        }
    }

    /**
     * Retourne la liste des commentaires de la personne sous la forme d'une chaine.
     *
     * @param string $mailAuthor
     * @return string|null
     */
    public function getRefusedCommentsByFlat(string $mailAuthor = self::COMMENT_NO_AUTHOR): ?string
    {
        $comments = $this->getRefusedCommentsBy($mailAuthor);
        if ($comments !== null) {
            return $this->flatComments($comments);
        }
        return null;
    }

    public function getRefusedCommentsFlat(): ?string
    {
        $out = [];
        foreach ($this->getRefusedComments() as $comments) {
            foreach ($comments as $comment) {
                $out[] = $comment;
            }
        }
        return $this->flatComments($out);
    }


    public function isAccepted(string $email): bool
    {
        return in_array($email, $this->recipients_accepted);
    }

    public function isRefused(string $email): bool
    {
        return in_array($email, $this->recipients_refused);
    }

    public function isNone(string $email): bool
    {
        return in_array($email, $this->recipients_none);
    }


    public function getDateDone(?string $email = null, bool $throw = true): ?\DateTime
    {
        $dateDone = null;
        if ($email === null) {
            $dateDone = $this->dateDone;
        }
        elseif (array_key_exists($email, $this->recipients_response_dates)) {
            $dateDone = $this->recipients_response_dates[$email];
        }

        if ($dateDone === null && $throw) {
            throw new \Exception('Unknown date done');
        }

        return $dateDone;
    }

    public function getStatus(): int
    {
        if ($this->status !== null && in_array($this->status, [
                Signature::STATUS_SIGNATURE_DELETE,
                Signature::STATUS_SIGNATURE_FULLDELETE,
                Signature::STATUS_SIGNATURE_CANCEL
            ])) {
            return $this->status;
        }
        if ($this->forceRefuse || count($this->recipients_refused)) {
            return Signature::STATUS_SIGNATURE_REJECT;
        }

        if ($this->isAllSignToComplete()) {
            if (count($this->recipients) == count($this->recipients_accepted)) {
                return Signature::STATUS_SIGNATURE_SIGNED;
            }
        }
        else {
            if (count($this->recipients_accepted) > 0) {
                return Signature::STATUS_SIGNATURE_SIGNED;
            }
        }
        return Signature::STATUS_SIGNATURE_WAIT;
    }

    public function toArray(): array
    {
        // Return
        $out = [
            "status"           => $this->getStatus(),
            "status_text"      => Signature::getStatusLabel($this->getStatus()),
            "status_remote"    => $this->status_remote,
            "accepted"         => $this->recipients_accepted,
            "response_date"    => $this->recipients_response_dates,
            "refused"          => $this->recipients_refused,
            "none"             => $this->recipients_none,
            "recipients"       => $this->recipients,
            "comments"         => $this->comments,
            "comments_refused" => $this->refusedComments,
        ];
        return $out;
    }

    public function isWaiting(): bool
    {
        return $this->getStatus() == Signature::STATUS_SIGNATURE_WAIT;
    }
}