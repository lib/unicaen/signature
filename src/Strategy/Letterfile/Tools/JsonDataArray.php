<?php

namespace UnicaenSignature\Strategy\Letterfile\Tools;

use UnicaenSignature\Exception\SignatureDataMissingException;

class JsonDataArray
{
    /**
     * @param array $json
     * @param string $name
     * @return mixed
     * @throws SignatureDataMissingException
     */
    protected static function getRequiredEntry(array $json, string $name): mixed
    {
        if (!array_key_exists($name, $json)) {
            throw new SignatureDataMissingException("Entrée '$name' requise est absente du retour des données.");
        }
        return $json[$name];
    }

    /**
     * @param array $json
     * @param string $name
     * @param mixed|null $defaultValue
     * @return mixed
     */
    protected static function getOptionalEntry(array $json, string $name, mixed $defaultValue = null): mixed
    {
        if (!array_key_exists($name, $json)) {
            return $defaultValue;
        }
        return $json[$name];
    }

    /**
     * @param array $json
     * @param string $name
     * @return \DateTime|null
     * @throws SignatureDataMissingException
     */
    protected static function getDate( array $json, string $name ): ?\DateTime
    {
        $isoDate = self::getRequiredEntry($json, $name);
        if( $isoDate === null ){
            return null;
        } else {
            return new \DateTime($isoDate);
        }
    }
}