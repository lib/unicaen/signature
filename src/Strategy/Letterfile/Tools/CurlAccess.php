<?php

namespace UnicaenSignature\Strategy\Letterfile\Tools;

use Monolog\Logger;
use UnicaenSignature\Exception\SignatureCurlException;
use UnicaenSignature\Service\LoggerService;

class CurlAccess
{

    const ERROR_HTTP_400 = "Mauvaise requête";
    private ?\CurlHandle $resource;
    private LoggerService $logger;

    private string $url;

    private ?string $archivePath;

    /**
     * @param LoggerService $logger
     * @param string|null $archivePath Emplacement où archiver les retours
     */
    public function __construct(LoggerService $logger, ?string $archivePath = null)
    {
        $this->logger = $logger;
        $this->archivePath = $archivePath;
    }


    protected function init(): void
    {
        $this->resource = curl_init($this->url);
    }

    protected function archive($result, string $url, ?array $send = null) :void {
        if( $this->archivePath ){
            $filename = sprintf('exchange_%s.txt', date('Y-m-d_H-i-s'));
            $location = $this->archivePath . DIRECTORY_SEPARATOR . $filename;
            $data = $result === null ? 'Aucune donnée reçue' : $result;
            $content = "URL ------------------------------ '$url'\n\n";
            $content .= "SEND ---------------------------\n" . ($send ? json_encode($send, JSON_PRETTY_PRINT) : "Rien envoyé") . "\n\n";
            $content .= "RESULT ---------------------------\n" . $data . "\n\n";
            if(!@file_put_contents($location, $content)){
                $this->logger->error("CURL [Exchange] can't write '$location'");
            }
        }
    }

    public function delete($url, array $posted):array{
        $this->logger->debug("CURL [delete] $url");

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $posted);
        curl_setopt($curl, CURLOPT_USERAGENT,'Opera/9.80 (Windows NT 6.2; Win64; x64) Presto/2.12.388 Version/12.15');
        curl_setopt($curl, CURLOPT_HTTPHEADER,array(
                             'User-Agent: Opera/9.80 (Windows NT 6.2; Win64; x64) Presto/2.12.388 Version/12.15','Referer: http://someaddress.tld','Content-Type: multipart/form-data')
        );
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // stop verifying certificate
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); // if any redirection after upload

        $result=curl_exec ($curl);
        $this->archive($result, $url, $posted);

        $curl_error = curl_error($curl);

        if ($curl_error) {
            $this->logger->error("[curl_exec ERROR] : " . $curl_error);
            throw new SignatureCurlException("Erreur CURL");
        }
        $info = curl_getinfo($curl);
        $this->logger->debug(print_r($info, true));
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close ($curl);

        if ($httpcode != 200) {
            switch ($httpcode) {
                case "400":
                    $msg = "Ressource missing";
                    break;
                case "500":
                    $msg = "Internal Server Error";
                    break;
                default:
                    $msg = "Unknow error";
            }
            if( $result ){
                $msg .= " ($httpcode) : " . $result;
            }
            throw new SignatureCurlException($msg);
        }

        return [
            'code' =>$httpcode,
            'result' => $result
        ];
    }

    public function post( $url, $post, $documents ):array
    {
        $this->logger->debug("CURL [post] $url");
        $posted = $post;
        foreach ($documents as $key=>$path) {
            $cfile = curl_file_create($path,'application/pdf',$key);
            $posted[$key] = $cfile;
        }
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$url);
        curl_setopt($curl, CURLOPT_USERAGENT,'Opera/9.80 (Windows NT 6.2; Win64; x64) Presto/2.12.388 Version/12.15');
        curl_setopt($curl, CURLOPT_HTTPHEADER,array(
            'User-Agent: Opera/9.80 (Windows NT 6.2; Win64; x64) Presto/2.12.388 Version/12.15','Referer: http://someaddress.tld','Content-Type: multipart/form-data')
        );
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // stop verifying certificate
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true); // enable posting
        curl_setopt($curl, CURLOPT_POSTFIELDS, $posted); // post images
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); // if any redirection after upload

        $result=curl_exec ($curl);
        $this->archive($result, $url, $posted);

        $curl_error = curl_error($curl);
        if ($curl_error) {
            $this->logger->error("[curl_exec ERROR] POST $url : " . $curl_error);
            throw new \Exception("Erreur CURL");
        }

        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($httpcode != 200) {
            switch ($httpcode) {
                case "400":
                    throw new  SignatureCurlException(
                        "[curl_exec ERROR](post '$url') : [$httpcode] " . self::ERROR_HTTP_400
                    );
                case "500":
                    $msg = "Internal Server Error";
                    if( $result ){
                        $jsonResponse = json_decode($result, true);
                        if( $jsonResponse && array_key_exists('trace', $jsonResponse) ){
                            $msg .= " : " . $jsonResponse['trace'];
                        }
                    }
                    throw new  SignatureCurlException("[curl_exec ERROR](post '$url') : [$httpcode] $msg");
                default:
                    throw new  SignatureCurlException("[curl_exec ERROR](post '$url') : [$httpcode] Ressource indisponible");
            }
        }

        curl_close ($curl);
        return [
          'code' =>$httpcode,
          'result' => $result
        ];
    }

    public function get(string $url): array
    {
        $this->logger->debug("CURL [get] $url");
        $return = [];
        $opt = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1
        ];

        $this->logger->debug("curl_init");
        $ch = curl_init();
        $this->logger->debug("curl_opt : " . print_r($opt, true));
        curl_setopt_array($ch, $opt);

        $this->logger->debug("curl_exec");
        $return = curl_exec($ch);
        $this->archive($return, $url);


        $curl_error = curl_error($ch);
        if ($curl_error) {
            $this->logger->error("[curl_exec ERROR] GET $url : " . $curl_error);
            throw new \Exception("Erreur CURL");
        }

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($httpcode != 200) {
            switch ($httpcode) {
                case "400":
                    throw new  SignatureCurlException(
                        "[curl_exec ERROR](get '$url') : [$httpcode] " . self::ERROR_HTTP_400
                    );
                default:
                    throw new  SignatureCurlException("[curl_exec ERROR](get '$url') : [$httpcode] Ressource indisponible");
            }
        }

        if ($return == false) {
            $this->logger->error("[curl_exec ERROR] GET $url : EMPTY RESULT");
            throw new \Exception("Pas de retour");
        }

        return ['return' => $return];
    }
}