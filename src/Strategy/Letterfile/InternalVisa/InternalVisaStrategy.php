<?php

namespace UnicaenSignature\Strategy\Letterfile\InternalVisa;

use UnicaenSignature\Entity\Db\Signature;
use UnicaenSignature\Entity\Db\SignatureRecipient;
use UnicaenSignature\Service\LoggerServiceAwareTrait;
use UnicaenSignature\Service\ServiceContainerAwareTrait;
use UnicaenSignature\Service\SignatureConfigurationServiceAwareTrait;
use UnicaenSignature\Strategy\Letterfile\ILetterfileStrategy;
use UnicaenSignature\Strategy\Letterfile\Tools\ResponseInfos;

class InternalVisaStrategy implements ILetterfileStrategy
{
    use ServiceContainerAwareTrait,
        SignatureConfigurationServiceAwareTrait,
        LoggerServiceAwareTrait
        ;

    private array $config;

    public function isDefault(): bool
    {
        return $this->config['default'] == true;
    }

    public function deleteSignature(Signature $signature): void
    {
        // TODO: Implement deleteSignature() method.
    }

    public function getLastDocumentSignature(Signature $signature): string
    {
        $docPath = $this->getSignatureConfigurationService()->getDocumentsLocation()
            . DIRECTORY_SEPARATOR
            . $signature->getDocumentPath();

        return file_get_contents($docPath);
    }

    public function getSignatureStatus(Signature $signature): string
    {
        $infos = $this->getSignatureInfos($signature);
        return $infos->getStatus();
    }

    public function getSignatureInfos(Signature $signature): ResponseInfos
    {
        $response = new ResponseInfos();
        $response->setStatus($signature->getStatus());
        $response->setStatusRemote($signature->getStatus());
        $response->setAllSignToComplete($signature->getAllSignToComplete());

        foreach ($signature->getRecipients() as $recipient) {
            $email = $recipient->getEmail();
            $response->addRecipient($email);
            if($recipient->getStatus() == Signature::STATUS_SIGNATURE_REJECT){
                $refuseText = $recipient->getInformations();
                // Récupération du message de refus
                $response->addRefused($email);
                $response->addCommentRefused($email, $refuseText);
                $response->setForceRefuse(true);
            }elseif($recipient->getStatus() == Signature::STATUS_SIGNATURE_SIGNED){
                $response->addAccepted($email);
            }else{
                $response->addNone($email);
            }
        }
        if($response->isForceRefuse()){
            $response->setStatus(Signature::STATUS_SIGNATURE_REJECT);
        }
        return $response;
    }


    public function sendSignature(Signature $signature): void
    {
        /** @var SignatureRecipient $recipient */
        foreach ($signature->getRecipients() as $recipient) {
            $hash = md5(uniqid("", true)) . "-" . md5(uniqid("", true));
            $recipient->setKeyAccess($hash)
                ->setUrlDocument($this->getUrlVisa())
                ->setDateUpdate(new \DateTime())
                ->setStatus(Signature::STATUS_SIGNATURE_WAIT)
            ;
        }
        $this->getServiceContainer()->get('doctrine.entitymanager.orm_default')->flush();
    }

    public function getUrlVisa(): string
    {
        return $this->config['url_visa'];
    }

    public function getLevels(): array
    {
        return $this->config['levels'];
    }

    public function getName(): string
    {
        return $this->config['name'];
    }

    public function getLabel(): string
    {
        return $this->config['label'];
    }

    public function getDescription(): string
    {
        return $this->config['description'];
    }

    public function checkAccess(): void
    {
        // nothing to o here
    }

    public function setConfig(array $config): ILetterfileStrategy
    {
        $this->config = $config;
        return $this;
    }
}