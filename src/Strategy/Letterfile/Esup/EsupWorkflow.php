<?php

namespace UnicaenSignature\Strategy\Letterfile\Esup;

use UnicaenSignature\Exception\SignatureDataMissingException;
use UnicaenSignature\Strategy\Letterfile\Tools\JsonDataArray;

class EsupWorkflow extends JsonDataArray
{
    private array $source;
    private int $id;
    private ?string $description;
    private ?string $name;
    private array $steps;

    protected function __construct()
    {
        $this->steps = [];
    }

    /**
     * @param array $arrayFromJson
     * @return EsupWorkflow[]
     * @throws SignatureDataMissingException
     */
    public static function getWorkflowsFromJsonArray( array $arrayFromJson ) :array
    {
        $out = [];
        foreach ($arrayFromJson as $j){
            $out[] = self::getWorkflowFromArray($j);
        }
        return $out;
    }

    /**
     * @param array $arrayFromJson
     * @return static
     * @throws SignatureDataMissingException
     */
    public static function getWorkflowFromArray( array $arrayFromJson ) :self
    {
        $dt = new EsupWorkflow();
        $dt->id = self::getRequiredEntry($arrayFromJson, 'id');
        $dt->description = self::getRequiredEntry($arrayFromJson, 'description');
        $dt->name = self::getRequiredEntry($arrayFromJson, 'name');
        $dt->source = $arrayFromJson;
        foreach (self::getRequiredEntry($arrayFromJson, 'workflowSteps') as $step) {
            $dt->steps[] = EsupWorkflowStep::extractFromJsonObject($step);
        }
        return $dt;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @return array
     */
    public function getSource(): array
    {
        return $this->source;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return EsupWorkflowStep[]
     */
    public function getSteps(): array
    {
        return $this->steps;
    }

    public function countSteps() :int
    {
        return count($this->steps);
    }
}