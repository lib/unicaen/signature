<?php

namespace UnicaenSignature\Strategy\Letterfile\Esup\Response;

use UnicaenSignature\Entity\Db\Signature;
use UnicaenSignature\Strategy\Letterfile\Tools\ResponseInfos;

/**
 * Cette classe permet de normalizer les échanges avec ESUP pour obtenir des ResponseInfos
 * utilisable par UnicaenSignature.
 */
class SignRequestInfo
{
    public static function getFromSignRequestGetId($returned) :ResponseInfos
    {
        $responseInfos = new ResponseInfos();

        // On regarde si une personne refuse
        if (array_key_exists('recipientHasSigned', $returned)) {
            foreach ($returned['recipientHasSigned'] as $item) {
                if (array_key_exists('actionType', $item) && $item['actionType'] == 'refused') {
                    $responseInfos->setForceRefuse(true);
                }
            }
        }

        // La clef auditTrail permet d'obtenir les personnes qui ont signées
        if ($returned['auditTrail'] != null && array_key_exists('auditSteps', $returned['auditTrail'])) {
            foreach ($returned['auditTrail']['auditSteps'] as $auditStep) {
                $email = $auditStep['email'];
                $date = $auditStep['timeStampDate'];
                $dateAccepted = new \DateTime($date);
                $responseInfos->addAccepted($email, null, $dateAccepted);
            }
        }

        // Récupération des emails qui ont réagi
        if (array_key_exists('parentSignBook', $returned)
            && array_key_exists('liveWorkflow', $returned['parentSignBook'])
            && array_key_exists('currentStep', $returned['parentSignBook']['liveWorkflow'])
            && array_key_exists('recipients', $returned['parentSignBook']['liveWorkflow']['currentStep'])
        ) {
            $allSignToComplete = $returned['parentSignBook']['liveWorkflow']['currentStep']['allSignToComplete'];
            $responseInfos->setAllSignToComplete($allSignToComplete);

            foreach ($returned['parentSignBook']['liveWorkflow']['currentStep']['recipients'] as $recipient) {
                $signed = $recipient['signed'];
                $email = $recipient['user']['email'];
                $comment = null;
                $responseInfos->addRecipient($email);
                if ($signed) {
                    if (!$responseInfos->isAccepted($email)) {
                        $responseInfos->addRefused($email, $comment);
                    }
                }
                else {
                    $responseInfos->addNone($email);
                }
            }

            if( array_key_exists('endDate', $returned['parentSignBook']) ) {
                if( !$responseInfos->isWaiting() ){
                    $endDate = new \DateTime($returned['parentSignBook']['endDate']);
                    $responseInfos->setDateDone($endDate);
                }
            }
        }


        // Commentaires
        foreach ($returned['comments'] as $com) {
            $dateCreated = new \DateTime($com['createDate']);
            $text = $com['text'];

            // Dans ESUP, pas d'infos sur l'auteur du commentaire/postit
            $author = ResponseInfos::COMMENT_NO_AUTHOR;
            $isRefused = $com['postitColor'] == '#FF7EB9';
            if( $isRefused ){
                $responseInfos->addCommentRefused($author, $text, $dateCreated);
            } else {
                $responseInfos->addComment($author, $text, $dateCreated);
            }
        }

        // Status
        $status_remote = $returned['status'];
        $responseInfos->setStatusRemote($status_remote);



        // Suppression dans le parapheur
        if ($status_remote == 'deleted') {
            $responseInfos->setStatus(Signature::STATUS_SIGNATURE_DELETE);
        }
        return $responseInfos;
    }
}