<?php
namespace UnicaenSignature\Strategy\Letterfile\Esup\Response;

use UnicaenSignature\Strategy\Letterfile\ILetterfileStrategy;

class SignRequestStatus
{
    /**
     * Tableau de correspondance statut reçu <> statut d'usage.
     *
     * @return string[]
     */
    protected static function inToOut() :array
    {
        return [
            'pending' => ILetterfileStrategy::STATUS_PENDING,
            'completed' => ILetterfileStrategy::STATUS_COMPLETED,
            'deleted' => ILetterfileStrategy::STATUS_DELETED,
            'refused' => ILetterfileStrategy::STATUS_REFUSED,
        ];
    }

    /**
     * Retour normalisé du statut.
     *
     * @param $responseCurl
     * @return string
     */
    public static function normalizeResponse($responseCurl) :string
    {
        $inOut = self::inToOut();
        $statusInResponse = $responseCurl['return'];
        if( !in_array($statusInResponse, $inOut) ){
            return 'unknow';
        } else {
            return $inOut[$statusInResponse];
        }
    }
}