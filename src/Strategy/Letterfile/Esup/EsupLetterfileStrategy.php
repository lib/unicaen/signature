<?php

namespace UnicaenSignature\Strategy\Letterfile\Esup;

use Exception;
use UnicaenSignature\Entity\Db\Signature;
use UnicaenSignature\Exception\SignatureCurlException;
use UnicaenSignature\Exception\SignatureDataMissingException;
use UnicaenSignature\Service\LoggerServiceAwareTrait;
use UnicaenSignature\Service\ServiceContainerAwareTrait;
use UnicaenSignature\Service\SignatureConfigurationServiceAwareTrait;
use UnicaenSignature\Strategy\Letterfile\Esup\Response\SignRequestInfo;
use UnicaenSignature\Strategy\Letterfile\ILetterfileStrategy;
use UnicaenSignature\Strategy\Letterfile\ISignRequest;
use UnicaenSignature\Strategy\Letterfile\Tools\CurlAccess;
use UnicaenSignature\Strategy\Letterfile\Tools\ResponseInfos;
use UnicaenSignature\Exception\SignatureException;

/**
 * Système de signature ESUP
 */
class EsupLetterfileStrategy implements ILetterfileStrategy
{

    use ServiceContainerAwareTrait,
        LoggerServiceAwareTrait,
        SignatureConfigurationServiceAwareTrait;

    // Generic
    private array $config;

    private ?CurlAccess $curlAccess = null;

    public ?SignatureConfigurationService $configurationService;

    // URLs STANDARDS ESUP
    const WS_WORKFLOW_POST_ID_NEW = '/ws/workflows/%s/new';
    const WS_WORKFLOW_GET_ID = '/ws/workflows/%s';
    const WS_WORKFLOW_GET_ALL = '/ws/workflows/all';

    const WS_SIGNREQUESTS_POST_NEW = '/ws/signrequests/new';
    const WS_SIGNREQUESTS_GET_ID = '/ws/signrequests/%s';
    const WS_SIGNREQUESTS_DELETE_ID = '/ws/signrequests/%s';
    const WS_SIGNREQUESTS_GET_STATUS_ID = '/ws/signrequests/status/%s';
    const WS_SIGNREQUESTS_GET_ALL = '/ws/signrequests/all';
    const WS_SIGNREQUESTS_GET_LAST_FILE = '/ws/signrequests/get-last-file/%s';


    public function setConfig(array $config): self
    {
        $this->config = $config;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->config['description'];
    }

    public function getName(): string
    {
        return $this->config['name'];
    }

    public function getLabel(): string
    {
        return $this->config['label'];
    }

    public function getLevels(): array
    {
        return $this->config['levels'];
    }

    public function isDefault(): bool
    {
        return $this->config['default'];
    }

    public function getArchiveExchange(): ?string
    {
        if( array_key_exists('archive_exchange', $this->config) ) {
            return $this->config['archive_exchange'];
        }
        return null;
    }

    public function sendSignature(Signature $signature): void
    {
        $this->addSignRequestFromSignature($signature);
    }

    public function deleteSignature(Signature $signature): void
    {
        $this->deleteSignRequest(intval($signature->getDocumentRemotekey()));
    }

    public function getSignatureStatus(Signature $signature): string
    {
        $infos = $this->getSignatureInfos($signature);
        return $infos['status'];
    }

    public function getSignatureInfos(Signature $signature): ResponseInfos
    {
        return $this->getSignRequestInfo($signature->getDocumentRemotekey());
    }

    public function getLastDocumentSignature(Signature $signature): string
    {
        return $this->getSignRequestLastFile($signature->getDocumentRemotekey());
    }

    public function checkAccess(): void
    {
        try {
            $access = $this->getAccess();
            $access->get($this->getBaseUrl() . self::WS_WORKFLOW_GET_ALL);
        } catch (Exception $e) {
            $msg = "Parafeur ESUP n'est pas accessible";
            $this->getLoggerService()->critical($e->getMessage());
            throw new SignatureException($msg);
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    /// USUAL
    ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Retourne l'accès CURL.
     *
     * @return CurlAccess|null
     */
    public function getAccess(): ?CurlAccess
    {
        if ($this->curlAccess === null) {
            $this->curlAccess = new CurlAccess($this->getLoggerService(), $this->getArchiveExchange());
        }
        return $this->curlAccess;
    }

    /**
     * @return string
     */
    public function getCreatedByEppn(): string
    {
        return $this->config['config']['createdByEppn'];
    }


    public function getBaseUrl(): string
    {
        return $this->config['config']['url'];
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    /// IMPLEMENTATION FONCTIONNELLE
    ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Liste des procédures en cours.
     *
     * @return ISignRequest[]
     * @throws SignatureCurlException
     * @throws SignatureDataMissingException
     */
    public function getSignRequests(): array
    {
        try {
            $jsonDatas = $this->getAccess()->get($this->getBaseUrl() . self::WS_SIGNREQUESTS_GET_ALL);
            $jsonDatas = json_decode($jsonDatas['return'], true);
            return EsupSignRequest::getSignRequestsFromDatas($jsonDatas);
        } catch (Exception $e) {
            $this->getLoggerService()->critical(
                "Impossible de charger les SignRequest : " . $e->getMessage()
            );
            throw $e;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// FIN INTERFACE
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Convertis les statuts reçus du parapheur vers les statuts normalisés des objets Signature.
     *
     * @param string $statusFromLetterFile
     * @return string
     * @throws Exception
     */
    public function statusConvert(string $statusFromLetterFile): string
    {
        $status = [
            'pending'       => Signature::STATUS_SIGNATURE_WAIT,
            'fully-deleted' => Signature::STATUS_SIGNATURE_FULLDELETE,
            'deleted'       => Signature::STATUS_SIGNATURE_DELETE,
            'completed'     => Signature::STATUS_SIGNATURE_SIGNED,
            'archived'      => Signature::STATUS_SIGNATURE_SIGNED,
            'refused'       => Signature::STATUS_SIGNATURE_REJECT
        ];
        if (!array_key_exists($statusFromLetterFile, $status)) {
            throw new Exception("Status '$statusFromLetterFile' sans correspondence dans le module signature ");
        }
        return $status[$statusFromLetterFile];
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///
    /// ACCESS au PARAFEUR 'Direct' (API REST)
    ///
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param string $genericType
     * @return string
     * @throws SignatureException
     */
    public function getSignRequestType(string $genericType): string
    {
        $marks = $this->getLevels();
        if (!array_key_exists($genericType, $marks)) {
            throw new SignatureException("Type de signature '$genericType' non configuré dans le parafeur");
        }
        return $marks[$genericType];
    }

    /**
     * Retourne la liste des Workflows.
     *
     * @return array
     * @throws SignatureCurlException
     * @throws SignatureDataMissingException
     */
    public function getWorkflows(): array
    {
        try {
            $jsonDatas = $this->getAccess()->get($this->getBaseUrl() . self::WS_WORKFLOW_GET_ALL);
            $jsonDatas = json_decode($jsonDatas['return'], true);
            return EsupWorkflow::getWorkflowsFromJsonArray($jsonDatas);
        } catch (Exception $e) {
            $this->getLoggerService()->critical("Impossible de charger les workflows : " . $e->getMessage());
            throw $e;
        }
    }

    /**
     * Suppression de la SignRequest dans le parapheur.
     *
     * @param int $id
     * @return mixed
     * @throws SignatureCurlException
     */
    public function deleteSignRequest(int $id): mixed
    {
        try {
            $jsonDatas = $this->getAccess()->delete(
                $this->getBaseUrl() . sprintf(self::WS_SIGNREQUESTS_DELETE_ID, $id),
                ['id' => $id]
            );

            if(isset($jsonDatas['code']) && $jsonDatas['code'] == 200)
            {
                return true;
            }
            return false;
        } catch (Exception $e) {
            $this->getLoggerService()->critical(
                "Impossible de supprimer la SignRequest '$id' : " . $e->getMessage()
            );
            throw $e;
        }
    }

    /**
     * @throws SignatureException
     */
    public function addSignRequestFromSignature(Signature $signature): void
    {
        // Emplacement du document
        $docPath = $this->getSignatureConfigurationService()->getDocumentsLocation()
            . DIRECTORY_SEPARATOR
            . $signature->getDocumentPath();

        $id = $this->addDocument(
            $docPath,
            $signature->getRecipientsEmailsArray(),
            $signature->getLabel(),
            $signature->getDescription(),
            $this->getSignRequestType($signature->getType()),
            $signature->getAllSignToComplete()
        );
        // https://signature-pp.unicaen.fr/user/signrequests/51638
        $url = $this->getBaseUrl() . '/user/signrequests/' . $id;
        foreach ($signature->getRecipients() as $recipient) {
            $recipient->setUrlDocument($url);
        }

        $signature->setDocumentRemotekey($id);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////// DIRECT ESUP

    /**
     * Retourne le dernier document de la SignRequest.
     *
     * @param int $idSignRequest
     * @return string
     * @throws SignatureCurlException
     */
    public function getSignRequestLastFile(int $idSignRequest): string
    {
        try {
            $jsonDatas = $this->getAccess()->get(
                $this->getBaseUrl() . sprintf(self::WS_SIGNREQUESTS_GET_LAST_FILE, $idSignRequest)
            );
            return $jsonDatas['return'];
        } catch (Exception $e) {
            $msg = "Impossible de charger le document de la SignRequest '$idSignRequest' dans ESUP";
            $this->getLoggerService()->critical(
                $msg . " : " . $e->getMessage()
            );
            throw new SignatureException($msg);
        }
    }

    /**
     * Retourne les informations sur la SignRequest.
     *
     * @param int $id
     * @return ResponseInfos
     * @throws SignatureCurlException
     */
    public function getSignRequestInfo(int $id): ResponseInfos
    {
        try {
            $jsonDatas = $this->getAccess()->get(
                $this->getBaseUrl() . sprintf(self::WS_SIGNREQUESTS_GET_ID, $id)
            );

            $returned = json_decode($jsonDatas['return'], true);
            //Cas d'une signature esup qui a été supprimé directement dans esup.
            if(empty($returned))
            {
                throw new Exception("La signature n'existe plus dans Esup");
            }

            return SignRequestInfo::getFromSignRequestGetId($returned);

        } catch (Exception $e) {
            $msg = "Impossible de charger les informations de la SignRequest '$id' dans ESUP";
            $this->getLoggerService()->critical(
                $msg . " : " . $e->getMessage()
            );
            throw new SignatureException($msg . ':' . $e->getMessage());
        }
    }

    /**
     * Retourne le status de la SignRequest.
     *
     * @param int $id
     * @return mixed
     * @throws SignatureCurlException
     */
    public function getSignRequestsStatus(int $id): mixed
    {
        try {
            $jsonDatas = $this->getAccess()->get(
                $this->getBaseUrl() . sprintf(self::WS_SIGNREQUESTS_GET_STATUS_ID, $id)
            );
            return $jsonDatas['return'];
        } catch (Exception $e) {
            $msg = "Impossible de charger le statut de la signature '$id'  dans ESUP";
            $this->getLoggerService()->critical(
                "$msg : " . $e->getMessage()
            );
            throw new SignatureException($msg);
        }
    }

    /**
     * Envoi d'une demande simple (Document, Emails, type de signature, etc...
     *
     * @param string $documentFullPath Chemin complet du document
     * @param string[] $emails Liste des emails
     * @param string|null $title
     * @param string|null $comment
     * @param string $signType Type de signature attendue
     * @param bool $allSignToComplete Tous les destinataires doivent signer
     *
     * @return string
     * @throws SignatureException
     */
    public function addDocument(
        string $documentFullPath,
        array $emails,
        ?string $title,
        ?string $comment,
        string $signType,
        bool $allSignToComplete
    ): string {
        $send = [
            'post'  => [
                'recipientsEmails'  => $emails,
                'allSignToComplete' => $allSignToComplete,
                'comment'           => $comment ?? '',
                'title'             => $title ?? '',
                'signType'          => $signType,
                'createByEppn'      => $this->getCreatedByEppn()
            ],
            'files' => [
                'multipartFiles' => $documentFullPath,
            ]
        ];

        try {
            $jsonDatas = $this->getAccess()->post(
                $this->getBaseUrl() . self::WS_SIGNREQUESTS_POST_NEW,
                $send['post'],
                $send['files']
            );
            return strval($jsonDatas['result']);
        } catch (Exception $e) {
            $msg = "Impossible de créer la signature dans ESUP";
            $this->getLoggerService()->critical("$msg : " . $e->getMessage());
            throw new SignatureException($msg);
        }
    }
}