<?php

namespace UnicaenSignature\Strategy\Letterfile\Esup;

use UnicaenSignature\Exception\SignatureDataMissingException;
use UnicaenSignature\Strategy\Letterfile\ISignRequest;
use UnicaenSignature\Strategy\Letterfile\Tools\JsonDataArray;

class EsupSignRequest extends JsonDataArray implements ISignRequest
{
    private int $id;
    private \DateTime $dateCreate;
    private ?\DateTime $dateEnd;
    private string $title;
    private string $status;

    protected function __construct()
    {
    }

    /**
     * @param array $arrayFromJson
     * @return EsupUser[]
     * @throws SignatureDataMissingException
     */
    public static function getSignRequestsFromDatas(array $datas): array
    {
        $out = [];
        foreach ($datas as $d) {
            $out[] = self::getSignRequestFromData($d);
        }
        return $out;
    }

    /**
     * @param array $arrayFromJson
     * @return static
     * @throws SignatureDataMissingException
     */
    public static function getSignRequestFromData(array $data): self
    {
        $dt = new EsupSignRequest();
        $dt->id = self::getRequiredEntry($data, 'id');
        $dt->dateCreate = self::getDate($data, 'createDate');
        $dt->dateEnd = self::getDate($data, 'endDate');
        $dt->title = self::getRequiredEntry($data, 'title');
        $dt->status = self::getRequiredEntry($data, 'status');
        return $dt;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreate(): \DateTime
    {
        return $this->dateCreate;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateEnd(): ?\DateTime
    {
        return $this->dateEnd;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getIdInLetterfile(): string
    {
        return strval($this->getId());
    }


    public function getRecipientsEmails(): ?array
    {
        return null;
    }

    public function getDateCreated(): \DateTime
    {
        return $this->getDateCreate();
    }

    public function getDateUpdated(): ?\DateTime
    {
        return $this->getDateEnd();
    }
}