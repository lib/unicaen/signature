<?php

namespace UnicaenSignature\Strategy\Letterfile\Esup;

use UnicaenSignature\Exception\SignatureDataMissingException;
use UnicaenSignature\Strategy\Letterfile\Tools\JsonDataArray;

class EsupUser extends JsonDataArray
{
    private int $id;
    private string $firstname;
    private string $name;
    private string $email;

    protected function __construct()
    {
    }

    /**
     * @param array $arrayFromJson
     * @return EsupUser[]
     * @throws SignatureDataMissingException
     */
    public static function getUsersFromJsonArray(array $arrayFromJson): array
    {
        $out = [];
        foreach ($arrayFromJson as $j) {
            $out[] = self::getWorkflowFromArray($j);
        }
        return $out;
    }

    /**
     * @param array $arrayFromJson
     * @return static
     * @throws SignatureDataMissingException
     */
    public static function getUserFromArray(array $arrayFromJson): self
    {
        $dt = new EsupUser();
        $dt->id = self::getRequiredEntry($arrayFromJson, 'id');
        $dt->firstname = self::getRequiredEntry($arrayFromJson, 'firstname');
        $dt->email = self::getRequiredEntry($arrayFromJson, 'email');
        $dt->name = self::getRequiredEntry($arrayFromJson, 'name');
        return $dt;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getFullname(): string
    {
        return sprintf('%s %s', $this->getFirstname(), $this->getName());
    }
}