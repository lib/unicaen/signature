<?php

namespace UnicaenSignature\Strategy\Letterfile\Esup;

use UnicaenSignature\Exception\SignatureDataMissingException;
use UnicaenSignature\Strategy\Letterfile\Tools\JsonDataArray;

class EsupWorkflowStep extends JsonDataArray
{
    private int $id;
    private ?string $name;
    private ?string $description;
    private ?bool $changeable;
    private ?bool $repeatable;
    private bool $allSignToComplete;
    private string $signType;
    private array $users;

    /**
     * @param array $users
     */
    public function __construct()
    {
        $this->users = [];
    }

    /**
     * @param array $json
     * @return static
     * @throws SignatureDataMissingException
     */
    public static function extractFromJsonObject( array $json ) :self
    {
        $dt = new EsupWorkflowStep();
        $dt->id = self::getRequiredEntry($json, 'id');
        $dt->description = self::getOptionalEntry($json, 'description', '');
        $dt->name = self::getOptionalEntry($json, 'name');
        $dt->changeable = self::getRequiredEntry($json, 'changeable');
        $dt->allSignToComplete = self::getRequiredEntry($json, 'allSignToComplete');
        $dt->signType = self::getRequiredEntry($json, 'signType');
        $dt->repeatable = self::getOptionalEntry($json, 'repeatable');
        foreach (self::getRequiredEntry($json, 'users') as $user) {
            $dt->users[] = EsupUser::getUserFromArray($user);
        }
        return $dt;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return bool|null
     */
    public function getChangeable(): ?bool
    {
        return $this->changeable;
    }

    /**
     * @return bool|null
     */
    public function getRepeatable(): ?bool
    {
        return $this->repeatable;
    }

    /**
     * @return bool
     */
    public function isAllSignToComplete(): bool
    {
        return $this->allSignToComplete;
    }

    /**
     * @return string
     */
    public function getSignType(): string
    {
        return $this->signType;
    }

    /**
     * @return EsupUser[]
     */
    public function getUsers(): array
    {
        return $this->users;
    }
}