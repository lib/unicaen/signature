<?php

namespace UnicaenSignature\Strategy\Letterfile;

use DateTime;

/**
 * Décrit la procédure de signature en cours
 */
interface ISignRequest
{

    /**
     * @return string
     */
    public function getIdInLetterfile(): string;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * Liste des emails des signataires.
     *
     * @return string[]|null
     */
    public function getRecipientsEmails(): ?array;

    /**
     * @return string
     */
    public function getStatus(): string;

    /**
     * @return DateTime
     */
    public function getDateCreated(): DateTime;

    /**
     * @return DateTime|null
     */
    public function getDateUpdated(): ?DateTime;
}