<?php

namespace UnicaenSignature\Utils;

use Exception;

class Configuration
{
    private array $config;

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @return array
     */
    protected function getConfig(): array
    {
        return $this->config;
    }

    /**
     * @param string $key
     * @return mixed
     * @throws Exception
     */
    public function getConfiguration(string $key): mixed
    {
        $config = $this->getConfig();
        if ($key) {
            $paths = explode('.', $key);
            foreach ($paths as $path) {
                if (!isset($config[$path])) {
                    throw new Exception("Clef '$path' absente dans la configuration");
                }
                $config = $config[$path];
            }
        }
        return $config;
    }

    /**
     * @param string $key
     * @param mixed|null $defaultValue
     * @return mixed
     */
    public function getOptionalConfiguration(string $key, mixed $defaultValue = null)
    {
        try {
            return $this->getConfiguration($key);
        } catch (Exception $e) {
            return $defaultValue;
        }
    }
}