<?php

namespace UnicaenSignature\Utils;

interface SignatureConstants
{
    const VISA_HIDDEN = 'visa_hidden';
    const VISA_VISUAL = 'visa_visual';
    const SIGN_VISUAL = 'sign_visual';
    const SIGN_CERTIF = 'sign_certif';
    const SIGN_EIDAS = 'sign_eidas';

    const FORMAT_JSON = 'format_json';
    const FORMAT_ARRAY = 'format_array';
    const FORMAT_DEFAULT = 'format_object';

    const GET_RECIPIENTS_METHOD_FIXED = 'recipients_fixed';
    const GET_RECIPIENTS_METHOD_FREE = 'recipients_free';


    // évenements liés aux destinataires
    const EVENT_RECIPIENT_ACCEPTED = 'event_recipient_accepted';
    const EVENT_RECIPIENT_REJECTED = 'event_recipient_rejected';

    // événements liés aux signatures
    const EVENT_SIGNATURE_SEND = 'event_signature_send';
    const EVENT_SIGNATURE_SIGNED = 'event_signature_signed';
    const EVENT_SIGNATURE_REJECTED = 'event_signature_rejected';
}

