<?php

namespace UnicaenSignature\Controller;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use UnicaenSignature\Service\SignatureService;

class SignatureControllerFactory implements FactoryInterface {

    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null) : SignatureController
    {
        $s = new SignatureController();
        $s->setSignatureService($container->get(SignatureService::class));
        return $s;
    }

}