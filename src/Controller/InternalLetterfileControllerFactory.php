<?php

namespace UnicaenSignature\Controller;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use UnicaenSignature\Service\InternalLetterfileService;

class InternalLetterfileControllerFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): InternalLetterfileController
    {
        $s = new InternalLetterfileController();
        $s->setInternalLetterfileService($container->get(InternalLetterfileService::class));
        return $s;
    }

}