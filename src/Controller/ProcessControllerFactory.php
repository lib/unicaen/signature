<?php

namespace UnicaenSignature\Controller;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use UnicaenSignature\Service\ProcessService;

class ProcessControllerFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): ProcessController
    {
        $s = new ProcessController();
        $s->setProcessService($container->get(ProcessService::class));
        $s->setProcessService($container->get(ProcessService::class));
        return $s;
    }

}