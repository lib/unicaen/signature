<?php

namespace UnicaenSignature\Controller;

use BjyAuthorize\Exception\UnAuthorizedException;
use Exception;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger;
use UnicaenSignature\Entity\Db\Signature;
use UnicaenSignature\Exception\SignatureException;
use UnicaenSignature\Service\InternalLetterfileServiceAwareTrait;

class InternalLetterfileController extends AbstractActionController
{

    use InternalLetterfileServiceAwareTrait;

    protected function flash(): FlashMessenger
    {
        return $this->getPluginManager()->get('flashmessenger');
    }

    ////////////////////////////////////////////////////////////////////////
    ///
    public function indexAction()
    {
        return [
            'signatures' => $this->getInternalLetterfileService()->getSignaturesInternal()
        ];
    }

    /**
     * Accès au "viseur" interne.
     *
     * @return array|void
     */
    public function visaAction()
    {
        try {
            $key = $this->params()->fromRoute('key');
            if (!$key) {
                throw new SignatureException("Paramètre manquant");
            }

            $signatureRecipient = $this->getInternalLetterfileService()->getSignatureRecipientByKey($key);
            $waitingVisa = $signatureRecipient->getSignature()->getStatus() == Signature::STATUS_SIGNATURE_WAIT;

            if (!$signatureRecipient->requireSignFromEmail(
                $this->getInternalLetterfileService()->getSignatureConfigurationService()->getCurrentUserMail()
            )) {
                $this->flash()->addErrorMessage("Ce document n'est pas disponible.");
                return $this->redirect()->toRoute('unicaen-signature/my-documents');
            }

            if ($this->getRequest()->isPost()) {
                $datas = $this->getRequest()->getPost()->toArray();

                $comment = trim($datas['comment']);

                try {
                    if ($datas['response'] == 'ok') {
                        $this->getInternalLetterfileService()->accept($signatureRecipient, $comment);
                        return $this->redirect()->toRoute('unicaen-signature/my-documents');
                        $waitingVisa = false;
                    }
                    elseif ($datas['response'] == 'ko') {
                        if ($comment == "") {
                            $this->flash()->addErrorMessage("Vous devez justifier votre refus en commentaire. Merci.");
                        }
                        else {
                            $this->getInternalLetterfileService()->refuse($signatureRecipient, $comment);
                            return $this->redirect()->toRoute('unicaen-signature/my-documents');
                            $waitingVisa = false;
                        }
                    }
                    else {
                        $this->flash()->addErrorMessage("Erreur de requête");
                    }
                } catch (Exception $e) {
                    $this->flash()->addErrorMessage("Erreur : " . $e->getMessage());
                }
            }

            return [
                'signatureRecipient' => $signatureRecipient,
                'waitingVisa'        => $waitingVisa
            ];
        } catch (Exception $e) {
            $this->flash()->addErrorMessage($e->getMessage());
        }
    }

    /**
     * Afichage du document à viser.
     * @todo Authoriser l'accès au signataire uniquement
     *
     * @return void
     */
    public function documentAction()
    {
        try {
            $key = $this->params()->fromRoute('key');
            if (!$key) {
                throw new SignatureException("Paramètre manquant");
            }

            $signatureRecipient = $this->getInternalLetterfileService()->getSignatureRecipientByKey($key);

            $this->getInternalLetterfileService()->getLoggerService()->debug("Consultation du document '$key'");
            $currentUserMail = $this->getInternalLetterfileService()->getSignatureConfigurationService(
            )->getCurrentUserMail();

            if ($signatureRecipient->getEmail() != $currentUserMail) {
                $err = "Consultation non autorisée pour " . $currentUserMail;
                $this->getInternalLetterfileService()->getLoggerService()->error($err);
                throw new SignatureException($err);
            }
            $documentInfos = $this->getInternalLetterfileService()->getDocumentByRecipient($signatureRecipient);
            if ($documentInfos['mime']) {
                header('Content-Type: ' . $documentInfos['mime']);
            }
            header('Content-Transfer-Encoding: Binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($documentInfos['path']));
            die($documentInfos['datas']);
        } catch (Exception $e) {
            $this->flash()->addErrorMessage($e->getMessage());
        }
    }
}