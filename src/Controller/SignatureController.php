<?php

namespace UnicaenSignature\Controller;

use Exception;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Laminas\View\Model\ViewModel;
use UnicaenSignature\Exception\SignatureException;
use UnicaenSignature\Service\SignatureServiceAwareTrait;

class SignatureController extends AbstractActionController
{

    use SignatureServiceAwareTrait;

    protected function flash(): FlashMessenger
    {
        return $this->getPluginManager()->get('flashmessenger');
    }

    public function homeAction(): ViewModel
    {
        return new ViewModel([]);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////// SIGNATURE

    /**
     * Liste des procédures des signatures en cours.
     *
     * @return ViewModel
     */
    public function signaturesAction(): ViewModel
    {
        try {
            return new ViewModel([
                                     'signatures'  => $this->getSignatureService()->getSignaturesSimples(),
                                     'letterfiles' => $this->getSignatureService()->getSignatureConfigurationService(
                                     )->getLetterFiles()
                                 ]);
        } catch (Exception $e) {
            $this->flash()->addErrorMessage($e->getMessage());
            return new ViewModel(['signatures' => []]);
        }
    }

    public function myDocumentsAction(): ViewModel
    {
        try {
            $datas = [
                'signatureRecipients' => $this->getSignatureService()
                    ->getSignaturesRecipientsByEmail(
                        $this->getSignatureService()->getSignatureConfigurationService()->getCurrentUserMail()
                    ),
            ];
            return new ViewModel($datas);
        } catch (Exception $e) {
            $this->flash()->addErrorMessage($e->getMessage());
            return new ViewModel(['signatures' => []]);
        }
    }

    /**
     * Permet d'afficher les données du document d'une Signature.
     *
     * @return void
     */
    public function previewDocumentAction(): void
    {
        try {
            $id = intval($this->params()->fromRoute('id'));
            if (!$id) {
                throw new SignatureException("Paramètre manquant");
            }

            $signature = $this->getSignatureService()->getSignature($id);
            $doc = $this->getSignatureService()->getDocumentData($signature);
            header('Content-Type: ' . $doc['mime']);
            header('Content-Transfer-Encoding: Binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($doc['path']));
            die($doc['datas']);
        } catch (Exception $e) {
            $this->flash()->addErrorMessage($e->getMessage());
        }
    }

    /**
     * Affichage du document.
     *
     * @return Response|void
     */
    public function downloadAction()
    {
        try {
            $id = $this->params('id');
            header("Content-type:application/pdf");
            header("Content-Disposition:attachment;filename=preview.pdf");
            die($this->getSignatureService()->getDocumentSignedSignatureById($id));
        } catch (Exception $e) {
            $this->flash()->addErrorMessage("Erreur : " . $e->getMessage());
        }
        return $this->redirect()->toRoute('unicaen-signature');
    }
}