<?php

namespace UnicaenSignature\Controller\Factory;

use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;
use UnicaenSignature\Controller\AdministrationController;
use UnicaenSignature\Service\ProcessService;
use UnicaenSignature\Service\SignatureService;

class AdministrationControllerFactory implements FactoryInterface {

    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null) : AdministrationController
    {
        $s = new AdministrationController();
        $s->setSignatureService($container->get(SignatureService::class));
        $s->setProcessService($container->get(ProcessService::class));
        return $s;
    }
}