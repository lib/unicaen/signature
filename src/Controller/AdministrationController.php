<?php

namespace UnicaenSignature\Controller;

use Exception;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;
use UnicaenSignature\Entity\Db\Signature;
use UnicaenSignature\Exception\SignatureException;
use UnicaenSignature\Form\SignatureForm;
use UnicaenSignature\Service\ProcessServiceAwareTrait;
use UnicaenSignature\Service\SignatureServiceAwareTrait;
use UnicaenSignature\Utils\FileUploader;
use UnicaenSignature\Utils\SignatureConstants;

class AdministrationController extends AbstractActionController
{

    use SignatureServiceAwareTrait,
        ProcessServiceAwareTrait;

    protected function flash(): FlashMessenger
    {
        return $this->getPluginManager()->get('flashmessenger');
    }

    /**
     * Liste des signatures dans l'application.
     *
     * @return ViewModel
     */
    public function indexAction(): ViewModel
    {
        return new ViewModel([]);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// SIGNATURES SIMPLES

    /**
     * Administration des signatures simples.
     *
     * @return ViewModel
     */
    public function simpleSignatureAction(): ViewModel
    {
        try {
            $datas = [
                'signatures'  => $this->getSignatureService()->getSignaturesSimples(),
                'letterfiles' => $this->getSignatureService()->getSignatureConfigurationService()->getLetterFiles()
            ];
            return new ViewModel($datas);
        } catch (Exception $e) {
            $this->flash()->addErrorMessage($e->getMessage());
            return new ViewModel(['signatures' => []]);
        }
    }

    /**
     * Mise à jour du statut d'une signature.
     *
     * @return Response
     */
    public function simpleSignatureStatusAction(): Response
    {
        try {
            $id = intval($this->params('id'));
            $this->getSignatureService()->updateStatusSignatureById($id);
            $this->flash()->addSuccessMessage("Le statut de la demande a bien été mis à jour");
        } catch (Exception $e) {
            $this->flash()->addErrorMessage("Erreur : " . $e->getMessage());
        }
        return $this->redirect()->toRoute('unicaen-signature/administration/simple-signature');
    }

    /**
     * Envoi d'une signature (en Brouillon).
     *
     * @return Response
     */
    public function simpleSignatureSendAction(): Response
    {
        try {
            $id = intval($this->params('id'));
            $this->getSignatureService()->sendSignatureById($id);
            $this->flash()->addSuccessMessage("La demande a bien été envoyée");
        } catch (Exception $e) {
            $this->flash()->addErrorMessage("Erreur : " . $e->getMessage());
        }
        return $this->redirect()->toRoute('unicaen-signature/administration/simple-signature');
    }

    /**
     * Suppression d'une signature.
     *
     * @return Response
     */
    public function simpleSignatureDeleteAction(): Response
    {
        try {
            $id = $this->params('id');
            $this->getSignatureService()->deleteSignatureById($id);
            $this->flash()->addSuccessMessage("La demande a bien été supprimée");
        } catch (Exception $e) {
            $this->flash()->addErrorMessage("Erreur : " . $e->getMessage());
        }
        return $this->redirect()->toRoute('unicaen-signature/administration/simple-signature');
    }

    /**
     * Nouvelle signature simple.
     * ATTENTION : Non prévu pour un usage en production
     *
     * @return Response|ViewModel
     * @throws SignatureException
     */
    public function simpleSignatureNewAction(): Response|ViewModel
    {
        // Clef du parapheur utilisé
        $letterfileKey = $this->params()->fromRoute('key', null);

        // Parapheur
        $letterFile = $this->getSignatureService()->getLetterfileService()->getLetterFileStrategy($letterfileKey);

        // Niveaux de signature disponibles
        $marksSelector = $this->getSignatureService()->getMarksSelectFromLetterFile($letterfileKey);

        // Création de la signature
        $signature = new Signature();
        $signature->setLetterfileKey($letterFile->getName());
        $form = new SignatureForm($marksSelector);
        $form->setSignatureService($this->getSignatureService());
        $form->init();
        $form->setObject($signature);

        $error = "";

        if ($this->getRequest()->isPost()) {
            $post = array_merge_recursive(
                $this->getRequest()->getPost()->toArray(),
                $this->getRequest()->getFiles()->toArray()
            );
            $form->setData($post);
            if ($form->isValid()) {
                $file = $this->getRequest()->getFiles()->toArray()['file'];
                $destination = $this->getSignatureService()->getSignatureConfigurationService()->getDocumentsLocation();
                $basename = uniqid('tmp_signature_simple_');
                $mimesAllowed = $this->getSignatureService()->getSignatureConfigurationService()->getDocumentMimes();

                $uploader = new FileUploader();
                $uploader->setDestination($destination)
                    ->setFilename($basename)
                    ->setMimesAllowed($mimesAllowed);
                $uploader->updoad($file);

                /** @var Signature $datas */
                $datas = $form->getData();
                $datas->setDocumentPath($uploader->getUploadName());

                try {
                    $this->getSignatureService()->saveNewSignature($form->getData(), true);
                } catch (Exception $e) {
                    $this->flash()->addErrorMessage($e->getMessage());
                }
                return $this->redirect()->toRoute('unicaen-signature/administration/simple-signature');
            }
            else {
                $error = "Informations incomplètes";
            }
        }

        $view = new ViewModel(
            [
                'letterfile' => $letterFile,
                'form'       => $form,
                'error'      => $error
            ]
        );

        $view->setTemplate('unicaen-signature/administration/simple-signature-form');

        return $view;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// PROCESSUS

    /**
     * Liste des processus
     *
     * @return ViewModel
     */
    public function processAction(): ViewModel
    {
        try {
            $datas = [
                'processes'      => $this->getProcessService()->getProcesses(),
                'signatureflows' => $this->getProcessService()->getSignatureFlows()
            ];
            return new ViewModel($datas);
        } catch (\Exception $e) {
            $this->flash()->addErrorMessage('Impossible de charger les processus en cours : ' . $e->getMessage());
        }
        return new ViewModel([]);
    }

    /**
     * @return ViewModel|Response
     */
    public function processConfigAction(): ViewModel|Response
    {
        try {
            $id = intval($this->params()->fromRoute('id'));
            if (!$id) {
                throw new SignatureException("Paramètre manquant");
            }

            $process = $this->getProcessService()->getProcessById($id);
            $documentPreviewUrl = $this->url()->fromRoute('unicaen-signature/process/document', ['id' => $id]);

            if ($this->getRequest()->isXmlHttpRequest() || $this->getRequest()->getQuery('format') == 'json') {
                $datas = [
                    'datas' => $this->getProcessService()->getSignatureService()->createSignatureFlowDatasById(
                        $documentPreviewUrl,
                        $process->getSignatureFlow()->getId(),
                        []
                    )
                ];
                return new JsonModel($datas);
            }

            if ($this->getRequest()->isPost()) {
                $json = $this->getRequest()->getPost()->toArray()['json'];
                $dt = json_decode($json, true);
                try {
                    $dt['doc_path'] = $this->getProcessService()->getProcessDocumentDatas($process)['path'];
                    $this->getProcessService()->configureProcess($process, $dt);
                } catch (\Exception $e) {
                    $this->flash()->addErrorMessage("Erreur de configuration : " . $e->getMessage());
                }
                $this->redirect()->toRoute('unicaen-signature/administration/process');
            }

            return new ViewModel([
                                     'entrypoint' => $this->url()->fromRoute(
                                             'unicaen-signature/administration/process-config', ['id' => $id]
                                         ) . '?format=json'
                                 ]);
        } catch (\Exception $e) {
            $this->flash()->addErrorMessage($e->getMessage());
        }
        $this->redirect()->toRoute('unicaen-signature/administration/process');
    }

    /**
     * Suppression d'un processus.
     *
     * @return ViewModel|Response
     */
    public function processDeleteAction(): ViewModel|Response
    {
        try {
            $id = $this->params()->fromRoute('id');
            if (!$id) {
                throw new SignatureException("Paramètre ID manquant");
            }

            $this->getProcessService()->deleteProcessById($id);

            $this->flash()->addSuccessMessage("Le processus a bien été supprimé");
        } catch (\Exception $e) {
            $this->flash()->addErrorMessage('Impossible de supprimer le processus');
        }
        return $this->redirect()->toRoute('unicaen-signature/administration/process');
    }


    public function processNewAction(): ViewModel
    {
        try {
            $id = $this->params()->fromRoute('id');
            if (!$id) {
                throw new SignatureException("Paramètre manquant");
            }

            $signatureFlow = $this->getProcessService()->getSignatureFlow($id);

            if ($this->getRequest()->isPost()) {
                $files = $this->getRequest()->getFiles()->toArray();
                if (!array_key_exists('file', $files)) {
                    throw new \Exception("Aucun upload");
                }

                $file = $this->getRequest()->getFiles()->toArray()['file'];
                $destination = $this->getProcessService()->getSignatureConfigurationService()->getDocumentsLocation();
                $filename = uniqid('tmp_process_');
                $mimesAllowed = $this->getProcessService()->getSignatureConfigurationService()->getDocumentMimes();

                $uploader = new FileUploader();
                $uploader->setDestination($destination)
                    ->setFilename($filename)
                    ->setMimesAllowed($mimesAllowed);

                $uploader->updoad($file);
                $process = $this->getProcessService()->createUnconfiguredProcess(
                    $uploader->getUploadName(),
                    $signatureFlow->getId()
                );

                $this->redirect()->toRoute(
                    'unicaen-signature/administration/process-config',
                    ['id' => $process->getId()]
                );
            }
        } catch (\Exception $e) {
            $this->flash()->addErrorMessage($e->getMessage());
        }

        return new ViewModel(['signatureflow' => $signatureFlow]);
    }

    public function processTriggerAction(): ViewModel
    {
        try {
            $id = $this->params()->fromRoute('id');
            if (!$id) {
                throw new SignatureException("Paramètre manquant");
            }
            $this->getProcessService()->triggerById($id, true);
            $this->flash()->addSuccessMessage("Processus recalculé");
        } catch (\Exception $e) {
            // TODO Ce message ne s'affiche pas
            $this->flash()->addErrorMessage($e->getMessage())->addInfoMessage("foo");
        }
        $this->redirect()->toRoute('unicaen-signature/administration/process');
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// PARAPHEURS
    public function letterfileAction(): ViewModel
    {
        return new ViewModel([
                                 'letterfiles_configuration' => $this->getSignatureService()->getLetterfileService()
                                     ->getSignatureConfigurationService()->getLetterfileConfiguration(),
                                 'levels'                    => $this->getSignatureService()->getLetterfileService()
                                     ->getSignatureConfigurationService()->getLevels()
                             ]);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// PARAPHEURS
    public function signatureflowsAction(): ViewModel|Response
    {
        if ($this->getRequest()->isPost()) {
            $this->getSignatureService()->getLoggerService()->debug("Traitement des données envoyées");
            $posted = $this->getRequest()->getPost()->toArray();
            $action = $posted['action'];
            switch ($action) {
                case 'save' :
                    $datas = json_decode($posted['json'], true);
                    try {
                        $this->getSignatureService()->saveSignatureFlowFromJSON($datas);
                        return new JsonModel(['message' => "ok"]);
                    } catch (Exception $e) {
                        $this->getProcessService()->getLoggerService()->error($e->getMessage());
                        $this->response->setCustomStatusCode(Response::STATUS_CODE_501);
                        return new JsonModel(['error' => $e->getMessage()]);
                    }

                    break;

                case 'delete' :
                    $datas = json_decode($posted['json'], true);
                    try {
                        $this->getSignatureService()->deleteSignatureFlowFromJSON($datas);
                        return new JsonModel(['message' => "ok"]);
                    } catch (Exception $e) {
                        $this->getProcessService()->getLoggerService()->error($e->getMessage());
                        $this->response->setCustomStatusCode(Response::STATUS_CODE_501);
                        return new JsonModel(['error' => $e->getMessage()]);
                    }

                default:
                    $this->response->setCustomStatusCode(Response::STATUS_CODE_400);
                    return new JsonModel(['error' => "Mauvaise utilisation de l'API"]);
            }
        }

        // SignatureFlow normalize
        $signatureflows = $this->getSignatureService()->getSignatureFlows(SignatureConstants::FORMAT_ARRAY);

        foreach ($signatureflows as $i => $sf) {
            foreach ($sf['steps'] as $j => &$step) {
                $options = $this->getSignatureService()
                    ->getSignatureConfigurationService()
                    ->getDefaultMethodOptionsByKey($step['method']);

                foreach ($options as $key => $value) {
                    if (!array_key_exists($key, $step['options'])) {
                        $step['options'][$key] = $value;
                    }
                }
                if ($step['options']) {
                    $step['options_infos'] = [];

                    $method = $step['method'];
                    $recipientsMethod = $this->getSignatureService()->getSignatureConfigurationService(
                    )->getMethodByKey($method);

                    foreach ($step['options'] as $key => $value) {
                        $index = array_search($key, array_column($recipientsMethod['options'], 'key'));
                        $optionsDatas = $recipientsMethod['options'][$index];
                        $label = $optionsDatas['label'];
                        $step['options_infos'][$optionsDatas['label']] = [];
                        if (is_array($optionsDatas['values'])) {
                            foreach ($value as $v) {
                                $indexValue = intval($v);
                                if (array_key_exists($indexValue, $optionsDatas['values'])) {
                                    $step['options_infos'][$label][] = $optionsDatas['values'][$indexValue];
                                }
                            }
                        }
                    }
                }

                $observers_options = $this->getSignatureService()
                    ->getSignatureConfigurationService()
                    ->getDefaultMethodOptionsByKey($step['method']);

                foreach ($observers_options as $key => $value) {
                    if (!array_key_exists($key, $step['observers_options'])) {
                        $step['observers_options'][$key] = $value;
                    }
                }



                if ($step['observers_options']) {
                    $step['observers_options_infos'] = [];

                    $method = $step['method'];
                    $recipientsMethod = $this->getSignatureService()->getSignatureConfigurationService(
                    )->getMethodByKey($method);


                    foreach ($step['observers_options'] as $key => $value) {
                        $index = array_search($key, array_column($recipientsMethod['options'], 'key'));
                        $optionsDatas = $recipientsMethod['options'][$index];

                        $step['observers_options_infos'][$optionsDatas['label']] = [];
                        foreach ($value as $v) {
                            if(is_array($v)) {

                            } else {
                                $step['observers_options_infos'][$optionsDatas['label']][] = $optionsDatas['values'][$v];
                            }
                        }
                    }
                }


                $signatureflows[$i]['steps'][$j]['options'] = $step['options'];
                $signatureflows[$i]['steps'][$j]['observers_options'] = $step['observers_options'];
                $signatureflows[$i]['steps'][$j]['options_infos'] = $step['options_infos'];
                $signatureflows[$i]['steps'][$j]['observers_options_infos'] = $step['observers_options_infos'];
            }
        }

        return new JsonModel(
            [
                'version'                => 1.0,
                'signatureflows'         => $signatureflows,
                'letterfiles'            => $this->getSignatureService()->getLetterFiles(
                    SignatureConstants::FORMAT_ARRAY
                ),
                'get_recipients_methods' => $this->getSignatureService()->getSignatureConfigurationService(
                )->getRecipientsMethods(),
                'levels'                 => []
            ]
        );
    }

    /**
     * @return ViewModel
     * @throws SignatureException
     * @todo
     */
    public function signatureflowsUiAction(): ViewModel
    {
        return new ViewModel([]);
    }

}