<?php

namespace UnicaenSignature\Controller;

use Exception;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger;
use UnicaenSignature\Exception\SignatureException;
use UnicaenSignature\Service\ProcessServiceAwareTrait;

class ProcessController extends AbstractActionController
{

    use ProcessServiceAwareTrait;

    protected function flash(): FlashMessenger
    {
        return $this->getPluginManager()->get('flashmessenger');
    }

    ////////////////////////////////////////////////////////////////////////

    /**
     * Affichage du document
     *
     * @return void
     */
    public function previewDocumentAction(): void
    {
        try {
            $id = $this->params()->fromRoute('id');
            if (!$id) {
                throw new SignatureException("Paramètre manquant");
            }

            $process = $this->getProcessService()->getProcessById($id);
            $doc = $this->getProcessService()->getProcessDocumentDatas($process);
            header('Content-Type: ' . $doc['mime']);
            header('Content-Transfer-Encoding: Binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($doc['path']));

            die($doc['datas']);
        } catch (Exception $e) {
            $this->flash()->addErrorMessage($e->getMessage());
        }
    }
}