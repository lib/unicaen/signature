<?php

namespace UnicaenSignature\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use UnicaenSignature\Entity\Db\Signature;
use UnicaenSignature\Entity\Db\SignatureRecipient;

class SignatureRecipientRepository extends EntityRepository
{

    /**
     * Retourne la liste des destinataires qui peuvent être notifiés.
     *
     * @return SignatureRecipient[]
     */
    public function getRecipientsNotifiable() :array {
        $qb = $this->createQueryBuilder('sr')
            ->leftJoin( 'sr.signature', 's');

        $qb->andWhere('sr.status = :status AND s.status = :status');
        $qb->andWhere('s.allSignToComplete = TRUE');

        $parameters = [
            'status' => Signature::STATUS_SIGNATURE_WAIT
        ];

        $qb->setParameters($parameters);

        return $qb->getQuery()->getResult();
    }

    /**
     * Retourne les signatures en fonction du mail (Signature en attente et faites).
     *
     * @return Signature[]
     */
    public function getForEmail(string $email) :array
    {
        return $this->getRecipientSignatures(
            $email,
            [Signature::STATUS_SIGNATURE_WAIT,Signature::STATUS_SIGNATURE_SIGNED,Signature::STATUS_SIGNATURE_REJECT]
        );
    }

    /**
     * @return Signature[]
     */
    public function getRecipientSignatures(string|null $email = null, array|null $status = null, string|null $letterfile = null ) :array
    {
        $params = [];
        $qb = $this->createQueryBuilder('sr')
            ->leftJoin( 'sr.signature', 's');

        if( $email ){
            $qb->andWhere('sr.email = :email');
            $params['email'] = $email;
        }

        if( $status ){
            $qb->andWhere('sr.status IN (:status)');
            $params['status'] = $status;
        }

        if( $letterfile ){
            $qb->andWhere('s.letterfile_key = :letterfile');
            $params['letterfile'] = $letterfile;
        }

        if( $params ){
            $qb->setParameters($params);
        }

        return $qb->getQuery()->getResult();
    }
}