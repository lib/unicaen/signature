<?php

namespace UnicaenSignature\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Oro\ORM\Query\AST\Functions\Numeric\Sign;
use UnicaenSignature\Entity\Db\Signature;

class SignatureRepository extends EntityRepository
{
    /**
     * Retourne la liste des signatures pouvant être mises à jour.
     *
     * @return Signature[]
     */
    public function getSignaturesUpdatable() :array {
        $statusUpdatable = [
            Signature::STATUS_SIGNATURE_WAIT
        ];
        return $this->createQueryBuilder('s')
            ->where('s.status in(:statusUpdatable)')
            ->setParameter('statusUpdatable', $statusUpdatable)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Signature[]
     */
    public function getSignaturesSimples() :array
    {
        return $this->createQueryBuilder('s')
            ->leftJoin( 's.processStep', 'p')
            ->where('p IS NULL')
            ->orderBy('s.dateUpdate', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function getSignatures(string|null $recipient_mail = null, string|null $letterfile = null, array|null $status = []) {
        $params = [];
        $qb = $this->createQueryBuilder('s')
            ->leftJoin('s.recipients', 'r');

        if( $recipient_mail ){
            $qb->where('r.email = :recipient_mail');
            $params['recipient_mail'] = $recipient_mail;
        }

        if( $letterfile ){
            $qb->where('s.letterfile_key = :letterfile_key');
            $params['letterfile_key'] = $letterfile;
        }

        if( $status ){
            $qb->where('s.status IN (status)');
            $params['status'] = $status;
        }

        if( $params ){
            $qb->setParameters($params);
        }

        $qb->orderBy('s.dateUpdate', 'DESC')
            ->getQuery();

        return $qb->getResult();
    }
}