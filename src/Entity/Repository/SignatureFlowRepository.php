<?php

namespace UnicaenSignature\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class SignatureFlowRepository extends EntityRepository
{
    public function getAll() :array {
        return $this->findAll();
    }
}