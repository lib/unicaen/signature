<?php

namespace UnicaenSignature\Entity\Data;

use UnicaenSignature\Exception\SignatureException;

class LevelInfo
{
    private string $key;
    private string $label;
    private string $description;
    private string $logMessage;
    private bool $used;

    /**
     * @throws SignatureException
     */
    public function __construct(array $config, string $key)
    {
        $this->key = $key;
        $this->label = $this->getConfigValue($config, 'label');
        $this->description = $this->getConfigValue($config, 'description');
        $this->logMessage = $this->getConfigValue($config, 'log_message');
        $this->used = $this->getConfigValue($config, 'used');
    }

    /**
     * @param array $config
     * @param string $key
     * @return mixed
     * @throws SignatureException
     */
    private function getConfigValue(array $config, string $key): mixed
    {
        if (!array_key_exists($key, $config)) {
            throw new SignatureException(
                sprintf(
                    "Erreur de configuration : Un type de signature n'a pas de propriété '%s'",
                    $key
                )
            );
        }
        return $config[$key];
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return mixed|string
     */
    public function getLabel(): mixed
    {
        return $this->label;
    }

    /**
     * @return mixed|string
     */
    public function getDescription(): mixed
    {
        return $this->description;
    }

    /**
     * @return mixed|string
     */
    public function getLogMessage(): mixed
    {
        return $this->logMessage;
    }

    /**
     * @return bool
     */
    public function isUsed(): bool
    {
        return $this->used;
    }

    public function toArray(): array
    {
        return [
            'key'         => $this->getKey(),
            'label'       => $this->getLabel(),
            'description' => $this->getDescription(),
        ];
    }
}