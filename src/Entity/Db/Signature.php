<?php

namespace UnicaenSignature\Entity\Db;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use UnicaenSignature\Utils\SignatureConstants;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="UnicaenSignature\Entity\Repository\SignatureRepository")
 * @ORM\Table(name="unicaen_signature_signature")
 */
class Signature
{
    const STATUS_PROCESS_UNCONFIGURED = 0;

    const STATUS_SIGNATURE_DRAFT = 101;
    const STATUS_SIGNATURE_WAIT = 105;
    const STATUS_SIGNATURE_SIGNED = 201;
    const STATUS_SIGNATURE_CANCEL = 401;
    const STATUS_SIGNATURE_DELETE = 440;
    const STATUS_SIGNATURE_FULLDELETE = 450;
    const STATUS_SIGNATURE_REJECT = 501;

    /**
     * @param $id
     */
    public function __construct()
    {
        $this->recipients = new ArrayCollection();
        $this->observers = new ArrayCollection();
        $this->document_path = "";
        $this->status = self::STATUS_SIGNATURE_DRAFT;
        $this->type = "";
        $this->label = "";
        $this->description = "";
        $this->processStep = null;
        $this->context_short = null;
        $this->context_long = null;
    }

    /**
     * @return string[]
     */
    public static function getStatusArray(): array
    {
        return [
            self::STATUS_SIGNATURE_DRAFT      => "Brouillon",
            self::STATUS_SIGNATURE_WAIT       => "En attente de signature",
            self::STATUS_SIGNATURE_SIGNED     => "Signé",
            self::STATUS_SIGNATURE_CANCEL     => "Annulé",
            self::STATUS_SIGNATURE_FULLDELETE => "Supprimé définitivement du parafeur",
            self::STATUS_SIGNATURE_DELETE     => "Supprimé du parafeur",
            self::STATUS_SIGNATURE_REJECT     => "Rejeté",
        ];
    }

    /**
     * @param int $status
     * @return string
     */
    public static function getStatusLabel(int $status): string
    {
        $status_array = self::getStatusArray();
        if (!array_key_exists($status, $status_array)) {
            return "Statut inconnu";
        }
        else {
            return $status_array[$status];
        }
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="unicaen_signature_signature_id_seq",allocationSize=1, initialValue=1)
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var ?DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTime $dateCreated;

    /**
     * @var ?string
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private ?string $type;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false, options={"default":"101"})
     */
    private int $status = self::STATUS_SIGNATURE_DRAFT;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false, name="ordering", options={"default":"0"})
     */
    private int $order = 0;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $label;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $description;

    /**
     * Liste des contrats de financement pour le projet.
     *
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="SignatureRecipient", mappedBy="signature", cascade={"persist","remove"}, orphanRemoval=true)
     */
    protected $recipients;

    /**
     * Liste des observateurs
     *
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="SignatureObserver", mappedBy="signature", cascade={"persist","remove"}, orphanRemoval=true)
     */
    protected $observers;

    /**
     * Date d'envoi au parapheur
     *
     * @var ?DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTime $dateSend;

    /**
     * Date du dernier changement d'état.
     *
     * @var ?DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTime $dateUpdate;

    /**
     * @var string
     * @ORM\Column (type="string", nullable=false)
     */
    private string $document_path;

    /**
     * Clef permettant d'identifier le document côté parapheur
     *
     * @var ?string
     * @ORM\Column (type="string", length=255, nullable=true)
     */
    private ?string $document_remotekey;

    /**
     * Clef permettant d'identifier le document côté application
     *
     * @var string
     * @ORM\Column (type="string", length=255, nullable=true)
     */
    private string $document_localkey;

    /**
     * Texte court utilisé en sujet de mail (notifications)
     *
     * @var ?string
     * @ORM\Column (type="string", length=255, nullable=true)
     */
    private ?string $context_short;

    /**
     * Texte long utilisé dans le corps du mail (notifications)
     *
     * @var ?string
     * @ORM\Column (type="text", nullable=true)
     */
    private ?string $context_long;

    /**
     * Message de refus
     *
     * @var ?string
     * @ORM\Column (type="text", nullable=true)
     */
    private ?string $refused_text;

    /**
     * Clef permettant d'identifier le parapheur
     *
     * @var string|null
     * @ORM\Column (type="string", length=255, nullable=true)
     */
    private ?string $letterfile_key;

    /**
     * Nom de la procédure de signature dans le parapheur
     *
     * @var ?string
     * @ORM\Column (type="string", length=255, nullable=true)
     */
    private ?string $letterfile_process;

    /**
     * URL permettant d'identifier la procédure dans le parapheur
     *
     * @var string|null
     * @ORM\Column (type="string", length=255, nullable=true)
     */
    private ?string $letterfile_url;

    /**
     * Une signature de tous les destinataires est attendue.
     *
     * @var string|null
     * @ORM\Column (type="boolean", options={"default"="false"})
     */
    private bool $allSignToComplete = false;

    /**
     * Une signature de tous les destinataires est attendue.
     *
     * @var string|null
     * @ORM\Column (type="boolean", options={"default"="false"})
     */
    private bool $notificationsRecipients = false;

    /**
     * @var ProcessStep|null
     * @ORM\OneToOne(targetEntity="ProcessStep", mappedBy="signature")
     */
    private ProcessStep|null $processStep;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function getAllSignToComplete(): bool
    {
        return $this->allSignToComplete;
    }

    /**
     * @param bool $allSignToComplete
     * @return Signature
     */
    public function setAllSignToComplete(bool $allSignToComplete): self
    {
        $this->allSignToComplete = $allSignToComplete;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder(int $order): self
    {
        $this->order = $order;
        return $this;
    }

    public function getContextShort(): ?string
    {
        return $this->context_short;
    }

    public function setContextShort(?string $context_short): self
    {
        $this->context_short = $context_short;
        return $this;
    }

    public function getContextLong(): ?string
    {
        return $this->context_long;
    }

    public function setContextLong(?string $context_long): self
    {
        $this->context_long = $context_long;
        return $this;
    }

    /**
     * @return bool
     */
    public function getNotificationsRecipients(): bool
    {
        return $this->notificationsRecipients;
    }

    /**
     * @param bool $notificationsRecipients
     * @return Signature
     */
    public function setNotificationsRecipients(bool $notificationsRecipients): self
    {
        $this->notificationsRecipients = $notificationsRecipients;
        return $this;
    }


    /**
     * @return ProcessStep|null
     */
    public function getProcessStep(): ?ProcessStep
    {
        return $this->processStep;
    }

    /**
     * @param ProcessStep|null $processStep
     * @return Signature
     */
    public function setProcessStep(?ProcessStep $processStep): self
    {
        $this->processStep = $processStep;
        return $this;
    }


    /**
     * @return DateTime|null
     */
    public function getDateCreated(): ?DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param DateTime|null $dateCreated
     * @return Signature
     */
    public function setDateCreated(?DateTime $dateCreated): self
    {
        $this->dateCreated = $dateCreated;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateSend(): ?DateTime
    {
        return $this->dateSend;
    }

    /**
     * @param DateTime|null $dateSend
     * @return Signature
     */
    public function setDateSend(?DateTime $dateSend): self
    {
        $this->dateSend = $dateSend;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     * @return Signature
     */
    public function setType(?string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     * @return Signature
     */
    public function setLabel(?string $label): self
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return Signature
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatus(): int|null
    {
        return $this->status;
    }

    /**
     * @param int|null $status
     * @return Signature
     */
    public function setStatus(int|null $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateUpdate(): ?DateTime
    {
        return $this->dateUpdate;
    }

    /**
     * @param DateTime|null $dateUpdate
     * @return Signature
     */
    public function setDateUpdate(?DateTime $dateUpdate): self
    {
        $this->dateUpdate = $dateUpdate;
        return $this;
    }

    public function getRefusedText(): ?string
    {
        return $this->refused_text;
    }

    public function setRefusedText(?string $refused_text): self
    {
        $this->refused_text = $refused_text;
        return $this;
    }

    /**
     * @return SignatureRecipient[]
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * Retourne la liste des destinaires qui ont signé.
     *
     * @return array
     */
    public function getSignedRecipients(): array
    {
        $signed = [];
        foreach ($this->getRecipients() as $recipient) {
            if ($recipient->isSigned()) {
                $signed[] = $recipient;
            }
        }
        return $signed;
    }

    /**
     * @param array $recipients
     * @return Signature
     */
    public function setRecipients(array $recipients): self
    {
        $this->recipients = new ArrayCollection($recipients);
        return $this;
    }

    /**
     * @param SignatureRecipient $recipient
     * @return Signature
     */
    public function addRecipient(SignatureRecipient $recipient): self
    {
        $recipient->setSignature($this);
        $this->recipients->add($recipient);
        return $this;
    }

    /**
     * @return SignatureObserver[]
     */
    public function getObservers()
    {
        return $this->observers;
    }

    /**
     * @param array $recipients
     * @return Signature
     */
    public function setObservers(array $observers): self
    {
        $this->observers = new ArrayCollection($observers);
        return $this;
    }

    /**
     * @return Signature
     */
    public function addObserver(SignatureObserver $observer): self
    {
        $this->observers->add($observer);
        return $this;
    }

    /**
     * @return string
     */
    public function getDocumentPath(): string
    {
        return $this->document_path;
    }

    /**
     * @param string $document_path
     */
    public function setDocumentPath(string $document_path): self
    {
        $this->document_path = $document_path;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDocumentRemotekey(): ?string
    {
        return $this->document_remotekey;
    }

    /**
     * @param string|null $document_remotekey
     * @return Signature
     */
    public function setDocumentRemotekey(?string $document_remotekey): self
    {
        $this->document_remotekey = $document_remotekey;
        return $this;
    }

    /**
     * @return string
     */
    public function getDocumentLocalkey(): string
    {
        return $this->document_localkey;
    }

    /**
     * @param string $document_localkey
     * @return Signature
     */
    public function setDocumentLocalkey(string $document_localkey): self
    {
        $this->document_localkey = $document_localkey;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLetterfileProcess(): ?string
    {
        return $this->letterfile_process;
    }

    /**
     * @param string|null $letterfile_process
     * @return Signature
     */
    public function setLetterfileProcess(?string $letterfile_process): self
    {
        $this->letterfile_process = $letterfile_process;
        return $this;
    }

    /**
     * @return string
     */
    public function getLetterfileKey(): ?string
    {
        return $this->letterfile_key;
    }

    /**
     * @param string $letterfile_key
     * @return Signature
     */
    public function setLetterfileKey(string $letterfile_key): self
    {
        $this->letterfile_key = $letterfile_key;
        return $this;
    }

    /**
     * @return string
     */
    public function getLetterfileUrl(): string
    {
        return $this->letterfile_url;
    }

    /**
     * @param string $letterfile_url
     * @return Signature
     */
    public function setLetterfileUrl(string $letterfile_url): self
    {
        $this->letterfile_url = $letterfile_url;
        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function isSendable(): bool
    {
        return $this->getStatus() == self::STATUS_SIGNATURE_DRAFT;
    }

    public function isUpdatable(): bool
    {
        return $this->getStatus() == self::STATUS_SIGNATURE_WAIT;
    }

    public function isFinished(): bool
    {
        $deletable = [
            self::STATUS_SIGNATURE_CANCEL,
            self::STATUS_SIGNATURE_DELETE,
            self::STATUS_SIGNATURE_FULLDELETE,
            self::STATUS_SIGNATURE_REJECT,
            self::STATUS_SIGNATURE_SIGNED,
        ];
        return in_array($this->getStatus(), $deletable);
    }

    public function isSend(): bool
    {
        return $this->getDateSend() !== null;
    }

    public function isNotifiableObserver(): bool
    {
        return $this->getObservers() && $this->getStatus() == self::STATUS_SIGNATURE_WAIT;
    }

    public function isNotifiable(): bool
    {
        return $this->getNotificationsRecipients() && $this->getStatus() == self::STATUS_SIGNATURE_WAIT;
    }

    public function isDeletable(): bool
    {
        $deletable = [
            self::STATUS_SIGNATURE_DRAFT,
            self::STATUS_SIGNATURE_WAIT
        ];
        return in_array($this->getStatus(), $deletable);
    }

    public function isDownloadable(): bool
    {
        return $this->getStatus() != self::STATUS_SIGNATURE_FULLDELETE && $this->getStatus(
            ) != self::STATUS_SIGNATURE_DRAFT;
    }

    public function getStatusText(): string
    {
        return self::getStatusLabel($this->getStatus());
    }

    /**
     * @return string[]
     */
    public function getRecipientsEmailsArray(): array
    {
        $out = [];
        foreach ($this->getRecipients() as $recipient) {
            $out[] = $recipient->getEmail();
        }
        return $out;
    }

    public function getVerb() :string {
        return $this->getType() == SignatureConstants::VISA_HIDDEN || SignatureConstants::VISA_VISUAL ? 'viser' : 'signer';
    }

    public function getNoun() :string {
        return $this->getType() == SignatureConstants::VISA_HIDDEN || SignatureConstants::VISA_VISUAL ? 'visa' : 'signature';
    }

    public function getParticiple() :string {
        return $this->getType() == SignatureConstants::VISA_HIDDEN || SignatureConstants::VISA_VISUAL ? 'visé' : 'signé';
    }

    public function __toString(): string
    {
        return sprintf(
            "[%s] signature (%s) : '%s' (%s)",
            $this->getId(),
            $this->getLetterfileKey(),
            $this->getLabel(),
            $this->getStatusText()
        );
    }
}