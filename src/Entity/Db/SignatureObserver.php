<?php

namespace UnicaenSignature\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="UnicaenSignature\Entity\Repository\SignatureObserverRepository")
 * @ORM\Table(name="unicaen_signature_observer")
 */
class SignatureObserver
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="unicaen_signature_observer_id_seq",allocationSize=1, initialValue=1)
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private ?string $firstname;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private ?string $lastname;

    /**
     * @var string
     * @ORM\Column(type="string", length=256)
     */
    private string $email;

    /**
     * @var Signature
     * @ORM\ManyToOne(targetEntity="Signature", inversedBy="observers")
     */
    private Signature $signature;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string|null $firstname
     * @return SignatureObserver
     */
    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string|null $lastname
     * @return SignatureObserver
     */
    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return SignatureObserver
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return Signature
     */
    public function getSignature(): Signature
    {
        return $this->signature;
    }

    /**
     * @param Signature $signature
     * @return SignatureObserver
     */
    public function setSignature(Signature $signature): self
    {
        $this->signature = $signature;
        return $this;
    }

    //////////////////////////////////////////////////////////////////////////////////////
    public function __toString(): string
    {
        return $this->getEmail();
    }

    /**
     * Nom complet (calculé)
     *
     * @return string
     */
    public function getFullname(): string
    {
        return trim($this->getFirstname() . " " . $this->getLastname());
    }

    public function toArray(): array
    {
        return [
            'fullname'  => $this->getFullname(),
            'firstname' => $this->getFirstname(),
            'lastname'  => $this->getLastname(),
            'email'     => $this->getEmail()
        ];
    }
}