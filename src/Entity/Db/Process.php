<?php

namespace UnicaenSignature\Entity\Db;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use UnicaenSignature\Exception\SignatureException;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="UnicaenSignature\Entity\Repository\ProcessRepository")
 * @ORM\Table(name="unicaen_signature_process")
 */
class Process
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="unicaen_signature_process_id_seq",allocationSize=1, initialValue=1)
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var null|DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private null|DateTime $dateCreated = null;

    /**
     * @var null|DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private null|DateTime $lastUpdate = null;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    private int $status;

    /**
     * Liste des contrats de financement pour le projet.
     *
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="ProcessStep", mappedBy="process", cascade={"persist","remove"})
     */
    protected $steps;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    protected int $currentStep = 0;

    /**
     * Label du process issu du SignatureFlow
     *
     * @var ?string
     * @ORM\Column (type="text", nullable=true)
     */
    private ?string $label = null;

    /**
     * @var SignatureFlow
     * @ORM\ManyToOne(targetEntity="SignatureFlow")
     */
    private SignatureFlow $signatureFlow;

    /**
     * @var string
     * @ORM\Column (type="string", nullable=false)
     */
    private string $document_name;

    /**
     * @param $id
     */
    public function __construct()
    {
        $this->steps = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getCurrentStep(): int
    {
        return $this->currentStep;
    }

    /**
     * @param int $currentStep
     */
    public function setCurrentStep(int $currentStep): self
    {
        $this->currentStep = $currentStep;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateCreated(): null|DateTime
    {
        return $this->dateCreated;
    }

    /**
     * @param DateTime|null $dateCreated
     */
    public function setDateCreated(null|DateTime $dateCreated): self
    {
        $this->dateCreated = $dateCreated;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getLastUpdate(): null|DateTime
    {
        return $this->lastUpdate;
    }

    /**
     * @param DateTime|null $lastUpdate
     */
    public function setLastUpdate(null|DateTime $lastUpdate): self
    {
        $this->lastUpdate = $lastUpdate;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    public function getStatusText(): string
    {
        return Signature::getStatusLabel($this->getStatus());
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return ProcessStep[]
     */
    public function getSteps()
    {
        return $this->steps;
    }

    /**
     * @param ArrayCollection $steps
     */
    public function setSteps(ArrayCollection $steps): self
    {
        $this->steps = $steps;
        return $this;
    }


    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }


    /**
     * @return SignatureFlow
     */
    public function getSignatureFlow(): SignatureFlow
    {
        return $this->signatureFlow;
    }

    /**
     * @param SignatureFlow $process
     */
    public function setSignatureFlow(SignatureFlow $signatureFlow): self
    {
        $this->signatureFlow = $signatureFlow;
        return $this;
    }

    /**
     * @return string
     */
    public function getDocumentName(): string
    {
        return $this->document_name;
    }

    /**
     * @param string $document_name
     */
    public function setDocumentName(string $document_name): self
    {
        $this->document_name = $document_name;
        return $this;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function getStep(): ProcessStep
    {
        foreach ($this->getSteps() as $processStep) {
            if ($processStep->getOrder() == $this->getCurrentStep()) {
                return $processStep;
            }
        }
        throw new SignatureException(
            sprintf(
                "Pas d'étape '%s' pour le processus '%s'",
                $this->getCurrentStep(),
                $this
            )
        );
    }

    public function isInProgress(): bool
    {
        return $this->getStatus() == Signature::STATUS_SIGNATURE_WAIT;
    }

    public function isFinished(): bool
    {
        return
            $this->getStatus() != Signature::STATUS_SIGNATURE_WAIT &&
            $this->getStatus() != Signature::STATUS_SIGNATURE_DRAFT &&
            $this->getStatus() != Signature::STATUS_PROCESS_UNCONFIGURED;
    }

    public function isSendable(): bool
    {
        return $this->getStatus() == Signature::STATUS_SIGNATURE_DRAFT;
    }

    public function isTriggerable(): bool
    {
        return $this->getStatus() == Signature::STATUS_SIGNATURE_WAIT;
    }

    public function isUnconfigured(): bool
    {
        return
            $this->getStatus() == Signature::STATUS_PROCESS_UNCONFIGURED;
    }

    public function __toString(): string
    {
        return $this->getLabel() . '(' . $this->getId() . ')';
    }

    public function previousSteps(): array
    {
        $out = [];
        foreach ($this->getSteps() as $step) {
            if( $step->getSignature()->isFinished() ){
                $out[] = $step;
            }
        }

        return $out;
    }

    public function toArray(): array
    {
        $return = [];
        $return['id'] = $this->getId();
        $return['label'] = $this->getLabel();
        $return['lastUpdate'] = $this->getLastUpdate() ? $this->getLastUpdate()->format('Y-m-d H:i:s') : '';
        $return['status'] = $this->getStatus();
        $return['status_text'] = $this->getStatusText();
        $order = 0;
        if ($this->getCurrentStep()) {
            $order = $this->getStep()->getOrder();
        }
        $return['current_step'] = $order;
        $return['total_steps'] = count($this->getSteps());

        $steps = [];

        foreach ($this->getSteps() as $step) {
            $steps[] = $step->toArray();
        }
        $return['steps'] = $steps;
        $return['lastUpdate'] = $this->getLastUpdate() ? $this->getLastUpdate()->format('Y-m-d H:i:s') : '';
        return $return;
    }

}