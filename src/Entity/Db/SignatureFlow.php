<?php

namespace UnicaenSignature\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use UnicaenSignature\Formatter\IObjectFormattable;
use UnicaenSignature\Formatter\ObjectFormatter;
use UnicaenSignature\Utils\SignatureException;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="UnicaenSignature\Entity\Repository\SignatureFlowRepository")
 * @ORM\Table(name="unicaen_signature_signatureflow")
 */
class SignatureFlow implements IObjectFormattable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="unicaen_signature_signatureflow_id_seq",allocationSize=1, initialValue=1)
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $label = null;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $description = null;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="SignatureFlowStep", mappedBy="signatureFlow", cascade={"persist","remove"})
     * @ORM\OrderBy({"order" = "ASC"})
     */
    protected $steps;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
    private bool $enabled = false;

    /**
     * @param ArrayCollection $steps
     */
    public function __construct()
    {
        $this->steps = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     */
    public function setLabel(?string $label): self
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return SignatureFlowStep[]
     */
    public function getSteps()
    {
        return $this->steps;
    }

    public function getStepsIndexed() :array {
        $out = [];
        foreach ($this->getSteps() as $step) {
            $out[$step->getId()] = $step;
        }
        return $out;
    }

    public function getStepAtOrder( int $order ):SignatureFlowStep {
        foreach ($this->getSteps() as $step) {
            if( $step->getOrder() == $order ){
                return $step;
            }
        }
        throw new SignatureException("L'étape '$order' n'existe pas");
    }

    /**
     * @param $steps
     */
    public function setSteps($steps): self
    {
        $this->steps = $steps;
        return $this;
    }

    public function toJson(): array
    {
        return $this->toArray();
    }

    public function toArray(): array
    {
        $objectFormatter = new ObjectFormatter();
        return [
            'id' => $this->getId(),
            'label' => $this->getLabel(),
            'enabled' => $this->isEnabled(),
            'description' => $this->getDescription(),
            'steps' => $objectFormatter->formatCollection($this->getSteps()->toArray(), ObjectFormatter::FORMAT_JSON)
        ];
    }

    public function getOptionLabel(): string
    {
        return $this->getLabel();
    }

    public function __toString(): string
    {
        return $this->getOptionLabel();
    }


}