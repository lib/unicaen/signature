<?php

namespace UnicaenSignature\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="unicaen_signature_process_step")
 */
class ProcessStep
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="unicaen_signature_process_step_id_seq",allocationSize=1, initialValue=1)
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @var Process
     * @ORM\ManyToOne(targetEntity="Process", inversedBy="steps")
     */
    private Process $process;

    /**
     * @var Signature
     * @ORM\OneToOne (targetEntity="Signature", inversedBy="processStep")
     */
    private Signature $signature;


    /**
     * Label de l'étape du process issu du SignatureFlowStep
     *
     * @var ?string
     * @ORM\Column (type="text", nullable=true)
     */
    private ?string $label = null;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Process
     */
    public function getProcess(): Process
    {
        return $this->process;
    }

    /**
     * @param Process $process
     */
    public function setProcess(Process $process): self
    {
        $this->process = $process;
        return $this;
    }

    /**
     * @return Signature
     */
    public function getSignature(): Signature
    {
        return $this->signature;
    }

    /**
     * @param Signature $signature
     */
    public function setSignature(Signature $signature): self
    {
        $this->signature = $signature;
        return $this;
    }

    public function setLabel(?string $label): self {
        $this->label = $label;

        return $this;
    }

    public function getLabel(): ?string {
        return $this->label;
    }


    /**
     * @return int
     */
    public function getOrder(): int|string|null
    {
        return $this->getSignature()->getOrder();
    }



    public function getLetterfileName() :string {
        return $this->getSignature()->getLetterfileKey();
    }

    public function getLevel() :string {
        return $this->getSignature()->getType();
    }

    /**
     * @return SignatureRecipient[]
     */
    public function getRecipients() :array|Collection {
        return $this->signature->getRecipients();
    }

    /**
     * @return int|null
     */
    public function getStatus() {
        return $this->getSignature()->getStatus();
    }

    public function getStatusText() {
        return $this->getSignature()->getStatusText();
    }

    public function getAcceptedByStr() :string {
        $out = [];
        foreach ($this->getAcceptedBy() as $i) {
            $out[] = $i->getDisplayed();
        }
        return implode(', ', $out);
    }

    public function getAcceptedBy() :array {
        $out = [];
        foreach ($this->getSignature()->getSignedRecipients() as $recipient){
            $out[] = $recipient;
        }
        return $out;
    }

    public function getMessage() :string {

        // Refusé
        $sufixe = $this->getSignature()->getStatus() == Signature::STATUS_SIGNATURE_DRAFT ? "A venir" : "En attente";
        if( $this->getSignature()->isFinished() ){
            $sufixe = "Terminé";
            if( $this->getSignature()->getStatus() == Signature::STATUS_SIGNATURE_REJECT ){
                $sufixe = "Rejeté ";
            }
            if( $this->getSignature()->getStatus() == Signature::STATUS_SIGNATURE_SIGNED ){
                $sufixe = $this->getSignature()->getParticiple();
                $sufixe .= " par " . $this->getAcceptedByStr();
            }

        }
        return $this->__toString() . " - " . $sufixe;
    }

    public function toArray() :array {
        $recipients = [];
        foreach ($this->getRecipients() as $recipient){
            $recipients[] = $recipient->toArray();
        }
        $observers = [];
        foreach ($this->getSignature()->getObservers() as $observer){
            $observers[] = $observer->toArray();
        }
        return [
            'label' => $this->getLabel(),
            'letterfile' => $this->getLetterfileName(),
            'level' => $this->getLevel(),
            'order' => $this->getOrder(),
            'status' => $this->getStatus(),
            'status_text' => $this->getStatusText(),
            'refused_text' => $this->getSignature()->getRefusedText(),
            'allSignToComplete' => $this->getSignature()->getAllSignToComplete(),
            'dateSignatureUpdate' => $this->getSignature()->getDateUpdate(),
            'recipients' => $recipients,
            'observers' => $observers
        ];
    }

    public function __toString(): string
    {
        return sprintf("Etape %s : %s",
            $this->getOrder(),
            $this->getLabel()
        );
    }


}