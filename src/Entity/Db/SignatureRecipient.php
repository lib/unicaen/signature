<?php

namespace UnicaenSignature\Entity\Db;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="UnicaenSignature\Entity\Repository\SignatureRecipientRepository")
 * @ORM\Table(name="unicaen_signature_recipient")
 */
class SignatureRecipient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="unicaen_signature_recipient_id_seq",allocationSize=1, initialValue=1)
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @var string|null
     * @ORM\Column(type="integer", nullable=false, options={"default":"101"})
     */
    private ?int $status;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private ?string $firstname;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private ?string $lastname;

    /**
     * @var string
     * @ORM\Column(type="string", length=256)
     */
    private string $email;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private ?string $phone;

    /**
     * Date du dernier changement d'état.
     *
     * @var ?DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTime $dateUpdate = null;

    /**
     * Date d'accomplissement
     *
     * @var ?DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTime $dateFinished = null;

    /**
     * Clef interne d'accès
     *
     * @var ?string
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $keyAccess = null;

    /**
     * Informations / Commentaires
     *
     * @var ?string
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $informations = null;

    /**
     * URL d'accès au document pour signature
     *
     * @var ?string
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $urlDocument = null;

    /**
     * @var Signature
     * @ORM\ManyToOne(targetEntity="Signature", inversedBy="recipients")
     */
    private Signature $signature;

    /**
     * @var bool Permet de "forcer" les notifications
     */
    private bool $forceUpdate = false;

    /**
     * @param string|null $email Courriel du destinataire
     * @param string|null $firstname Prénom
     * @param string|null $lastname Nom
     * @param int|null $status Etat
     */
    public function __construct(
        ?string $email = null,
        ?string $firstname = null,
        ?string $lastname = null,
        ?int $status = Signature::STATUS_SIGNATURE_DRAFT
    ) {
        if($email) $this->setEmail($email);
        if($firstname) $this->setFirstname($firstname);
        if($lastname) $this->setLastname($lastname);
        $this->setStatus($status);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return SignatureRecipient
     */
    public function setStatus(int $status, bool $updateDate = false): self
    {
        if( $updateDate && $status != $this->status ){
            $this->setDateUpdate(new DateTime());
        }
        $this->status = $status;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    /**
     * @param string|null $firstname
     * @return SignatureRecipient
     */
    public function setFirstname(?string $firstname): self
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    /**
     * @param string|null $lastname
     * @return SignatureRecipient
     */
    public function setLastname(?string $lastname): self
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return SignatureRecipient
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateUpdate(): ?DateTime
    {
        return $this->dateUpdate;
    }

    /**
     * @param DateTime|null $dateUpdate
     * @return SignatureRecipient
     */
    public function setDateUpdate(?DateTime $dateUpdate): self
    {
        $this->dateUpdate = $dateUpdate;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateFinished(): ?DateTime
    {
        return $this->dateFinished;
    }

    /**
     * @param DateTime|null $dateFinished
     */
    public function setDateFinished(?DateTime $dateFinished): self
    {
        $this->dateFinished = $dateFinished;
        return $this;
    }

    /**
     * @return Signature
     */
    public function getSignature(): Signature
    {
        return $this->signature;
    }

    /**
     * @param Signature $signature
     * @return SignatureRecipient
     */
    public function setSignature(Signature $signature): self
    {
        $this->signature = $signature;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return SignatureRecipient
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInformations(): ?string
    {
        return $this->informations;
    }

    /**
     * @param string|null $informations
     */
    public function setInformations(?string $informations): self
    {
        $this->informations = $informations;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getKeyAccess(): ?string
    {
        return $this->keyAccess;
    }

    /**
     * @param string|null $keyAccess
     */
    public function setKeyAccess(?string $keyAccess): self
    {
        $this->keyAccess = $keyAccess;
        return $this;
    }

    public function getUrlDocument(): ?string
    {
        return $this->urlDocument;
    }

    public function setUrlDocument(?string $urlDocument): self
    {
        $this->urlDocument = $urlDocument;
        return $this;
    }

    public function isForceUpdate(): bool
    {
        return $this->forceUpdate;
    }

    public function setForceUpdate(bool $forceUpdate): self
    {
        $this->forceUpdate = $forceUpdate;
        return $this;
    }

    //////////////////////////////////////////////////////////////////////////////////////
    public function __toString(): string
    {
        return $this->getEmail();
    }

    /**
     * Nom complet (calculé)
     *
     * @return string
     */
    public function getFullname(): string
    {
        return trim($this->getFirstname() . " " . $this->getLastname());
    }

    public function getDisplayed() :string
    {
        return $this->getFullname()?:$this->getEmail();
    }

    public function isFinished() :bool
    {
        return $this->getSignature()->isFinished() ||
            ($this->getStatus() == Signature::STATUS_SIGNATURE_REJECT || $this->getStatus() == Signature::STATUS_SIGNATURE_SIGNED);
    }

    public function isSigned() :bool {
        return $this->getStatus() == Signature::STATUS_SIGNATURE_SIGNED;
    }

    public function isRefused() :bool {
        return $this->getStatus() == Signature::STATUS_SIGNATURE_REJECT;
    }

    public function isDone() :bool {
        return $this->isSigned() || $this->isRefused();
    }

    public function isNotifiable() :bool
    {
        return $this->getStatus() == Signature::STATUS_SIGNATURE_WAIT;
    }

    /**
     * Retourne true
     * @param string $email
     * @return bool
     */
    public function requireSignFromEmail( string $email ):bool {
        if( $this->isFinished() ){
            return false;
        }
        if( $this->getEmail() == $email && $this->getStatus() == Signature::STATUS_SIGNATURE_WAIT ){
            return true;
        }
        return false;
    }

    public function isSignableInternal() :bool
    {
        return boolval($this->getKeyAccess()) && $this->status == Signature::STATUS_SIGNATURE_WAIT;
    }

    public function getStatusText() :string
    {
        return Signature::getStatusLabel($this->getStatus());
    }

    public function getLabel() :string
    {
        if( $this->getSignature()->getProcessStep() ){
            return
                $this->getSignature()->getLabel()
                . ' > '
                . $this->getSignature()->getProcessStep()->getLabel();

        } else {
            return $this->getSignature()->getLabel();
        }
    }

    /**
     * Retourne le message en cas de signature
     * @return string
     */
    public function forgeMessageSigned() :string
    {
        $who = $this->getFullname() ?: $this->getEmail();
        $at = $this->getDateFinished() ? $this->getDateFinished()->format('Y-m-d H:i:s') : 'void';
        $on = $this->getSignature()->getContextShort() ?: $this->getLabel();
        $verb = $this->getSignature()->getVerb();
        return "$on a été $verb par $who le $at";
    }/**
     * Retourne le message en cas de signature
     * @return string
     */
    public function forgeMessageRefused() :string
    {
        $who = $this->getFullname() ?: $this->getEmail();
        $at = $this->getDateFinished() ? $this->getDateFinished()->format('Y-m-d H:i:s') : 'void';
        $on = $this->getSignature()->getContextShort() ?: $this->getLabel();
        // $verb = $this->getSignature()->getVerb();
        return "$on a été REFUSE par $who le $at";
    }

    public function toArray() :array {
        return [
            'fullname' => $this->getFullname(),
            'firstname' => $this->getFirstname(),
            'lastname' => $this->getLastname(),
            'email' => $this->getEmail(),
            'status' => $this->getStatus(),
            'status_text' => $this->getStatusText(),
            'informations' => $this->getInformations(),
            'dateFinished' => $this->getDateFinished() ? $this->getDateFinished()->format('Y-m-d H:i:s') : null,
        ];
    }
}