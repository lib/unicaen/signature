<?php

namespace UnicaenSignature\Entity\Db;

use Doctrine\ORM\Mapping as ORM;
use UnicaenSignature\Formatter\IObjectFormattable;

/**
 * @ORM\Entity
 * @ORM\Table(name="unicaen_signature_signatureflowstep")
 */
class SignatureFlowStep implements IObjectFormattable
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="unicaen_signature_signatureflowstep_id_seq",allocationSize=1, initialValue=1)
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private ?string $recipientsMethod = null;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private ?string $label = null;

    /**
     * @var string
     * @ORM\Column(type="string", length=256)
     */
    private string $letterfileName;

    /**
     * @var string
     * @ORM\Column(type="string", name="signlevel", length=256)
     */
    private string $level;

    /**
     * @var int
     * @ORM\Column(type="integer", name="ordering")
     */
    private int $order = 0;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":"true"})
     */
    private bool $allRecipientsSign = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":"false"})
     */
    private bool $notificationsRecipients = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean", options={"default":"false"})
     */
    private bool $editableRecipients = false;

    /**
     * @var SignatureFlow
     * @ORM\ManyToOne(targetEntity="SignatureFlow", inversedBy="steps")
     */
    private SignatureFlow $signatureFlow;

    /**
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $options = null;

    /**
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $observers_options = null;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private ?string $observersMethod;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return SignatureFlowStep
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRecipientsMethod(): ?string
    {
        return $this->recipientsMethod;
    }

    /**
     * @param string|null $recipientsMethod
     * @return SignatureFlowStep
     */
    public function setRecipientsMethod(?string $recipientsMethod): self
    {
        $this->recipientsMethod = $recipientsMethod;
        return $this;
    }

    /**
     * @return array
     */
    public function getObserversOptions(): array
    {
        if ($this->observers_options) {
            return json_decode($this->observers_options, true);
        }
        else {
            return [];
        }
    }

    /**
     * @param array|null $observers_options
     * @return SignatureFlowStep
     */
    public function setObserversOptions(?array $observers_options): self
    {
        if ($observers_options) {
            $this->observers_options = json_encode($observers_options);
        }
        else {
            $this->observers_options = "";
        }
        return $this;
    }

    /**
     * @return string|null
     */
    public function getObserversMethod(): ?string
    {
        return $this->observersMethod;
    }

    /**
     * @param string|null $observersMethod
     * @return SignatureFlowStep
     */
    public function setObserversMethod(?string $observersMethod): self
    {
        $this->observersMethod = $observersMethod;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEditableRecipients(): bool
    {
        return $this->editableRecipients;
    }

    /**
     * @param bool $editableRecipients
     * @return SignatureFlowStep
     */
    public function setEditableRecipients(bool $editableRecipients): self
    {
        $this->editableRecipients = $editableRecipients;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     * @return SignatureFlowStep
     */
    public function setLabel(?string $label): self
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getLetterfileName(): string
    {
        return $this->letterfileName;
    }

    /**
     * @param string $letterfileName
     * @return SignatureFlowStep
     */
    public function setLetterfileName(string $letterfileName): self
    {
        $this->letterfileName = $letterfileName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLevel(): string
    {
        return $this->level;
    }

    /**
     * @param string $level
     * @return SignatureFlowStep
     */
    public function setLevel(string $level): self
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     * @return SignatureFlowStep
     */
    public function setOrder(int $order): self
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return SignatureFlow
     */
    public function getSignatureFlow(): SignatureFlow
    {
        return $this->signatureFlow;
    }

    /**
     * @param SignatureFlow $signatureFlow
     * @return SignatureFlowStep
     */
    public function setSignatureFlow(SignatureFlow $signatureFlow): self
    {
        $this->signatureFlow = $signatureFlow;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAllRecipientsSign(): bool
    {
        return $this->allRecipientsSign;
    }

    /**
     * @param bool $allRecipientsSign
     * @return SignatureFlowStep
     */
    public function setAllRecipientsSign(bool $allRecipientsSign): self
    {
        $this->allRecipientsSign = $allRecipientsSign;
        return $this;
    }

    /**
     * @return bool
     */
    public function isNotificationsRecipients(): bool
    {
        return $this->notificationsRecipients;
    }

    /**
     * @param bool $notificationsRecipients
     * @return SignatureFlowStep
     */
    public function setNotificationsRecipients(bool $notificationsRecipients): self
    {
        $this->notificationsRecipients = $notificationsRecipients;
        return $this;
    }

    public function toJson(): array
    {
        return $this->toArray();
    }

    public function toArray(): array
    {
        return [
            "id"                      => $this->getId(),
            "label"                   => $this->getLabel(),
            "level"                   => $this->getLevel(),
            "order"                   => $this->getOrder(),
            "letterfile"              => $this->getLetterfileName(),
            "allRecipientsSign"       => $this->isAllRecipientsSign(),
            "notificationsRecipients" => $this->isNotificationsRecipients(),
            "editable"                => $this->isEditableRecipients(),
            "method"                  => $this->getRecipientsMethod(),
            "options"                 => $this->getOptions(),
            "observers_method"        => $this->getObserversMethod(),
            "observers_options"       => $this->getObserversOptions()
        ];
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        if ($this->options) {
            return json_decode($this->options, true);
        }
        else {
            return [];
        }
    }

    /**
     * @param array|null $options
     * @return SignatureFlowStep
     */
    public function setOptions(?array $options): self
    {
        if ($options) {
            $this->options = json_encode($options);
        }
        else {
            $this->options = "";
        }
        return $this;
    }


    public function getOptionLabel(): string
    {
        return $this->getLabel();
    }

}