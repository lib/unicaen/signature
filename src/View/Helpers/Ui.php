<?php

namespace UnicaenSignature\View\Helpers;

use Laminas\View\Helper\AbstractHtmlElement;
use UnicaenSignature\Exception\SignatureException;

class Ui extends AbstractHtmlElement
{

    /** Configuration */
    private $manifest;

    private $basePath;

    private $buildedJs;

    private $buildedCss;

    private $mode;

    public function __construct(string $mode = 'prod', string $pathRoot = 'dist/', string $baseUrl = '/dist/')
    {
        $this->mode = $mode;
        $this->basePath = $pathRoot;
        $this->baseUrl = $baseUrl;
        $this->buildedJs = [];
        $this->buildedCss = [];

        $this->initManifest();
    }

    private function initManifest()
    {
        if ($this->mode == 'prod') {
            $this->manifest = json_decode(file_get_contents(__DIR__ . '/../../../public/dist/manifest.json'), true);
        }
    }

    protected function getServeurBasePath() :string {
        if ($this->mode != 'prod') {
        }
    }

    public function build(string $script)
    {
        if (in_array($script, $this->buildedJs)) {
            return;
        }

        $css_import = [];
        $server = '';

        if ($this->mode != 'prod') {
            $server = 'http://localhost:5173/';
            $jsFile = $script;
        } else {
            $server = '/unicaen/signature/';
            if (!array_key_exists($script, $this->manifest)) {
                throw new SignatureException("Fichier manquant : le script '$script' est absent du manifeste");
            }

            $conf = $this->manifest[$script];

            if (array_key_exists('imports', $conf)) {
                foreach ($conf['imports'] as $import) {
                    $this->build($import);
                }
            }
            if (array_key_exists('css', $conf)) {
                foreach ($conf['css'] as $css) {
                    if (in_array($css, $this->buildedCss)) {
                        continue;
                    }
                    $this->buildedCss[] = $css;
                    $css_import[] = $css;
                }
            }

            $jsFile = $conf['file'];
        }
        $url = $jsFile;
        foreach ($css_import as $css) {
            echo '<link rel="stylesheet" href="'.$server.  $css . '" />';
        }

        if(preg_match_all('/css$/m', $url, $matches, PREG_SET_ORDER, 0)){
            if ($this->mode != 'prod') {
                echo '<script type="module" src="'.$server. $url . '"></script>';
            } else {
                echo '<link rel="stylesheet" href="' . $server . $url . '" />';
            }
        } else {
            echo '<script type="module" src="'.$server. $url . '"></script>';
        }

    }

    public function addJs(string $file): void
    {
        $this->build($file);
    }

    public function staticCss( array|string $urls) :void {
        if( is_string($urls) ){
            $paths = [$urls];
        } else {
            $paths = $urls;
        }
        $server = '/unicaen/signature/';
        foreach ($paths as $url) {
            echo '<link rel="stylesheet" href="' . $server . $url . '" />';
        }
    }

}
