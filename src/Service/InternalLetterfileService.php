<?php

namespace UnicaenSignature\Service;

use DateTime;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Exception;
use UnicaenSignature\Entity\Db\Signature;
use UnicaenSignature\Entity\Db\SignatureRecipient;
use UnicaenSignature\Entity\Repository\SignatureRepository;
use UnicaenSignature\Exception\SignatureException;

class InternalLetterfileService
{
    use
        LoggerServiceAwareTrait,
        ProvidesObjectManager,
        SharedEventManagerAwareTrait,
        SignatureConfigurationServiceAwareTrait,
        SignatureServiceAwareTrait;

    /**
     * @return SignatureRepository
     */
    protected function getSignatureRepository(): SignatureRepository
    {
        return $this->getObjectManager()->getRepository(Signature::class);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Retourne le SignatureRecipient via la clef.
     *
     * @param string $key
     *
     * @return SignatureRecipient
     * @throws SignatureException
     */
    public function getSignatureRecipientByKey(string $key): SignatureRecipient
    {
        try {
            return $this->getObjectManager()->getRepository(SignatureRecipient::class)
                ->findOneBy(['keyAccess' => $key]);
        } catch (Exception $e) {
            $this->getLoggerService()->errorLogAndThrow($e, "Impossible de charger les informations, clef invalide");
        }
    }

    /**
     * Retourne les informations sur le document à signer pour le signataire donné.
     *
     * @param SignatureRecipient $signatureRecipient
     *
     * @return array
     * @throws SignatureException
     */
    public function getDocumentByRecipient(SignatureRecipient $signatureRecipient): array
    {
        return $this->getSignatureService()->getDocumentData($signatureRecipient->getSignature());
    }

    /**
     * Valide le document.
     *
     * @param SignatureRecipient $signatureRecipient
     * @param string $comment
     *
     * @return void
     * @throws SignatureException
     */
    public function accept(SignatureRecipient $signatureRecipient, string $comment): void
    {
        $this->updateStatusSignatureRecipient($signatureRecipient, Signature::STATUS_SIGNATURE_SIGNED, $comment);
    }

    /**
     * @param SignatureRecipient $signatureRecipient
     * @param string $comment
     *
     * @return void
     * @throws SignatureException
     */
    public function refuse(SignatureRecipient $signatureRecipient, string $comment): void
    {
        $this->updateStatusSignatureRecipient($signatureRecipient, Signature::STATUS_SIGNATURE_REJECT, $comment);
    }

    /**
     * @param SignatureRecipient $signatureRecipient
     * @param int $status
     * @param string $comment
     * @return void
     * @throws SignatureException
     */
    protected function updateStatusSignatureRecipient(
        SignatureRecipient $signatureRecipient,
        int $status,
        string $comment
    ): void {
        if ($signatureRecipient->isFinished()) {
            throw new SignatureException("Le document a déjà été visé");
        }

        try {
            $signatureRecipient->getSignature()->setDateUpdate(new DateTime());
            $signatureRecipient->setForceUpdate(true);
            $signatureRecipient->setStatus($status)
                ->setInformations($comment)
                ->setDateUpdate(new DateTime())
                ->setDateFinished(new DateTime());
            $this->getObjectManager()->flush();
            $this->getSignatureService()->updateStatusSignature($signatureRecipient->getSignature());
        } catch (Exception $e) {
            $this->getLoggerService()->errorLogAndThrow($e, 'Impossible de viser le document');
        }
    }
}