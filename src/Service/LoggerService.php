<?php

namespace UnicaenSignature\Service;

use Exception;
use Monolog\Handler\FilterHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use UnicaenSignature\Exception\SignatureException;

class LoggerService
{
    use ServiceContainerAwareTrait;

    private array $config;
    private ?Logger $logger = null;
    private ?Logger $customLogger = null;

    const NO_LOGGER = 0;

    /** @var array  */
    private array $handlers;

    /**
     * @param array $config
     * @return void
     */
    public function configure(array $config): void
    {
        $level = $config['level'];
        $permission = $config['file_permission'];
        $this->handlers = [];

        if( $level === self::NO_LOGGER ){
            return;
        }

        if( $config['customLogger'] ){
            $this->customLogger = $this->getServiceContainer()->get($config['customLogger']);
        }
        $this->logger = new Logger('unicaen-signature');
//
        if( $config['enable'] === true ){
            $path = $config['file'];
            $fileHandler = new StreamHandler($path, $level, true, $permission);
            $this->handlers[] = $fileHandler;
            $this->logger->pushHandler($fileHandler);
        }
        /***
        "/home/bouvry/Projects/Unicaen/OscarProject/Ripley/oscar/config/autoload../../logs" a
         */

        // Sortie standard (Built-in server)
        if( $config['stdout'] === true ){
            $stout = new StreamHandler('php://stdout', $level);
            $this->handlers[] = $stout;
            $this->logger->pushHandler($stout);
        }
    }

    public function setVerbosity(int $verbosity) {
        if( $this->logger ){
            switch ($verbosity) {
                case OutputInterface::VERBOSITY_QUIET;
                    $level = Logger::ERROR;
                    break;
                case OutputInterface::VERBOSITY_NORMAL;
                    $level = Logger::WARNING;
                    break;
                case OutputInterface::VERBOSITY_VERBOSE;
                    $level = Logger::NOTICE;
                    break;
                case OutputInterface::VERBOSITY_VERY_VERBOSE;
                    $level = Logger::INFO;
                    break;
                case OutputInterface::VERBOSITY_DEBUG;
                    $level = Logger::DEBUG;
                    break;
                default:
                    throw new Exception("Verbosity level unknow $verbosity");
            }
            foreach ($this->handlers as $handler) {
                $handler->setLevel($level);
            }
        }
    }

    /**
     * @param Exception $exception
     * @param $level
     * @return void
     * @throws Exception
     */
    public function throwLoggedException( Exception $exception, $level = Logger::ERROR ):void {
        $this->log($exception->getMessage(), $level);
        throw $exception;
    }


    /**
     * @param Exception $e
     * @param string $message
     * @return void
     * @throws SignatureException
     */
    public function errorLogAndThrow( Exception $e, string $message ):void {
        $this->error($message . " : " . $e->getMessage());
        throw new SignatureException($message);
    }


    /**
     * @param string $msg
     * @param string $level
     * @return void
     */
    protected function log( string $msg, string $level ):void
    {
        $this->logger?->log($level, $msg);
        $this->customLogger?->log($level, '[signature] '.$msg);
    }

    /**
     * @param string $msg
     * @return void
     */
    public function debug( string $msg ):void
    {
        $this->log($msg, Logger::DEBUG);
    }

    /**
     * @param string $msg
     * @return void
     */
    public function info( string $msg ):void
    {
        $this->log($msg, Logger::INFO);
    }

    /**
     * @param string $msg
     * @return void
     */
    public function notice( string $msg ):void
    {
        $this->log($msg, Logger::NOTICE);
    }

    /**
     * @param string $msg
     * @return void
     */
    public function warning( string $msg ):void
    {
        $this->log($msg, Logger::WARNING);
    }

    /**
     * @param string $msg
     * @return void
     */
    public function error( string $msg ):void
    {
        $this->log($msg, Logger::ERROR);
    }

    /**
     * @param string $msg
     * @return void
     */
    public function critical( string $msg ):void
    {
        $this->log($msg, Logger::CRITICAL);
    }

    /**
     * @param string $msg
     * @return void
     */
    public function alert( string $msg ):void
    {
        $this->log($msg, Logger::ALERT);
    }

    /**
     * @param string $msg
     * @return void
     */
    public function emergency( string $msg ):void
    {
        $this->log($msg, Logger::EMERGENCY);
    }
}