<?php

namespace UnicaenSignature\Service;

use Exception;
use UnicaenSignature\Strategy\Letterfile\ILetterfileStrategy;
use UnicaenSignature\Exception\SignatureException;

/**
 * Cette classe gère l'accès aux différents parapheurs configurés.
 */
class LetterfileService
{
    use SignatureConfigurationServiceAwareTrait,
        ServiceContainerAwareTrait,
        LoggerServiceAwareTrait;

    /**
     * @var ILetterfileStrategy[]|null
     */
    private ?array $letterFileStrategy = null;

    /**
     * @param string $name
     * @return array
     * @throws SignatureException
     */
    private function getConfigByName(string $name): array
    {
        foreach ($this->getSignatureConfigurationService()->getLetterfileConfiguration() as $lfConfig) {
            if ($lfConfig['name'] == $name) {
                return $lfConfig;
            }
        }
        throw new SignatureException("Configuration du parapheur '$name' inconnue");
    }

    /**
     * @param string|null $name
     * @return ILetterfileStrategy
     * @throws SignatureException
     */
    public function getLetterFileStrategy(?string $name): ILetterfileStrategy
    {
        if ($name === null) {
            return $this->getDefaultLetterFileStrategy();
        }

        if ($this->letterFileStrategy === null) {
            $this->letterFileStrategy = [];
        }

        if (!array_key_exists($name, $this->letterFileStrategy)) {
            try {
                // Create instance
                $config = $this->getConfigByName($name);
                $class = $config['class'];
                /** @var ILetterfileStrategy $letterFile */
                $letterFile = new $class;
                $letterFile->setServiceContainer($this->getServiceContainer());
                $letterFile->setLoggerService($this->getLoggerService());
                $letterFile->setSignatureConfigurationService($this->getSignatureConfigurationService());
                $letterFile->setConfig($config);
                $this->letterFileStrategy[$name] = $letterFile;
            } catch (Exception $e) {
                $msg = "Impossible de charger le parapheur '$name'";
                $this->getLoggerService()->critical($msg . " : " . $e->getMessage());
                throw new SignatureException($msg);
            }
        }

        return $this->letterFileStrategy[$name];
    }

    /**
     * @return ILetterfileStrategy
     * @throws SignatureException
     */
    public function getDefaultLetterFileStrategy(): ILetterfileStrategy
    {
        foreach ($this->getSignatureConfigurationService()->getLetterfileConfiguration() as $lfConfig) {
            if ($lfConfig['default']) {
                return $this->getLetterFileStrategy($lfConfig['name']);
            }
        }
        throw new SignatureException("Aucun parafeur n'est définit par défaut");
    }

    public function getLetterfileInformations(): array
    {
        return [
            "someinfo" => "Here"
        ];
    }
}