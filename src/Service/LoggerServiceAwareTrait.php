<?php

namespace UnicaenSignature\Service;

trait LoggerServiceAwareTrait {

    private LoggerService $loggerService;

    /**
     * @return LoggerService
     */
    public function getLoggerService(): LoggerService
    {
        return $this->loggerService;
    }

    /**
     * @param LoggerService $loggerService
     * @return void
     */
    public function setLoggerService(LoggerService $loggerService): void
    {
        $this->loggerService = $loggerService;
    }

}