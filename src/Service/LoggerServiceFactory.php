<?php

namespace UnicaenSignature\Service;

use Psr\Container\ContainerInterface;

class LoggerServiceFactory
{
    public function __invoke(ContainerInterface $container): LoggerService
    {
        $s = new LoggerService();

        $s->setServiceContainer($container);
        /** @var SignatureConfigurationService $configuration */
        $configuration = $container->get(SignatureConfigurationService::class);
        $s->configure($configuration->getLoggerConfiguration());
        return $s;
    }
}