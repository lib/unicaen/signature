<?php

namespace UnicaenSignature\Service;

trait SignatureConfigurationServiceAwareTrait {

    private SignatureConfigurationService $signatureConfigurationService;

    /**
     * @return SignatureConfigurationService
     */
    public function getSignatureConfigurationService(): SignatureConfigurationService
    {
        return $this->signatureConfigurationService;
    }

    /**
     * @param SignatureConfigurationService $signatureConfigurationService
     * @return void
     */
    public function setSignatureConfigurationService(SignatureConfigurationService $signatureConfigurationService): void
    {
        $this->signatureConfigurationService = $signatureConfigurationService;
    }

}