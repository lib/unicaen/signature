<?php

namespace UnicaenSignature\Service;

use UnicaenSignature\Service\Signature\Configuration;

trait LetterfileServiceAwareTrait {

    private LetterfileService $letterfileService;

    /**
     * @return LetterfileService
     */
    public function getLetterfileService(): LetterfileService
    {
        return $this->letterfileService;
    }

    /**
     * @param LetterfileService $letterfileService
     * @return void
     */
    public function setLetterfileService(LetterfileService $letterfileService): void
    {
        $this->letterfileService = $letterfileService;
    }

}