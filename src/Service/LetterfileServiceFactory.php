<?php

namespace UnicaenSignature\Service;

use Psr\Container\ContainerInterface;

class LetterfileServiceFactory
{


    public function __invoke(ContainerInterface $container): LetterfileService
    {
        $s = new LetterfileService();
        $s->setServiceContainer($container);
        $s->setLoggerService($container->get(LoggerService::class));
        $s->setSignatureConfigurationService($container->get(SignatureConfigurationService::class));
        return $s;
    }
}