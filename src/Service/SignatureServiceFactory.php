<?php

namespace UnicaenSignature\Service;

use Doctrine\ORM\EntityManager;
use Laminas\EventManager\SharedEventManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class SignatureServiceFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SignatureService
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new SignatureService();
        $service->setSharedEventManager($container->get(SharedEventManager::class));
        $service->setServiceContainer($container);
        $service->setObjectManager($entityManager);
        $service->setSignatureConfigurationService($container->get(SignatureConfigurationService::class));
        $service->setLoggerService($container->get(LoggerService::class));
        $service->setLetterfileService($container->get(LetterfileService::class));

        return $service;
    }
}