<?php

namespace UnicaenSignature\Service;

trait ProcessServiceAwareTrait {

    private ProcessService $processService;

    /**
     * @return ProcessService
     */
    public function getProcessService(): ProcessService
    {
        return $this->processService;
    }

    /**
     * @param ProcessService $processService
     * @return void
     */
    public function setProcessService(ProcessService $processService): void
    {
        $this->processService = $processService;
    }

}