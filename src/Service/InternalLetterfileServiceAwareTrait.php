<?php

namespace UnicaenSignature\Service;

trait InternalLetterfileServiceAwareTrait {

    private InternalLetterfileService $internalLetterfileService;

    /**
     * @return InternalLetterfileService
     */
    public function getInternalLetterfileService(): InternalLetterfileService
    {
        return $this->internalLetterfileService;
    }

    /**
     * @param InternalLetterfileService $internalLetterfileService
     * @return void
     */
    public function setInternalLetterfileService(InternalLetterfileService $internalLetterfileService): void
    {
        $this->internalLetterfileService = $internalLetterfileService;
    }

}