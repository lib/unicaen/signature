<?php

namespace UnicaenSignature\Service;

use Psr\Container\ContainerInterface;

trait ServiceContainerAwareTrait {

    private ContainerInterface $serviceContainer;

    public function getServiceContainer() :ContainerInterface
    {
        return $this->serviceContainer;
    }

    public function setServiceContainer(ContainerInterface $sc) :void
    {
        $this->serviceContainer = $sc;
    }
}