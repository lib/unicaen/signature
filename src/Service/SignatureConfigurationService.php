<?php

namespace UnicaenSignature\Service;

use UnicaenSignature\Entity\Data\LevelInfo;
use UnicaenSignature\Utils\Configuration;
use UnicaenSignature\Exception\SignatureException;

class SignatureConfigurationService
{
    use LoggerServiceAwareTrait, ServiceContainerAwareTrait;

    /// ERRORS
    const ERR_CONFIG_LEVELS = "CONFIG ERROR : Les niveaux de signatures ne sont pas configurés.";
    const ERR_CONFIG_LEVEL = "CONFIG ERROR : Le niveau de signature '%s' n'existe pas.";

    private Configuration $configuration;

    public function __construct($container, Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    public function getMessage(): string
    {
        return $this->configuration->getConfiguration('unicaen-signature.message');
    }

    public function getLetterfileConfiguration(): array
    {
        return $this->configuration->getConfiguration('unicaen-signature.letterfiles');
    }

    public function getDocumentsLocation( bool $checkWritable = true ):string
    {
        $path = $this->configuration->getConfiguration('unicaen-signature.documents_path');
        if( $checkWritable ){
            if( !is_writable($path) ) {
                throw new SignatureException("Le dossier pour stoquer les documents est inaccessible en écriture");
            }
        }
        return $path;
    }

    /**
     * @return string|null
     */
    public function getCurrentUserMail() :?string
    {
        try {
            $call = $this->configuration->getConfiguration('unicaen-signature.current_user');
            if( $call && is_callable($call) ){
                return $call($this->getServiceContainer());
            }
        } catch (\Exception $e) {

        }
        return null;
    }

    /**
     * @return string[]
     */
    public function getDocumentMimes() :array
    {
        return $this->configuration->getConfiguration('unicaen-signature.documents_mimes');
    }

    public function getLoggerConfiguration(): array
    {
        return $this->configuration->getConfiguration('unicaen-signature.logger');
    }

    public function getLetterFiles() :array
    {
        $out = [];
        foreach ($this->getLetterfileConfiguration() as $letterFileConfiguration) {
            $key = $letterFileConfiguration['name'];
            $levelsInfos = $this->getLetterFileLevelsInfos($letterFileConfiguration['levels']);
            $letterFileConfiguration['levels_infos'] = $levelsInfos;
            $out[$key] = $letterFileConfiguration;
        }
        return $out;
    }

    protected function getLetterFileLevelsInfos( array $keys ){
        $out = [];
        foreach ($keys as $name=>$keyInLetterFile) {
            /** @var LevelInfo $data */
            $data = $this->getLevelByName($name)->toArray();
            $data['key_in_letterfile'] = $keyInLetterFile;
            $out[$name] = $data;
        }
        return $out;
    }

    /**
     * Retourne la liste des niveaux de signatures possibles.
     *
     * @return array
     * @throws SignatureException
     */
    public function getLevels() :array
    {
        static $_levels;
        if( $_levels === null ){
            try {
                $levels = $this->configuration->getConfiguration('unicaen-signature.signature_levels');
            } catch (\Exception $e) {
                throw new SignatureException(self::ERR_CONFIG_LEVELS);
            }
            foreach ($levels as $key => $levelConfig) {
                $_levels[$key] = new LevelInfo($levelConfig, $key);
            }
        }
        return $_levels;
    }

    /**
     * Retourne la liste des méthodes d'obtention des destinataires.
     *
     * @return array
     * @throws \Exception
     */
    public function getRecipientsMethods() :array
    {
        $methods = $this->configuration->getConfiguration('unicaen-signature.get_recipients_methods');
        $return = [];
        foreach ($methods as $method) {
            if(array_key_exists('options', $method)) {
                foreach ($method['options'] as &$option) {
                    if (is_callable($option['values'])) {
                        $values           = $option['values']($this->serviceContainer);
                        $option['values'] = $values;
                    }
                }
            }
            $return[] = $method;
        }
        return $return;
    }

    public function getMethodByKey( string $key ):array {
        foreach ($this->getRecipientsMethods() as $method) {
            if($method['key'] == $key){
                return $method;
            }
        }
        return [];
    }

    public function getDefaultMethodOptionsByKey(string $key) :array {
        $method = $this->getMethodByKey($key);
        $options = [];
        if( $method['options'] ){
            foreach ($method['options'] as $opt) {
                $value = null;
                switch ($opt['type']) {
                    case 'checkbox':
                        $value = [];
                        break;
                }
                $options[$opt['key']] = $value;
            }
        }
        return $options;
    }

    /**
     * @param string $name
     * @return array
     * @throws SignatureException
     */
    public function getLevelByName(string $name): LevelInfo
    {
        $levels = $this->getLevels();
        if( array_key_exists($name, $levels) ){
            return $levels[$name];
        }
        throw new SignatureException(sprintf(self::ERR_CONFIG_LEVEL, $name));
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getNotificationsBaseUrl() :string
    {
        return $this->configuration->getConfiguration('unicaen-signature.notifications_messages.base_url');
    }

    public function getNotificationStrategy(): array
    {
        return $this->configuration->getConfiguration('unicaen-signature.notification_strategy');
    }

    //////////////////////////////////// TODO
    public function getNotificationsMessageRecipientSend() :string
    {
        return $this->configuration->getConfiguration('unicaen-signature.notifications_messages.recipient_send');
    }

}