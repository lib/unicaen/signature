<?php

namespace UnicaenSignature\Service;

use Doctrine\ORM\EntityManager;
use Laminas\EventManager\SharedEventManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class InternalLetterfileServiceFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): InternalLetterfileService
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new InternalLetterfileService();
        $service->setSharedEventManager($container->get(SharedEventManager::class));
        $service->setObjectManager($entityManager);
        $service->setSignatureConfigurationService($container->get(SignatureConfigurationService::class));
        $service->setLoggerService($container->get(LoggerService::class));
        $service->setSignatureService($container->get(SignatureService::class));

        return $service;
    }
}