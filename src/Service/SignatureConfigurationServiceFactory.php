<?php

namespace UnicaenSignature\Service;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenSignature\Utils\Configuration;

class SignatureConfigurationServiceFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): SignatureConfigurationService
    {
        $s = new SignatureConfigurationService(
            $container,
            new Configuration($container->get('Config'))
        );
        $s->setServiceContainer($container);
        return $s;
    }
}