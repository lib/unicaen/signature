<?php

namespace UnicaenSignature\Service;

use Laminas\EventManager\SharedEventManager;

trait SharedEventManagerAwareTrait {

    private SharedEventManager $sharedEventManager;

    /**
     * @return SharedEventManager
     */
    public function getSharedEventManager(): SharedEventManager
    {
        return $this->sharedEventManager;
    }

    /**
     * @param SharedEventManager $sharedEventManager
     * @return void
     */
    public function setSharedEventManager(SharedEventManager $sharedEventManager): void
    {
        $this->sharedEventManager = $sharedEventManager;
    }

}