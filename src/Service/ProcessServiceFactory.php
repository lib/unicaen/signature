<?php

namespace UnicaenSignature\Service;

use Doctrine\ORM\EntityManager;
use Laminas\EventManager\SharedEventManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class ProcessServiceFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ProcessService
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new ProcessService();
        $service->setSharedEventManager($container->get(SharedEventManager::class));
        $service->setObjectManager($entityManager);
        $service->setSignatureConfigurationService($container->get(SignatureConfigurationService::class));
        $service->setLoggerService($container->get(LoggerService::class));
        $service->setSignatureService($container->get(SignatureService::class));

        return $service;
    }
}