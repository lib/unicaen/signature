<?php

namespace UnicaenSignature\Provider;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class SignaturePrivileges extends Privileges
{
    const SIGNATURE_INDEX = 'SIGNATURE-SIGNATURE_INDEX';
    const SIGNATURE_CREATE = 'SIGNATURE-SIGNATURE_CREATE';
    const SIGNATURE_DELETE = 'SIGNATURE-SIGNATURE_DELETE';
    const SIGNATURE_DELETE_FINISHED = 'SIGNATURE-SIGNATURE_DELETE_FINISHED';
    const SIGNATURE_SYNC = 'SIGNATURE-SIGNATURE_SYNC';
    const SIGNATURE_ADMIN = 'SIGNATURE-SIGNATURE_ADMIN';

    /**
     * Configuration des processus de signature
     */
    const SIGNATURE_ADMIN_CONFIG = 'SIGNATURE-SIGNATURE_ADMIN_CONFIG';
}