<?php

namespace UnicaenSignature\Event;

use Laminas\EventManager\Event;

class ProcessEvent extends Event
{
    const EVENT_TYPE_START = 'process-start';
    const EVENT_TYPE_STEP = 'process-step';
    const EVENT_TYPE_SIGNED = 'process-signed';
    const EVENT_TYPE_REJECTED = 'process-rejected';
}