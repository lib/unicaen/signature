<?php

namespace UnicaenSignature\Event;

use Laminas\EventManager\Event;

class SignatureEvent extends Event
{
    const EVENT_TYPE_ERROR = 'signature-error';
    const EVENT_TYPE_SIGNED = 'signature-signed';
    const EVENT_TYPE_SEND = 'signature-send';
    const EVENT_TYPE_STATUS = 'signature-status';
    const EVENT_TYPE_REJECTED = 'signature-rejected';

    const EVENT_TYPE_RECIPIENT_SIGNED = 'signature-recipient-signed';
    const EVENT_TYPE_RECIPIENT_REJECTED = 'signature-recipient-rejected';
    const EVENT_TYPE_RECIPIENT_CANCEL = 'signature-recipient-cancel';
}