<?php

namespace UnicaenSignature\Formatter;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class ObjectFormatter
{
    const FORMAT_ARRAY = 'FORMAT_ARRAY';
    const FORMAT_JSON = 'FORMAT_JSON';
    const FORMAT_OBJECT = 'FORMAT_OBJECT';
    const FORMAT_OPTIONS = 'FORMAT_OPTIONS';

    /**
     * @param array $objects
     * @param string $format
     * @return array
     */
    public function formatCollection( array $objects, string $format = self::FORMAT_OBJECT ):array {
        if( $format === self::FORMAT_OBJECT ){
            return $objects;
        }
        $out = [];

        /** @var IObjectFormattable $object */
        foreach ($objects as $object) {
            switch ($format){
                case self::FORMAT_OPTIONS:
                    $out[$object->getId()] = $object->getOptionLabel();
                    break;
                case self::FORMAT_ARRAY:
                    $out[$object->getId()] = $object->toArray();
                    break;
                case self::FORMAT_JSON:
                    $out[] = $object->toJson();
                    break;
            }
        }
        return $out;
    }
}