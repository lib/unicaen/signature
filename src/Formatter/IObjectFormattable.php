<?php

namespace UnicaenSignature\Formatter;

interface IObjectFormattable
{
    public function getId() :int;
    public function toJson(): array;
    public function toArray(): array;
    public function getOptionLabel(): string;
}