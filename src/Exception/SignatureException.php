<?php

namespace UnicaenSignature\Exception;

use Exception;

/**
 * Erreur générique du module de signature
 */
class SignatureException extends Exception
{

}