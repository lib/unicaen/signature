<?php

namespace UnicaenSignature\Exception;

/**
 * Erreur générique du module de signature
 */
class SignatureNotImplementedException extends SignatureException
{

}