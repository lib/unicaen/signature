<?php

namespace UnicaenSignature\Exception;

/**
 * Erreur générique du module de signature
 */
class SignatureDataMissingException extends SignatureException
{

}