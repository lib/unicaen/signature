<?php

namespace UnicaenSignature\Exception;

/**
 * Erreur générique du module de signature
 */
class SignatureCurlException extends SignatureException
{

}