<?php

namespace UnicaenSignature\Form;

use Laminas\Form\Element\Checkbox;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use UnicaenSignature\Service\SignatureServiceAwareTrait;
use UnicaenSignature\Utils\ObjectFormatter;

class TypeSignatureForm extends Form
{
    use SignatureServiceAwareTrait;

    public function init(): void
    {
        $this->add([
                       'type' => Text::class,
                       'name' => 'label',
                       'options' => [
                           'label' => 'Intitulé',
                           'empty_option' => 'Intitulé affiché pour ce type de signature',
                       ]
                   ]);

        $this->add([
                       'type' => Text::class,
                       'name' => 'description',
                       'options' => [
                           'label' => 'Description',
                           'empty_option' => 'Information sur le type de signature',
                       ]
                   ]);

        $this->add([
                       'type' => Checkbox::class,
                       'name' => 'inLetterFile',
                       'options' => [
                           'label' => 'Passe par le parafeur',
                       ]
                   ]);

        $this->add([
                       'type' => Text::class,
                       'name' => 'keyInLetterFile',
                       'options' => [
                           'label' => 'Clef dans le parafeur',
                           'empty_option' => 'Identification du type de signature dans le parafeur',
                       ]
                   ]);
    }
}