<?php

namespace UnicaenSignature\Form;

use Laminas\Form\Element\Checkbox;
use Laminas\Form\Element\File;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use UnicaenSignature\Hydrator\SignatureFormHydrator;
use UnicaenSignature\Service\SignatureServiceAwareTrait;

class SignatureForm extends Form
{
    use SignatureServiceAwareTrait;

    private array $marksOptions;

    /**
     * @param array $marksOptions Liste des niveaux de signature
     */
    public function __construct(array $marksOptions)
    {
        parent::__construct('signatureform');
        $this->setHydrator(new SignatureFormHydrator($marksOptions));
        $this->marksOptions = $marksOptions;
    }


    public function init(): void
    {
        $this->add(
            [
                'type' => Hidden::class,
                'name' => 'id',
            ]
        );
        // Type de signature
        $this->add([
                       'type'    => Select::class,
                       'name'    => 'type',
                       'options' => [
                           'label'         => 'Type de signature/visa',
                           'empty_option'  => 'Selectionner un type de signature/visa...',
                           'value_options' => $this->marksOptions
                       ]
                   ]);

        // Type de signature
        $this->add([
                       'type'    => Text::class,
                       'name'    => 'label',
                       'options' => [
                           'label' => 'Intitulé'
                       ]
                   ]);

        // Type de signature
        $this->add([
                       'type'    => File::class,
                       'name'    => 'file',
                       'options' => [
                           'label' => 'Document'
                       ]
                   ]);

        // Destinataires (liste de mails)
        $this->add([
                       'type'    => Textarea::class,
                       'name'    => 'emails',
                       'options' => [
                           'label'        => 'Emails des signataires',
                           'empty_option' => 'Emails séparés par une virgule',
                       ]
                   ]);

        // Observateurs (liste de mails)
        $this->add([
                       'type'    => Textarea::class,
                       'name'    => 'observers',
                       'options' => [
                           'label'        => 'Emails des observateurs',
                           'empty_option' => 'Emails séparés par une virgule',
                       ]
                   ]);

        $this->add([
                       'type'    => Checkbox::class,
                       'name'    => 'allSignToComplete',
                       'options' => [
                           'label' => 'Tous doivent signer'
                       ]
                   ]);

        // Description
        $this->add([
                       'type'    => Textarea::class,
                       'name'    => 'description',
                       'options' => [
                           'label' => 'Description'
                       ]
                   ]);
    }
}