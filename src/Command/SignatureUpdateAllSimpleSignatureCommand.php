<?php

namespace UnicaenSignature\Command;

use Laminas\ServiceManager\ServiceManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UnicaenSignature\Entity\Db\Signature;

class SignatureUpdateAllSimpleSignatureCommand extends SignatureCommandAbstract
{
    protected static $defaultName = 'signature:update-simplesignature-all';

    protected function configure()
    {
        $this
            ->setDescription("Actualise l'état des signatures");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = $this->getIO($input, $output);

        $headers = ['id', 'label', 'status', 'Parapheur', 'Remote ID', 'doc', 'msg'];
        $rows = [];
        /** @var Signature $signature */
        foreach ($this->getSignatureService()->getSignaturesSimples() as $signature)  {
            $msg = "Rien à faire";
            $row = [
                $signature->getId(),
                $signature->getLabel(),
                $signature->getStatusText(),
                $signature->getLetterfileKey(),
                $signature->getDocumentRemotekey(),
                $signature->getDocumentPath(),
            ];
            $msg = "Rien à faire";
            if( $signature->isUpdatable() ){
                try {
                    $this->getSignatureService()->updateStatusSignature($signature);
                    $msg = "Fait (" . $signature->getStatusText() . ")";
                } catch (\Exception $e) {
                    $msg = 'Error : ' . $e->getMessage();
                }
            }
            $row[] = $msg;
            $rows[] = $row;
        }
        $io->table($headers, $rows);


        return self::SUCCESS;
    }
}