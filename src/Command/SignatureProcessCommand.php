<?php

namespace UnicaenSignature\Command;

use Laminas\ServiceManager\ServiceManager;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SignatureProcessCommand extends SignatureCommandAbstract
{
    // php bin/oscar.php signature:add-signature "$(<module/UnicaenSignature/test/datas/json-datas-add-signature.json)"

    protected static $defaultName = 'signature:process';

    const ARG_ID = "id";

    protected function configure()
    {
        $this
            ->setDescription("Processus")
            ->addOption('id', 'i', InputOption::VALUE_OPTIONAL, 'Identifiante du process')
            ->addOption('action', 'a', InputOption::VALUE_OPTIONAL, 'Action', 'list');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = $this->getIO($input, $output);
        $action = $input->getOption('action');
        switch ($action) {
            case 'list' :
                $io->title("Liste des processus");
                $headers = ['ID', 'Label', 'étape(s)', 'status'];
                $rows = [];
                foreach ($this->getProcessService()->getProcesses() as $process) {
                    $rows[] = [
                        $process->getId(),
                        $process->getLabel(),
                        count($process->getSteps()),
                        $process->getStatus(),
                    ];
                }
                $io->table($headers, $rows);
                break;

            case 'delete' :
                $id = $input->getOption(self::ARG_ID);
                $io->title("Suppression de '$id'");
                try {
                    $this->getSignatureService()->deleteProcess($id);
                } catch (\Exception $e) {
                    $io->error($e->getMessage());
                    return self::FAILURE;
                }
                break;
            default :
                return self::INVALID;
        }

        return self::SUCCESS;
    }
}