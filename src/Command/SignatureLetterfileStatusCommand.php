<?php

namespace UnicaenSignature\Command;

use Laminas\ServiceManager\ServiceManager;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SignatureLetterfileStatusCommand extends SignatureCommandAbstract
{
    protected static $defaultName = 'signature:letterfile-status';
    protected $disabled = true;

    const ARG_ID = "id";

    protected function configure()
    {
        $this
            ->setDescription("Vérification du status de la procédure de signature depuis le parafeur")
            ->addArgument(self::ARG_ID, InputArgument::REQUIRED, "données JSON")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = $this->getIO($input, $output);
        $id = $input->getArgument(self::ARG_ID);
        $status = $this->getSignatureService()->getLetterfileService()->getDefaultLetterFileStrategy()->getSignRequestsStatus($id);
        $io->info("STATUT '$id' : " . $status);
        return self::SUCCESS;
    }
}