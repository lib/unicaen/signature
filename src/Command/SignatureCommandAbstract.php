<?php

namespace UnicaenSignature\Command;

use Laminas\ServiceManager\ServiceManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use UnicaenSignature\Service\ProcessService;
use UnicaenSignature\Service\SignatureService;

abstract class SignatureCommandAbstract extends Command
{
    /** @var ServiceManager ServiceManager */
    private ServiceManager $serviceManager;

    public function __construct(ServiceManager $sm)
    {
        $this->serviceManager = $sm;
        parent::__construct();
    }

    /**
     * @return ServiceManager
     */
    protected function getServicemanager(): ServiceManager
    {
        return $this->serviceManager;
    }

    /**
     * @return SignatureService
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function getSignatureService(): SignatureService
    {
        return $this->getServicemanager()->get(SignatureService::class);
    }

    protected function getProcessService(): ProcessService
    {
        return $this->getServicemanager()->get(ProcessService::class);
    }

    /**
     * Style par défaut
     *
     * @param OutputInterface $output
     * @return void
     */
    protected function addOutputStyle(OutputInterface $output): void
    {
        $outputStyle = new OutputFormatterStyle('cyan', 'default', ['bold']);
        $output->getFormatter()->setStyle('id', $outputStyle);

        $outputStyle = new OutputFormatterStyle('red', 'default', ['bold']);
        $output->getFormatter()->setStyle('red', $outputStyle);

        $outputStyle = new OutputFormatterStyle('blue', 'default', ['underscore']);
        $output->getFormatter()->setStyle('link', $outputStyle);

        $outputStyle = new OutputFormatterStyle('default', 'default', ['bold']);
        $output->getFormatter()->setStyle('bold', $outputStyle);

        $outputStyle = new OutputFormatterStyle('yellow', 'default', ['bold']);
        $output->getFormatter()->setStyle('none', $outputStyle);

        $outputStyle = new OutputFormatterStyle('cyan', 'default', ['bold']);
        $output->getFormatter()->setStyle('title', $outputStyle);

        $outputStyle = new OutputFormatterStyle('green', 'default', ['bold']);
        $output->getFormatter()->setStyle('green', $outputStyle);

        $outputStyle = new OutputFormatterStyle('#AAA', 'default', []);
        $output->getFormatter()->setStyle('light', $outputStyle);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param bool $extraStyle
     * @return SymfonyStyle
     */
    public function getIO(InputInterface $input, OutputInterface $output, bool $extraStyle = true): SymfonyStyle
    {
        if ($extraStyle) {
            $this->addOutputStyle($output);
        }

        return new SymfonyStyle($input, $output);
    }
}