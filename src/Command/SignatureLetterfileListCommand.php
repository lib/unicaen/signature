<?php

namespace UnicaenSignature\Command;

use Laminas\ServiceManager\ServiceManager;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SignatureLetterfileListCommand extends SignatureCommandAbstract
{
    protected static $defaultName = 'signature:letterfiles-list';

    const opt_letterfile = 'letterfile';
    protected $disabled = true;

    protected function configure()
    {
        $this
            ->setDescription("Vérification du status de la procédure de signature depuis le parafeur")
            ->addOption(self::opt_letterfile, 'l', InputOption::VALUE_OPTIONAL, 'Clef du parafeur', null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = $this->getIO($input, $output);
        $io->info("LISTE des INFORMATION DANS le Parafeur");

        $letterfileKey = $input->getOption(self::opt_letterfile);

        if( $letterfileKey === null ){
            $letterfile = $this->getSignatureService()->getLetterfileService()->getDefaultLetterFileStrategy();
        } else {
            $letterfile = $this->getSignatureService()->getLetterfileService()->getLetterFileStrategy($letterfileKey);
        }

        $list = $letterfile->getSignRequests();
        $headers = ['ID', 'title', 'Emails', 'Statut', 'Créé le', 'Dernière MAJ'];
        $rows = [];
        foreach ($list as $signrequest){
            $row = [];
            $row[] = $signrequest->getIdInLetterfile();
            $row[] = substr($signrequest->getTitle(), 0, 50);
            $row[] = $signrequest->getRecipientsEmails() ? count($signrequest->getRecipientsEmails()) . ' mail(s)' : 'unknow';
            $row[] = $signrequest->getStatus();
            $row[] = $signrequest->getDateCreated()->format('Y-m-d H:i');
            $row[] = $signrequest->getDateUpdated() ? $signrequest->getDateUpdated()->format('Y-m-d H:i') : "-";
            $rows[] = $row;
        }

        $io->table($headers, $rows);

        return self::SUCCESS;
    }
}