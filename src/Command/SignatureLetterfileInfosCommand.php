<?php

namespace UnicaenSignature\Command;

use Laminas\ServiceManager\ServiceManager;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SignatureLetterfileInfosCommand extends SignatureCommandAbstract
{
    protected static $defaultName = 'signature:letterfile-info';

    const ARG_ID = "id";
    protected $disabled = true;

    protected function configure()
    {
        $this
            ->setDescription("Récupération des informations sur la procédure de signature dans ESUP")
            ->addArgument(self::ARG_ID, InputArgument::REQUIRED, "données JSON")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = $this->getIO($input, $output);
        $id = $input->getArgument(self::ARG_ID);
        $infos = $this->getSignatureService()->getLetterfileService()->getDefaultLetterFileStrategy()->getSignRequestInfo($id);
        echo json_encode($infos->toArray(), JSON_PRETTY_PRINT);
        return self::SUCCESS;
    }
}