<?php

namespace UnicaenSignature\Command;

use Laminas\ServiceManager\ServiceManager;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SignatureUpdateAllProcessesCommand extends SignatureCommandAbstract
{
    protected static $defaultName = 'signature:update-process-all';

    protected function configure()
    {
        $this
            ->setDescription("Actualise l'état des procédures de signature")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $io = $this->getIO($input, $output);

        $headers = ['id', 'label', 'status', 'msg'];
        $rows = [];
        foreach ($this->getProcessService()->getProcesses() as $process)  {
            $msg = "Rien à faire";
            $row = [
                $process->getId(),
                $process->getLabel(),
                $process->getStatusText(),
            ];
            if( $process->isTriggerable() ){
                try {
                    $this->getProcessService()->trigger($process);
                    $msg = "Fait";
                } catch (\Exception $e) {
                    $msg = 'Error : ' . $e->getMessage();
                }
            }
            $row[] = $msg;
            $rows[] = $row;
        }
        $io->table($headers, $rows);


        return self::SUCCESS;
    }
}