<?php

namespace UnicaenSignature\Command;

use Laminas\ServiceManager\ServiceManager;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SignatureAddSignatureCommand extends SignatureCommandAbstract
{
    protected static $defaultName = 'signature:add-signature';
    //protected static $disabled = true;

    const ARG_DATAS = "arg_datas";

    protected function configure()
    {
        $this
            ->setDescription("Création d'une procédure de signature")
            ->addArgument(self::ARG_DATAS, InputArgument::REQUIRED, "données JSON")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = $this->getIO($input, $output);
        $jsonFile = $input->getArgument(self::ARG_DATAS);
        if( !file_exists($jsonFile) ){
            $io->error("Fichier inconnu '$jsonFile'");
            return self::FAILURE;
        }
        try {
            $json = json_decode(file_get_contents($jsonFile), true);
            if( !$json ){
                throw new \Exception("Mauvais contenu JSON");
            }
        } catch (\Exception $e) {
            $io->error("erreur JSON dans '$jsonFile'");
            return self::FAILURE;
        }
        $io->info("From '$jsonFile'");
        $io->block(json_encode($json, JSON_PRETTY_PRINT));
        $this->getSignatureService()->getLetterfileService()->getDefaultLetterFileStrategy()->addDocument(
            $json['document'],
            $json['users'],
            $json['workflow'],
            $json['title'],
            $json['comment'],
            $json['signType'],
            true
        );
        return self::SUCCESS;
    }
}