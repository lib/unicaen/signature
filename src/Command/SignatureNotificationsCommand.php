<?php

namespace UnicaenSignature\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use UnicaenSignature\Entity\Db\SignatureNotification;

class SignatureNotificationsCommand extends SignatureCommandAbstract
{
    protected static $defaultName = 'signature:notifications';

    protected function configure()
    {
        $this
            ->setDescription("Notifications manuelles des signatures")
            ->addOption('history', 'i', InputOption::VALUE_NONE)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = $this->getIO($input, $output);

        // TODO

        return self::SUCCESS;
    }
}