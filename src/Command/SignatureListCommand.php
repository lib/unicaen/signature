<?php

namespace UnicaenSignature\Command;

use Laminas\ServiceManager\ServiceManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UnicaenSignature\Entity\Db\Signature;

class SignatureListCommand extends SignatureCommandAbstract
{
    protected static $defaultName = 'signature:list';

    protected function configure()
    {
        $this
            ->setDescription("Liste des procédures de signatures");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = $this->getIO($input, $output);
        $io->title("Procédures de signature");

        $headers = ["ID", "Label", "Description", "Parafeur", "Destinataires", "Tous", "Doc ID", "Statut"];
        $rows = [];

        $this->getSignatureService()->testTrigger();

        /** @var Signature $signature */
        foreach ($this->getSignatureService()->getSignatures() as $signature) {
            $nbrSigned = count($signature->getSignedRecipients());
            $rows[] = [
                $signature->getId(),
                $signature->getLabel(),
                $signature->getDescription(),
                $signature->getLetterfileKey(),
                $nbrSigned . '/' . count($signature->getRecipientsEmailsArray()),
                $signature->getAllSignToComplete() ? "O" : "N",
                $signature->getDocumentRemotekey(),
                $signature->getStatusText(),
            ];
        }

        $io->table($headers, $rows);

        return self::SUCCESS;
    }
}