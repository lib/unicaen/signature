<?php

namespace UnicaenSignature\Command;

use Laminas\ServiceManager\ServiceManager;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UnicaenSignature\Entity\Db\Signature;

class SignatureSendCommand extends SignatureCommandAbstract
{
    protected static $defaultName = 'signature:send';

    protected function configure()
    {
        $this
            ->addArgument('id', InputArgument::REQUIRED, 'ID de la signature à envoyer')
            ->setDescription("Liste des procédures de signatures");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = $this->getIO($input, $output);

        try {
            $signature = $this->getSignatureService()->getSignature($input->getArgument('id'));
            if( $signature->getProcessStep() ){
                $io->warning("Cette signature fait partie d'un processus, utilisez les commandes de processus");
                return self::INVALID;
            }
            $this->getSignatureService()->sendSignature($signature);
            $io->title("Envoi de la signature '$signature'");
        } catch (\Exception $exception) {
            $io->error($exception->getMessage());
            return self::FAILURE;
        }
        return self::SUCCESS;
    }
}