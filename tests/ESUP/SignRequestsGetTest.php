<?php
namespace ESUP;

class SignRequestsGetTest extends \PHPUnit\Framework\TestCase
{
    public function testSignRequestsGetPostIts()
    {
        $datas = $this->getJsonData('esup-retour-refused-post-it-comments.json');
        $this->assertTrue($datas !== null);

        $esupSignRequest = \UnicaenSignature\Strategy\Letterfile\Esup\Response\SignRequestInfo::getFromSignRequestGetId($datas);

        $this->assertTrue(
            $esupSignRequest->getStatus() === \UnicaenSignature\Entity\Db\Signature::STATUS_SIGNATURE_REJECT
        );

        $this->assertTrue(
            $esupSignRequest->isAccepted('karin.fery@unicaen.fr')
        );
        $this->assertTrue(
            $esupSignRequest->isAccepted('jean-baptiste.oellers@unicaen.fr')
        );

        $this->assertEquals(
            "Commentaire de REFUS à récupérer",
            $esupSignRequest->getRefusedCommentsByFlat()
        );

        $this->assertEquals(
            (new \DateTime('2024-10-15 10:05:34'))->format('Y-m-d H:i:s'),
            $esupSignRequest->getDateDone(null, false)->format('Y-m-d H:i:s')
        );
    }

    public function testSignRequestsGetWaiting1of3()
    {
        $datas = $this->getJsonData('esup-retour-waiting-signed-2-of-3.json');
        $this->assertTrue($datas !== null);

        $esupSignRequest = \UnicaenSignature\Strategy\Letterfile\Esup\Response\SignRequestInfo::getFromSignRequestGetId($datas);

        $this->assertTrue(
            $esupSignRequest->getStatus() === \UnicaenSignature\Entity\Db\Signature::STATUS_SIGNATURE_WAIT
        );

        $this->assertTrue(
            $esupSignRequest->isAccepted('karin.fery@unicaen.fr')
        );
        $this->assertTrue(
            $esupSignRequest->isAccepted('jean-baptiste.oellers@unicaen.fr')
        );

        $this->assertNull(
            $esupSignRequest->getDateDone(null, false)
        );
    }

    public function testSignRequestsGetRefused1of3()
    {
        $datas = $this->getJsonData('esup-retour-refused-2-accepted.json');
        $this->assertTrue($datas !== null);

        $esupSignRequest = \UnicaenSignature\Strategy\Letterfile\Esup\Response\SignRequestInfo::getFromSignRequestGetId($datas);

        $this->assertTrue(
            $esupSignRequest->getStatus() === \UnicaenSignature\Entity\Db\Signature::STATUS_SIGNATURE_REJECT
        );

        $this->assertTrue(
            $esupSignRequest->isRefused('stephane.bouvry@unicaen.fr')
        );
        $this->assertTrue(
            $esupSignRequest->isAccepted('karin.fery@unicaen.fr')
        );
        $this->assertTrue(
            $esupSignRequest->isAccepted('jean-baptiste.oellers@unicaen.fr')
        );

        $this->assertEquals(
            (new \DateTime('2024-10-15 09:35:37'))->format('Y-m-d H:i:s'),
            $esupSignRequest->getDateDone(null, false)->format('Y-m-d H:i:s')
        );

        $this->assertEquals(
            $esupSignRequest->getRefusedCommentsByFlat(), "Commentaire de refus à récupérer"
        );

    }

    public function testSignRequestsGetRefused()
    {
        $datas = $this->getJsonData('esupt-retour-refused.json');
        $this->assertTrue($datas !== null);

        $esupSignRequest = \UnicaenSignature\Strategy\Letterfile\Esup\Response\SignRequestInfo::getFromSignRequestGetId($datas);

        $this->assertTrue(
            $esupSignRequest->getStatus() === \UnicaenSignature\Entity\Db\Signature::STATUS_SIGNATURE_REJECT);

        $this->assertTrue(
            $esupSignRequest->isRefused('stephane.bouvry@unicaen.fr')
        );

        $this->assertEquals(
            $esupSignRequest->getRefusedCommentsByFlat(), "Il y'a un problème dans ce document"
        );

        $this->assertEquals(
            new \DateTime("2024-10-14T09:10:29.204+00:00"),
            $esupSignRequest->getDateDone()
        );
    }

    protected function getJsonData(string $filename): array
    {
        $file_path = __DIR__.'/../datas/' . $filename;
        if( !file_exists($file_path) ){
            throw new \Exception($file_path.' n\'existe pas');
        }
        $content = file_get_contents($file_path);
        return json_decode($content, true);
    }

}