# Parapheur interne

Vous pouvez utiliser un parafeur interne.

## configuration

Dans `config/autoload/unicaen-signature.local.php`, ajoutez dans la configuration des parapheurs : 

```php
<?php
// config/autoload/unicaen-signature.local.php

use Psr\Container\ContainerInterface;

return [
    'unicaen-signature' => [
        // ...
        'letterfiles'            => [
            /// OSCAR (Viseur interne)
            [
                'label'   => 'OSCAR visa',
                'name'    => 'internal', // internal est une clef dédiée pour identifier le parapheur interne
                'default' => false,
                'class'   => \UnicaenSignature\Strategy\Letterfile\InternalVisa\InternalVisaStrategy::class,
                'levels'  => [
                    'visa_hidden' => 'visa_hidden',
                ],
                'config'  => [
                    'checkUserAcces' => function ($sc, $signatureRecipient) {
                        return true;
                        //return $sc->get(\Oscar\Service\ProjectGrantService::class)->getRecipients($options);
                    }
                ]
            ]    
        ]
    ]
    // ...
];
```