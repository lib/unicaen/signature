# Parapheur ESUP

Vous pouvez utiliser un parafeur interne.

## configuration

Dans `config/autoload/unicaen-signature.local.php`, ajoutez dans la configuration des parapheurs : 

```php
<?php
// config/autoload/unicaen-signature.local.php

use Psr\Container\ContainerInterface;

return [
    'unicaen-signature' => [
        // ...
        'letterfiles'            => [
            /// ESUP
            [
                // Nom visible côté applicatif
                'label' => 'ESUP',

                // Code/Clef (unique)
                'name'  => 'esup',

                'description' => 'Parapheur numérique ESUP',

                // Utilisé par défaut
                'default'     => true,

                // Classe
                'class'       => \UnicaenSignature\Strategy\Letterfile\Esup\EsupLetterfileStrategy::class,

                // Niveaux de signature disponible
                'levels'      => [
                    \UnicaenSignature\Utils\SignatureConstants::VISA_VISUAL => 'hiddenVisa',
                    \UnicaenSignature\Utils\SignatureConstants::SIGN_VISUAL => 'pdfImageStamp',
                    \UnicaenSignature\Utils\SignatureConstants::SIGN_CERTIF => 'certSign',
                    \UnicaenSignature\Utils\SignatureConstants::SIGN_EIDAS  => 'nexuSign',
                ],

                // Configuration du parapheur
                'config'      => [
                    // ESUP configuration
                    'url'           => "https://signature-pp.unicaen.fr",

                    // Créateur
                    'createdByEppn' => 'bouvry@unicaen.fr',
                ]
            ],   
        ]
    ]
    // ...
];
```

Dans la partie `config`, renseignez les informations relatives à votre instance ESUP, et adaptez les niveaux de signature à rendre disponible.

## Côté ESUP

Côté ESUP, une configuration est requise pour authoriser votre application à communiquer avec lui.

L'accès à l'API REST de Esup necessite d'autoriser l'accès à l'IP de votre application.

Fichier dans Esup install/path/conf

Relancer Esup pour prendre en compte la configuration
