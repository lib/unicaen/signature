# Mise à jour de la base de données

```sql
-- DROP TABLE unicaen_signature_signature;
CREATE TABLE unicaen_signature_signature
(
    id                      int4         NOT NULL,
    datecreated             timestamp(0) NULL DEFAULT NULL::timestamp without time zone,
    "type"                  varchar(32) NULL DEFAULT NULL::character varying,
    status                  int4         NOT NULL DEFAULT 101,
    "ordering"              int4         NOT NULL DEFAULT 0,
    "label"                 varchar(255) NULL DEFAULT NULL::character varying,
    description             varchar(255) NULL DEFAULT NULL::character varying,
    datesend                timestamp(0) NULL DEFAULT NULL::timestamp without time zone,
    dateupdate              timestamp(0) NULL DEFAULT NULL::timestamp without time zone,
    document_path           varchar(255) NOT NULL,
    document_remotekey      varchar(255) NULL DEFAULT NULL::character varying,
    document_localkey       varchar(255) NULL DEFAULT NULL::character varying,
    context_short           varchar(255) NULL,
    context_long            text NULL,
    letterfile_key          varchar(255) NULL DEFAULT NULL::character varying,
    letterfile_process      varchar(255) NULL DEFAULT NULL::character varying,
    letterfile_url          varchar(255) NULL DEFAULT NULL::character varying,
    allsigntocomplete       bool         NOT NULL DEFAULT false,
    notificationsrecipients bool         NOT NULL DEFAULT false,
    CONSTRAINT unicaen_signature_signature_pkey PRIMARY KEY (id)
);

-- DROP TABLE unicaen_signature_recipient;
CREATE TABLE unicaen_signature_recipient
(
    id           int4         NOT NULL,
    signature_id int4 NULL,
    status       int4         NOT NULL DEFAULT 101,
    firstname    varchar(64) NULL DEFAULT NULL::character varying,
    lastname     varchar(64) NULL DEFAULT NULL::character varying,
    email        varchar(256) NOT NULL,
    phone        varchar(20) NULL DEFAULT NULL::character varying,
    dateupdate   timestamp(0) NULL DEFAULT NULL::timestamp without time zone,
    datefinished timestamp(0) NULL DEFAULT NULL::timestamp without time zone,
    keyaccess    varchar(255) NULL DEFAULT NULL::character varying,
    informations varchar(255) NULL DEFAULT NULL::character varying,
    urlDocument VARCHAR(255) DEFAULT NULL::character varying,

CONSTRAINT unicaen_signature_recipient_pkey PRIMARY KEY (id),
    CONSTRAINT fk_f47c5330ed61183a FOREIGN KEY (signature_id) REFERENCES unicaen_signature_signature (id)
);
CREATE INDEX idx_f47c5330ed61183a ON unicaen_signature_recipient USING btree (signature_id);

-- DROP TABLE unicaen_signature_observer;
CREATE TABLE unicaen_signature_observer
(
    id           int4         NOT NULL,
    signature_id int4 NULL,
    firstname    varchar(64) NULL DEFAULT NULL::character varying,
    lastname     varchar(64) NULL DEFAULT NULL::character varying,
    email        varchar(256) NOT NULL,
    CONSTRAINT unicaen_signature_observer_pkey PRIMARY KEY (id),
    CONSTRAINT fk_eac19423ed61183a FOREIGN KEY (signature_id) REFERENCES unicaen_signature_signature (id)
);
CREATE INDEX idx_eac19423ed61183a ON unicaen_signature_observer USING btree (signature_id);

-- DROP TABLE unicaen_signature_signatureflow;
CREATE TABLE unicaen_signature_signatureflow
(
    id          int4 NOT NULL,
    "label"     varchar(255) NULL DEFAULT NULL::character varying,
    description varchar(255) NULL DEFAULT NULL::character varying,
    enabled     bool NOT NULL DEFAULT false,
    CONSTRAINT unicaen_signature_signatureflow_pkey PRIMARY KEY (id)
);

-- DROP TABLE unicaen_signature_signatureflowstep;
CREATE TABLE unicaen_signature_signatureflowstep
(
    id                      int4         NOT NULL,
    recipientsmethod        varchar(64) NULL DEFAULT NULL::character varying,
    "label"                 varchar(64) NULL DEFAULT NULL::character varying,
    letterfilename          varchar(256) NOT NULL,
    signlevel               varchar(256) NOT NULL,
    "ordering"              int4         NOT NULL,
    allrecipientssign       bool         NOT NULL DEFAULT true,
    notificationsrecipients bool         NOT NULL DEFAULT false,
    editablerecipients      bool         NOT NULL DEFAULT false,
    "options"               text NULL,
    observers_options       text NULL,
    observersmethod         varchar(64) NULL DEFAULT NULL::character varying,
    signatureflow_id        int4 NULL,
    CONSTRAINT unicaen_signature_signatureflowstep_pkey PRIMARY KEY (id),
    CONSTRAINT fk_a575dc3eb4090c8a FOREIGN KEY (signatureflow_id) REFERENCES unicaen_signature_signatureflow (id)
);
CREATE INDEX idx_a575dc3eb4090c8a ON unicaen_signature_signatureflowstep USING btree (signatureflow_id);

-- DROP TABLE unicaen_signature_process;
CREATE TABLE unicaen_signature_process
(
    id               int4         NOT NULL,
    datecreated      timestamp(0) NULL DEFAULT NULL::timestamp without time zone,
    lastupdate       timestamp(0) NULL DEFAULT NULL::timestamp without time zone,
    status           int4 NULL,
    currentstep      int4         NOT NULL,
    document_name    varchar(255) NOT NULL,
    label varchar(255) NULL DEFAULT NULL::character varying,
    signatureflow_id int4 NULL,
    CONSTRAINT unicaen_signature_process_pkey PRIMARY KEY (id),
    CONSTRAINT fk_994855d2b4090c8a FOREIGN KEY (signatureflow_id) REFERENCES unicaen_signature_signatureflow (id)
);
CREATE INDEX idx_994855d2b4090c8a ON unicaen_signature_process USING btree (signatureflow_id);

-- DROP TABLE unicaen_signature_process_step;
CREATE TABLE unicaen_signature_process_step
(
    id                   int4 NOT NULL,
    process_id           int4 NULL,
    signature_id         int4 NULL,
    label varchar(255) NULL DEFAULT NULL::character varying,
    CONSTRAINT unicaen_signature_process_step_pkey PRIMARY KEY (id),
    CONSTRAINT fk_cf70b0a57ec2f574 FOREIGN KEY (process_id) REFERENCES unicaen_signature_process (id),
    CONSTRAINT fk_cf70b0a5ed61183a FOREIGN KEY (signature_id) REFERENCES unicaen_signature_signature (id)
);
CREATE INDEX idx_cf70b0a57ec2f574 ON unicaen_signature_process_step USING btree (process_id);
CREATE UNIQUE INDEX uniq_cf70b0a5ed61183a ON unicaen_signature_process_step USING btree (signature_id);

-- Sequences
CREATE SEQUENCE public.unicaen_signature_observer_id_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;

CREATE SEQUENCE public.unicaen_signature_process_id_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;

CREATE SEQUENCE public.unicaen_signature_process_step_id_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;

CREATE SEQUENCE public.unicaen_signature_recipient_id_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;

CREATE SEQUENCE public.unicaen_signature_signature_id_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;

CREATE SEQUENCE public.unicaen_signature_signatureflow_id_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;

CREATE SEQUENCE public.unicaen_signature_signatureflowstep_id_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
	CACHE 1
	NO CYCLE;
```