# Utilisation des commandes

## Console de base

L'utilitaire en ligne de commande propose différent outils pour gérer les signatures de l'application

```bash
php vendor/bin/unicaen-signature 
```

Sortie : 
```text
Console Tool

Usage:
  command [options] [arguments]

Options:
  -h, --help            Display help for the given command. When no command is given display help for the list command
  -q, --quiet           Do not output any message
  -V, --version         Display this application version
      --ansi|--no-ansi  Force (or disable --no-ansi) ANSI output
  -n, --no-interaction  Do not ask any interactive question
  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

Available commands:
  completion                            Dump the shell completion script
  help                                  Display help for a command
  list                                  List commands
 signature
  signature:add-signature               Création d'un procédure de signature
  signature:check-config                Vérification de la configuration du système de signature éléctronique
  signature:letterfile-info             Récupération des informations sur la procédure de signature dans ESUP
  signature:letterfile-status           Vérification du status de la procédure de signature depuis le parafeur
  signature:letterfiles-list            Vérification du status de la procédure de signature depuis le parafeur
  signature:list                        Liste des procédures de signatures
  signature:notifications               Notifications manuelles des signatures
  signature:process                     Processus
  signature:update-process-all          Actualise l'état des signatures
  signature:update-simplesignature-all  Actualise l'état des signatures
```
## Commandes usuelles

### Vérifier la configuration

la commande `signature:check-config` permet de vérifier la configuration ainsi que le/les accès au parapheur numérique.

```bash
php vendor/bin/unicaen-signature signature:check-config
```

```text
Vérification de la configuration UnicaenSignature
=================================================

LOGGER
------

 Activé : ✅
 Stdout (développement) : non
 Level : 100 (DEBUG)
 File : /home/bouvry/Projects/Unicaen/UnicaenLib/signature-test/config/autoload/../../logs/signature.log

ESUP
----

 Name : esup
 Par défaut : ✅
 Type de signature prise en charge : 
  - [visa_hidden] : Visa caché, Visa applicatif, pas de trace dans le document)
  - [visa_visual] : Visa visuel, Visa visuel. Nom, prénom et date appliqués sur le document.)
  - [sign_visual] : Signature calligraphique, Signature calligraphique. Signature au format image. Appliquée sur le document.)
  - [sign_certif] : Signature par certificat (PKCS12), Signature certifiée avec un keystore/certificat via un magasin de clés au format PKCS12. Appliquée sur le document.)
  - [sign_eidas] : Signature par clef cryptographique, Signature par clef cryptographique via clé usb (eIDAS) qui contient un certificat. Certificat qui peut être personnel ou au titre de l'établissement (cachet établissement). Appliquée sur le document.)

 Configuration du parafeur : 
 --------------- --------------------------------- -------- 
  clef            valeur                            type    
 --------------- --------------------------------- -------- 
  url             https://signature-pp.unicaen.fr   string  
  createdByEppn   bouvry@unicaen.fr                 string  
 --------------- --------------------------------- -------- 

 Accès au parafeur : 
                                                                                                                
 [OK]                                                                                                                   
```

### Liste des signatures

Affiche la liste des signatures

```bash
php vendor/bin/unicaen-signature signature:list
```

### Liste des processus

Affiche la liste des processus

```bash
php vendor/bin/unicaen-signature signature:process
```

### Mise à jour des signatures (CRON)

Recalcule automatique de l'état des signatures

```bash
php vendor/bin/unicaen-signature signature:update-simplesignature-all
```

### Mise à jour des processus (CRON)

Recalcule automatique de l'état des processus

```bash
php vendor/bin/unicaen-signature signature:update-process-all
```



## Annexe

### Commandes dans votre application (exemple)

Exemple, créer un fichier `bin/commands.php` :

```php
<?php
require __DIR__.'/../vendor/autoload.php';

chdir(dirname(__DIR__));
$console = new \Symfony\Component\Console\Application();
$conf = require __DIR__.'/../config/application.config.php';
$app = \Laminas\Mvc\Application::init($conf);

$commands = [];

$paths = $app->getConfig()['console_commands'];

function scanCommandFiles(string $namespace, string $dir){
    global $app;
    $output = [];
    $re = '/.*Command\.php/m';

    if( !file_exists($dir) ){
        echo ("[WARNING] Command path : Le dossier '$dir' n'existe pas\n");
        return [];
    }

    $scan = scandir($dir);
    foreach ($scan as $key => $value) {
        if( !in_array($value, ['.', '..']) ){
            if( is_dir($dir.DIRECTORY_SEPARATOR.$value) ){

            } else {
                if( preg_match($re, $value, $matches) ){
                    $class = substr($value, 0, strlen($value)-4);
                    $reflector = new ReflectionClass($namespace.$class);
                    if( $reflector->isSubclassOf(\Symfony\Component\Console\Command\Command::class) ){
                        $instance = $reflector->newInstanceArgs([$app->getServiceManager()]);
                        if( property_exists($instance, 'disabled') ) {
                            continue;
                        }
                        $result[] = $instance;
                    }
                }
            }
        }
    }
    return $result;
}

foreach ($paths as $namespace=>$path) {
    $commands = scanCommandFiles($namespace, $path);
    $console->addCommands($commands);
}

$console->run();
```

Et voilà

```bash
php bin/commands.php
```