# Usage simple


## Envoyer un document au parapheur

### SignatureService

L'envoi de document et la gestion des signatures passe par un unique service : `SignatureService`, vous devez le rendre accessible  

```php
<?php
// Exemple
namespace Application\Controller;

use Psr\Container\ContainerInterface;
use UnicaenSignature\Service\SignatureService;

class IndexControllerFactory
{
    public function __invoke(ContainerInterface $container): IndexController
    {
        $s = new IndexController();
        
        // On passe le 'SignatureService' au controlleur
        $s->setSignatureService($container->get(SignatureService::class));
        
        return $s;
    }
}
```

Ensuite, Vous pourrez créé des signatures et envoyer des documents au parapheur : 

```php
try {
    // ---------------------------------------------------------------- OPTIONS
    // Parapheur à utiliser : Correspond à la clef "name" dans la configuration du parpheur
    $parapheur = 'esup';

    // Niveau de signature (Configuré dans la clef 'levels' du parapheur
    $level = SignatureConstants::SIGN_VISUAL;

    // Tous les destinataires doivent signer
    $allSignToComplete = true;

    // Fichier à envoyer (nom du fichier)
    // !!! CE DOCUMENT DOIT ÊTRE DANS LE DOSSIER 'documents_path'
    // renseigné dans la configuration de UnicaenSignature
    $document_path = 'foo-document.pdf';

    // Intitulé
    $label = "Document d'exemple à signer (Test " . uniqid() . ")";

    // Description
    $description = "Test fait le " . date('Y-m-d H:i:s');

    // -------------------------------------------------------------------------

    // ------------------------------------------------------------- CREATION et ENVOI
    // Nouvelle signature
    $signature = new Signature();

    // Destinataires
    $signature->addRecipient(new SignatureRecipient(
        'antony.lecourtes@unicaen.fr', 'Antony', 'Le Courtes'));
    $signature->addRecipient(new SignatureRecipient(
        'stephane.bouvry@unicaen.fr', 'Stéphane', 'Bouvry'));

    // Configuration
    $signature->setLetterfileKey($parapheur);
    $signature->setDocumentPath($document_path);
    $signature->setType($level);
    $signature->setAllSignToComplete($allSignToComplete);
    $signature->setLabel($label);
    $signature->setDescription($description);

    // On enregistre (l'option TRUE permet d'envoyer directement au parapheur)
    $this->getSignatureService()->saveNewSignature($signature, true);
} catch (\Exception $exception) {
    die("ERROR : " . $exception->getMessage());
}
```

En cas d'erreur, vous pouvez consulter le fichier de log (par défaut dans `./logs/signature.log`). [Configurer les logs](#système-de-log-pour-les-développeurs)


## Déclencher la procédure de signature

Si vous n'utilisez pas l'option `TRUE` lors de la création d'un signature avec la méthode `saveNewSignature(Signature $signature, bool $send):void`, vous pouvez déclencher l'envoi avec la méthode `sendSignature(Signature $signature):void`

```php
$signatureService->sendSignature($signature);
```


## Mettre à jour le statut d'une signature

### Via le code

Le service `SignatureService` propose un méthode pour déclencher l'actualisation de l'état d'un signature. Elle va : 
 - Interroger le parapheur
 - Actualiser les données de statut si besoin
 - Emèttre des événements en fonction des événements

```php
$signatureService->updateStatusSignature($signature);
```

Cette méthode emet des `SignatureException` si une erreurs survient. Les détails techniques de l'erreurs sont disponible dans [les fichier de log](#système-de-log-pour-les-développeurs). 

Voilà

### Avec les commandes (CRON)

la commande `

```bash 
php vendor/unicaen/signature/bin/unicaen-signature.php signature:update-simplesignature-all
```
Exemple de sortie : 
```text
 ---- -------------------------------------------------- ------------------------- ----------- ----------- ------------------ -------------------------------- 
id   label                                              status                    Parapheur   Remote ID   doc                msg
 ---- -------------------------------------------------- ------------------------- ----------- ----------- ------------------ -------------------------------- 
17   Document d'exemple à signer (Test 669001261482f)   En attente de signature   esup        51638       foo-document.pdf   Fait (En attente de signature)
 ---- -------------------------------------------------- ------------------------- ----------- ----------- ------------------ -------------------------------- 
```

## Supprimer une signature

### Via le code

```php
$signatureService->deleteSignature(
    Signature $signature, // La signature
    bool $flush = true, // Enregistrement en BDD
    bool $removedoc = true // Suppression du document
)
```

Le service `SignatureService` permet de supprimer une signature :
- Simple suppression hors parapheur, dans la BDD (Brouillon)
- Suppression dans le prapheur, puis dans BDD
- Suppression du document


## Evénements

Vous pouvez surveiller les changements d'état des signature via les événements, plusieurs événements sont diffusé lors de la *vie* d'une signature : 

 - `SignatureConstants::EVENT_SIGNATURE_SEND` Signature envoyée au parapheur
 - `SignatureConstants::EVENT_SIGNATURE_SIGNED` Signature signée (Terminée)
 - `SignatureConstants::EVENT_SIGNATURE_REJECTED` Signature rejetée (Terminée)

> L'événement contient l'identifiant de la signature

Vous pouvez également surveiller plus finement la signature en écoutant les événements des signataires
 - `SignatureConstants::EVENT_RECIPIENT_ACCEPTED` Le signataire a signé
 - `SignatureConstants::EVENT_RECIPIENT_REJECTED` Le signataire a rejeté

> L'événement contient l'identifiant du SignatureReipient
 
### Intercepter les événements dans Module.php

Exemple : 

```php
<?php
// module/Application/src/Module.php
namespace Application;

use Laminas\ModuleManager\ModuleManager;
use Laminas\Mvc\ModuleRouteListener;
use Laminas\Mvc\MvcEvent;
use Laminas\Http\Request as HttpRequest;
use Laminas\Config\Factory as ConfigFactory;
use Laminas\Session\SessionManager;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Stdlib\Glob;
use UnicaenSignature\Service\SignatureService;
use UnicaenSignature\Utils\SignatureConstants;

class Module
{
    private $serviceContainer;

    public function onBootstrap(MvcEvent $e)
    {
        $this->serviceContainer = $e->getApplication()->getServiceManager();
        // ...
    }

    public function onSignatureChange($e)
    {
        // Votre code ici
        /** @var SignatureService $signatureService */
        $signatureService = $this->serviceContainer->get(SignatureService::class);

        $signatureService->getLoggerService()->info('EVENT INTERCEPTED ' . print_r($e, true));
    }

    // FIX : ZendFramework 3
    public function init(ModuleManager $manager)
    {
        $manager->getEventManager()->getSharedManager()->attach('*', SignatureConstants::EVENT_SIGNATURE_SIGNED, function ($e) {
            $this->onSignatureChange($e);
        });
    }
    // ...
}
```


## Système de log pour les développeurs

UnicaenSignature permet de tracer les opérations effectuées par le module en utilisant **Monolog**, vous pouvez l'activer dans la configuration :

```php
<?php
// config/autoload/signature.local.php
return [
    'unicaen-signature' => [
        'logger' => [
            ///////////////////////////////////////
            // Activation d'un logger autonome
            'enable'          => true, // Actif
            'level'           => \Monolog\Logger::DEBUG, // Niveau de log
            'file'            => __DIR__ . '/../../logs/signature.log', // Fichier d'écriture
            'file_permission' => 0666,

            ///////////////////////////////////////
            /// Sortie standard (pour le développement le built-in serveur)
            'stdout'          => false,

            ///////////////////////////////////////
            /// Logger complémentaire (celui de l'application utilisant le module)
            /// -> implementation de LoggerInterface (ex: Monolog)
            'customLogger' => null
            //'customLogger'    => 'Logger' // customLogger (LoggerInterface) 
        ],
```

Le fichier `logs/signature.log` doit pouvoir être créé/écrit, il contiendra des logs détaillés selon le niveau de log définit dans `level`.


### Ajouter les log de UnicaenSignature à vos logs (Monolog)

La clef `customLogger` vous permet de renseigner votre service de log si vous en utilisez un (un `Logger` de **Monolog**).

> UnicaenSignature accédera à votre Logger en utilisant le container de service :
> ```php
> $container->get($customLogger);
> ```


### Archiver les échanges avec le parapheur

Vous pouvez également archiver les échanges avec le parapheur avec l'option `archive_exchange` (Si l'implementation du parapheur la prend en charge).

```bash
mkdir -P logs/parapheur/esup
```

Exemple pour ESUP :

```php
<?php
// config/autoload/unicaen-signature.local.php 
use Psr\Container\ContainerInterface;

return [
    /**  **/
    'unicaen-signature' => [
        // ...
        // Configuration des parafeurs numérique
        'letterfiles'            => [
            /************/
            [
                // Nom visible côté applicatif
                'label' => 'ESUP',

                // ...

                // [DEV] Emplacement où sont archivé les échanges de données avec le parapheur
                'archive_exchange' => __DIR__.'/../../logs/parapheur/esup',

                // ...
            ]
        ]
    ]
];
```

On obtiens un fichier **exchange_YYYY-MM-DD_H-M-S.txt** qui ressemble à ça : 

```test
URL ------------------------------ 'https://signature-pp.unicaen.fr/ws/signrequests/new'

SEND ---------------------------
{
    "recipientsEmails": [
        "antony.lecourtes@unicaen.fr",
        "stephane.bouvry@unicaen.fr"
    ],
    "allSignToComplete": true,
    "comment": "Test fait le 2024-07-11 15:58:30",
    "title": "Document d'exemple \u00e0 signer (Test 669001261482f)",
    "signType": "pdfImageStamp",
    "createByEppn": "bouvry@unicaen.fr",
    "multipartFiles": {
        "name": "\/home\/bouvry\/Projects\/Unicaen\/UnicaenLib\/signature-test\/config\/autoload\/..\/..\/data\/signature\/foo-document.pdf",
        "mime": "application\/pdf",
        "postname": "multipartFiles"
    }
}

RESULT ---------------------------
51638
```

Cela permet d'analyse ce que le module à envoyé, et ce qu'il a reçu.

> Attention, TOUTES les transactions avec ESUP seront archivées avec les données brutes reçu du parapheur. Cette option n'est a utiliser que pour auditer un bug ou pour le développement

