# Développer une intégration d'un Parapheur

Vous pouvez utiliser un parafeur interne.

## configuration

Dans `config/autoload/unicaen-signature.local.php`, ajoutez dans la configuration des parapheurs : 

```php
<?php
// config/autoload/unicaen-signature.local.php

use Psr\Container\ContainerInterface;

return [
    'unicaen-signature' => [
        // ...
        'letterfiles'            => [
            /// ESUP
            [
                // Nom visible côté applicatif
                'label' => 'PARAPHEUR DEV',

                // Code/Clef (unique)
                'name'  => 'parapheur-dev',

                'description' => 'Parapheur à intégrer',

                // Utilisé par défaut
                'default'     => false,

                // Classe
                'class'       => \UnicaenSignature\Strategy\Letterfile\Esup\EsupLetterfileStrategy::class,

                // Niveaux de signature disponible
                'levels'      => [
                    \UnicaenSignature\Utils\SignatureConstants::VISA_HIDDEN => 'hidden',
                    \UnicaenSignature\Utils\SignatureConstants::VISA_VISUAL => 'visa',
                    \UnicaenSignature\Utils\SignatureConstants::SIGN_VISUAL => 'pdfImageStamp',
                    \UnicaenSignature\Utils\SignatureConstants::SIGN_CERTIF => 'certSign',
                    \UnicaenSignature\Utils\SignatureConstants::SIGN_EIDAS  => 'nexuSign',
                ],

                // Configuration du parapheur
                'config'      => [
                    // ESUP configuration
                    'param1'           => "Valeur 1",

                    // Créateur
                    'paramX' => 'Valeur X',
                ]
            ],   
        ]
    ]
    // ...
];
```

## Classe du parapheur

## Mode DEBUG

## Tracer les échanges