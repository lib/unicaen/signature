# Interfaces UnicaenSignature

## Installation

### Ajout des privilèges

Pour utiliser les éléments de l'interface, Vous devez disposer des privilèges Unicaen, voici ceux utilisés par le module :

```sql
INSERT INTO UNICAEN_PRIVILEGE_CATEGORIE (
    CODE,
    LIBELLE,
    NAMESPACE,
    ORDRE
)
values (
           'signature',
           'UnicaenSignature - Gestion des signatures',
           'UnicaenSignature\Provider\SignaturePrivileges',
           1101
       )
    ON CONFLICT (CODE) DO
UPDATE SET
    LIBELLE=excluded.LIBELLE,
    NAMESPACE=excluded.NAMESPACE,
    ORDRE=excluded.ORDRE;

WITH d(code, lib, ordre) AS (
    SELECT 'SIGNATURE_INDEX', 'Liste des signatures', 1 UNION
    SELECT 'SIGNATURE_DELETE', 'Suppression des signatures', 2 UNION
    SELECT 'SIGNATURE_CREATE', 'Création de signature', 3 UNION
    SELECT 'SIGNATURE_SYNC', 'Synchronisation de signature', 5 UNION
    SELECT 'SIGNATURE_ADMIN', 'Accès à l''interface d''administration', 6 UNION
    SELECT 'SIGNATURE_ADMIN_CONFIG', 'Configuration des processus métier', 5
)
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
         JOIN unicaen_privilege_categorie cp ON cp.CODE = 'mail'
    ON CONFLICT (CATEGORIE_ID, CODE) DO
UPDATE SET
    LIBELLE=excluded.LIBELLE,
    ORDRE=excluded.ORDRE;

insert into unicaen_privilege_privilege_role_linker
    (role_id, privilege_id)
    (select role.id, privilege.id from unicaen_utilisateur_role role,
                                       unicaen_privilege_privilege privilege
                                           join unicaen_privilege_categorie cp on privilege.categorie_id = cp.id
     where role.role_id = 'Admin_tech'
       and cp.code in('signature'))
    on conflict do nothing;
```

### Activer les ASSETS (UI)

Si vous utilisez les interfaces du modules, vous devez rendre accessible les scripts JS/CSS à l'url `/unicaen/signature`, le plus simple est de passer par un lien symbolique :

```bash
cd public/unicaen
ln -s ../../vendor/unicaen/signature/public/dist signature
```
