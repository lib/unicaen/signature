import {defineConfig, splitVendorChunkPlugin} from 'vite'
import vue from '@vitejs/plugin-vue'
import {viteStaticCopy} from 'vite-plugin-static-copy'
import {resolve} from 'path'

console.log("UNICAEN-SIGNATURE");

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),
        splitVendorChunkPlugin(),
        viteStaticCopy({
            targets: [
                {
                    src: 'src/styles/fontello',
                    dest: ''
                }
            ]
        })
    ],
    build: {
        outDir: "./dist",
        sourcemap: false,
        emptyOutDir: true,
        manifest: true,
        minify: true,

        rollupOptions: {
            input: {
                signaturescss: resolve(__dirname, 'src/signature.scss'),
                signatureflows: resolve(__dirname, 'src/SignatureFlows.js'),
                triggerprocess: resolve(__dirname, 'src/TriggerProcess.js')
            },
            output: {
                chunkFileNames: 'vendor.js'
            },
        },
    },
    resolve: {
        alias: {
            'vue': '/node_modules/vue/dist/vue.runtime.esm-browser.js'
        }
    }
})
