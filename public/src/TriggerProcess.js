import { createApp } from 'vue'

import SignatureFlows from './views/TriggerProcess.vue'

let elemId = '#triggerprocess';
let elemDatas = document.querySelector(elemId);

const app = createApp(SignatureFlows, {
    "entrypoint": elemDatas.dataset.entrypoint
});

app.mount(elemId);