import { createApp } from 'vue'

import SignatureFlows from './views/SignatureFlows.vue'

let elemId = '#signatureflows';
let elemDatas = document.querySelector(elemId);

const app = createApp(SignatureFlows, {
    "entrypoint": elemDatas.dataset.entrypoint
});

app.mount(elemId);