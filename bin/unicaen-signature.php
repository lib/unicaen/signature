<?php

use Laminas\Mvc\Application;
use Laminas\Stdlib\ArrayUtils;

ini_set('display_errors', true);
chdir(__DIR__);

$previousDir = '.';

while (!file_exists('config/application.config.php')) {
    $dir = dirname(getcwd());

    if ($previousDir === $dir) {
        throw new RuntimeException(
            'Unable to locate "config/application.config.php": ' .
            'is DoctrineModule in a subdir of your application skeleton?'
        );
    }

    $previousDir = $dir;
    chdir($dir);
}


if (is_readable('init_autoloader.php')) {
    include_once 'init_autoloader.php';
} elseif (file_exists(__DIR__ . '/../vendor/autoload.php')) {
    include_once __DIR__ . '/../vendor/autoload.php';
} elseif (file_exists(__DIR__ . '/../../../autoload.php')) {
    include_once __DIR__ . '/../../../autoload.php';
} else {
    throw new RuntimeException('Error: vendor/autoload.php could not be found. Did you run php composer.phar install?');
}

$console = new \Symfony\Component\Console\Application();
$appConfig = include 'config/application.config.php';
if (file_exists('config/development.config.php')) {
    $appConfig = ArrayUtils::merge($appConfig, include 'config/development.config.php');
}
$app = Application::init($appConfig);

$commands = [];

$paths = $app->getConfig()['console_commands'];

function scanCommandFiles(string $namespace, string $dir){
    global $app;
    $output = [];
    $re = '/.*Command\.php/m';

    if( !file_exists($dir) ){
        echo ("[WARNING] Command path : Le dossier '$dir' n'existe pas\n");
        return [];
    }

    $scan = scandir($dir);
    foreach ($scan as $key => $value) {
        if( !in_array($value, ['.', '..']) ){
            if( is_dir($dir.DIRECTORY_SEPARATOR.$value) ){

            } else {
                if( preg_match($re, $value, $matches) ){
                    $class = substr($value, 0, strlen($value)-4);
                    $reflector = new ReflectionClass($namespace.$class);
                    if( $reflector->isSubclassOf(\Symfony\Component\Console\Command\Command::class) ){
                        $instance = $reflector->newInstanceArgs([$app->getServiceManager()]);
                        if( property_exists($instance, 'disabled') ) {
                            continue;
                        }
                        $result[] = $instance;
                    }
                }
            }
        }
    }
    return $result;
}

foreach ($paths as $namespace=>$path) {
    // On ne garde que UnicaenSignature ici
    // TODO Trouver un système plus propre en allant voir sur Doctrine comment c'est fait
    if( $namespace == "UnicaenSignature\Command\\" ) {
        $commands = scanCommandFiles($namespace, $path);
        $console->addCommands($commands);
    }
}
exit($console->run());