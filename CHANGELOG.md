# CHANGELOG

## 1.0.4 (03/02/25)

### Corrections

- Correction initialisation propriété label à null sur Process et ProcessStep

## 1.0.3 (30/01/25)

### Général

- Correction typage getter et setter de Process et de ProcessStep

## 1.0.2 (30/01/25)

### Général

- Suppression d'une contrainte de référence entre les entités processStep et signatureFlowStep, empêchant la suppression d'une étape d'un circuit de signature si le circuit avait déjà été utilisé pour faire des signatures dans l'application. 

## 16 octobre 2024

### Général
 
 - Ajout de tests unitaires (intégrés dans Gitlab au push)
 - Traitement des commentaires dans `SignRequestInfo`
 - Ajout des messages de refus : mise à jour du champ information - non testé* - sur `SignatureRecipient` et ajout d'un champ pour le message d'erreur dans `Signature::refused_text`

```sql
ALTER TABLE unicaen_signature_signature ADD refused_text TEXT DEFAULT NULL;

```

### ESUP
 - **Prise en charge des commentaires/Post-its ESUP** Cependant, la version actuelle d'ESUP ne permet pas de déterminer l'auteur du Post-it, donc ils seront traité de façon global
 - Les commentaires/Post-its sont ignorés pour le moment