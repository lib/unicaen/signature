<?php

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Regex;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;
use UnicaenSignature\Controller\AdministrationController;
use UnicaenSignature\Controller\Factory\AdministrationControllerFactory;
use UnicaenSignature\Controller\InternalLetterfileController;
use UnicaenSignature\Controller\InternalLetterfileControllerFactory;
use UnicaenSignature\Controller\ProcessController;
use UnicaenSignature\Controller\ProcessControllerFactory;
use UnicaenSignature\Controller\SignatureController;
use UnicaenSignature\Controller\SignatureControllerFactory;
use UnicaenSignature\Provider\SignaturePrivileges;
use UnicaenSignature\Service\InternalLetterfileService;
use UnicaenSignature\Service\InternalLetterfileServiceFactory;
use UnicaenSignature\Service\LetterfileService;
use UnicaenSignature\Service\LetterfileServiceFactory;
use UnicaenSignature\Service\LoggerService;
use UnicaenSignature\Service\LoggerServiceFactory;
use UnicaenSignature\Service\ProcessService;
use UnicaenSignature\Service\ProcessServiceFactory;
use UnicaenSignature\Service\SignatureConfigurationService;
use UnicaenSignature\Service\SignatureConfigurationServiceFactory;
use UnicaenSignature\Service\SignatureService;
use UnicaenSignature\Service\SignatureServiceFactory;
use UnicaenSignature\Strategy\Notification\NotificationLogDebugStrategy;
use UnicaenSignature\Strategy\Notification\NotificationLogDebugStrategyFactory;
use UnicaenSignature\View\Helpers\MomentFactory;
use UnicaenSignature\View\Helpers\UiFactory;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => SignatureController::class,
                    'action'     => [
                        'download',
                        'home',
                        'letterfiles',
                        'letterfileWorkflows',
                        'myDocuments',
                        'process',
                        'processNextStep',
                        'signatures',
                        'previewDocument',
                        'triggerProcess',
                    ],
                    'privileges' => [
                        SignaturePrivileges::SIGNATURE_INDEX,
                    ],
                    'roles'      => []
                ],

                ///////////////////////////////////////////////////////////
                /// ADMINISTRATION
                [
                    'controller' => AdministrationController::class,
                    'action'     => [
                        'index',
                        'letterfile',
                        'simpleSignature',
                        'simpleSignatureDelete',
                        'simpleSignatureSend',
                        'simpleSignatureStatus',
                        'simpleSignatureNew',
                        'process',
                        'processConfig',
                        'processDelete',
                        'processNew',
                        'processSend',
                        'processStatus',
                        'processTrigger',
                        'signatureflows',
                        'signatureflowsUi',
                    ],
                    'privileges' => [
                        SignaturePrivileges::SIGNATURE_INDEX,
                    ],
                    'roles'      => []
                ],
                [
                    'controller' => AdministrationController::class,
                    'action'     => [
                        'process',
                    ],
                    'privileges' => [
                        SignaturePrivileges::SIGNATURE_ADMIN_CONFIG,
                    ],
                    'roles'      => []
                ],
                ///////////////////////////////////////////////////////////

                [
                    'controller' => SignatureController::class,
                    'action'     => [
                        'myDocuments',
                    ],
                    'roles'      => ['user']
                ],

                [
                    'controller' => SignatureController::class,
                    'action'     => [
                        'status'
                    ],
                    'privileges' => [
                        SignaturePrivileges::SIGNATURE_SYNC,
                    ],
                    'roles'      => []
                ],
                [
                    'controller' => SignatureController::class,
                    'action'     => [
                        'new',
                        'send'
                    ],
                    'privileges' => [
                        SignaturePrivileges::SIGNATURE_CREATE,
                    ],
                    'roles'      => []
                ],
                [
                    'controller' => SignatureController::class,
                    'action'     => [
                        'delete',
                    ],
                    'privileges' => [
                        SignaturePrivileges::SIGNATURE_DELETE,
                    ],
                    'roles'      => []
                ],

                ////////////////////////////////////////////////////////////////////
                /// PROCESS
                [
                    'controller' => ProcessController::class,
                    'action'     => [
                        'previewDocument',
                    ],
                    'privileges' => [
                        SignaturePrivileges::SIGNATURE_INDEX,
                    ],
                    'roles'      => []
                ],
                [
                    'controller' => InternalLetterfileController::class,
                    'action'     => [
                        'visa',
                        'document',
                        'index',
                    ],
                    'roles'      => []
                ]
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-signature' => [
                'type'          => Literal::class,
                'options'       => [
                    'route'    => '/signature',
                    'defaults' => [
                        'controller' => SignatureController::class,
                        'action'     => 'home',
                    ],
                ],
                'may_terminate' => true,

                ///////////////////////////////////////////////////////////////////////////////////////////
                ///////////////////////////////////////////////////////////////////////////////////////////
                /// ADMINISTRATION
                ///////////////////////////////////////////////////////////////////////////////////////////
                ///////////////////////////////////////////////////////////////////////////////////////////

                'child_routes' => [
                    'administration' => [
                        'type'          => Literal::class,
                        'options'       => [
                            'route'    => '/administration',
                            'defaults' => [
                                'controller' => AdministrationController::class,
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [

                            ////////////////////////////////////////////////////////////////////////////////////////////
                            /// PROCESSUS
                            ////////////////////////////////////////////////////////////////////////////////////////////
                            'process' => [
                                'type'    => Literal::class,
                                'options' => [
                                    'route'    => '/process',
                                    'defaults' => [
                                        'controller' => AdministrationController::class,
                                        'action'     => 'process',
                                    ],
                                ],
                            ],

                            'process-config' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '/process-config/:id',
                                    'defaults' => [
                                        'controller' => AdministrationController::class,
                                        'action'     => 'processConfig',
                                    ],
                                ],
                            ],

                            'process-delete' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '/process-delete/:id',
                                    'defaults' => [
                                        'controller' => AdministrationController::class,
                                        'action'     => 'processDelete',
                                    ],
                                ],
                            ],

                            'process-new' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '/process-new/:id',
                                    'defaults' => [
                                        'controller' => AdministrationController::class,
                                        'action'     => 'processNew',
                                    ],
                                ],
                            ],

                            'process-trigger' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '/process-trigger/:id',
                                    'defaults' => [
                                        'controller' => AdministrationController::class,
                                        'action'     => 'processTrigger',
                                    ],
                                ],
                            ],

                            ////////////////////////////////////////////////////////////////////////////////////////////
                            /// SIGNATURE FLOW

                            'signatureflowsui'        => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '/signatureflows-ui',
                                    'defaults' => [
                                        'action' => 'signatureflowsUi'
                                    ]
                                ]
                            ],
                            'signatureflows'          => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '/signatureflows-api',
                                    'defaults' => [
                                        'action' => 'signatureflows'
                                    ]
                                ]
                            ],


                            ////////////////////////////////////////////////////////////////////////////////////////////
                            /// SIMPLE SIGNATURE
                            'simple-signature'        => [
                                'type'    => Literal::class,
                                'options' => [
                                    'route'    => '/simple-signature',
                                    'defaults' => [
                                        'controller' => AdministrationController::class,
                                        'action'     => 'simpleSignature',
                                    ],
                                ],
                            ],
                            'simple-signature-new'    => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '/simple-signature-new/:key',
                                    'defaults' => [
                                        'controller' => AdministrationController::class,
                                        'action'     => 'simpleSignatureNew',
                                    ],
                                ],
                            ],
                            'simple-signature-send'   => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '/simple-signature-send/:id',
                                    'defaults' => [
                                        'controller' => AdministrationController::class,
                                        'action'     => 'simpleSignatureSend',
                                    ],
                                ],
                            ],
                            'simple-signature-status' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '/simple-signature-status/:id',
                                    'defaults' => [
                                        'controller' => AdministrationController::class,
                                        'action'     => 'simpleSignatureStatus',
                                    ],
                                ],
                            ],
                            'simple-signature-delete' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '/simple-signature-delete/:id',
                                    'defaults' => [
                                        'controller' => AdministrationController::class,
                                        'action'     => 'simpleSignatureDelete',
                                    ],
                                ],
                            ],

                            ////////////////////////////////////////////////////////////////////////////////////////////
                            /// PARAPHEUR
                            'letterfile'              => [
                                'type'    => Literal::class,
                                'options' => [
                                    'route'    => '/letterfile',
                                    'defaults' => [
                                        'controller' => AdministrationController::class,
                                        'action'     => 'letterfile',
                                    ],
                                ],
                            ],
                        ]
                    ],

                    //////////////////////////////////////////////////////////////////////// < PROCESS
                    'process'        => [
                        'type'          => Literal::class,
                        'options'       => [
                            'route'    => '/process',
                            'defaults' => [
                                'controller' => ProcessController::class,
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,

                        'child_routes' => [

                            'document' => [
                                'type'    => Regex::class,
                                'options' => [
                                    'regex'    => '/document-(?<id>\d+)-preview?',
                                    'spec'     => '/document-%id%-preview',
                                    'defaults' => [
                                        'action' => 'previewDocument'
                                    ]
                                ],
                            ],
                        ]
                    ],

                    /////////////////////////////////////////////////////////////////////// < PARAPHEUR INTERNE (VISEUR)
                    'internal'       => [
                        'type'          => Segment::class,
                        'options'       => [
                            'route'    => '/internal',
                            'defaults' => [
                                'action'     => 'index',
                                'controller' => InternalLetterfileController::class
                            ]
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'visa'     => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '/visa/:key',
                                    'defaults' => [
                                        'action' => 'visa'
                                    ]
                                ]
                            ],
                            'document' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '/document/:key',
                                    'defaults' => [
                                        'action' => 'document'
                                    ]
                                ]
                            ]
                        ]
                    ],
                    //////////////////////////////////////////////////////////////////////// > PROCESS
                    'signatures'     => [
                        'type'          => Segment::class,
                        'options'       => [
                            'route'    => '/signatures',
                            'defaults' => [
                                'action' => 'signatures'
                            ]
                        ],
                        'may_terminate' => true,
                        'child_routes'  => [
                            'preview-document' => [
                                'type'    => Segment::class,
                                'options' => [
                                    'route'    => '/preview-document/:id',
                                    'defaults' => [
                                        'action' => 'previewDocument'
                                    ]
                                ]
                            ]
                        ]
                    ],

                    'download'       => [
                        'type'    => Segment::class,
                        'options' => [
                            'route'    => '/download/:id',
                            'defaults' => [
                                'action' => 'download'
                            ]
                        ]
                    ],

                    'my-documents'   => [
                        'type'    => Segment::class,
                        'options' => [
                            'route'    => '/my-documents',
                            'defaults' => [
                                'action' => 'myDocuments'
                            ]
                        ]
                    ]
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            InternalLetterfileService::class     => InternalLetterfileServiceFactory::class,
            LetterfileService::class             => LetterfileServiceFactory::class,
            LoggerService::class                 => LoggerServiceFactory::class,
            NotificationLogDebugStrategy::class  => NotificationLogDebugStrategyFactory::class,
            SignatureConfigurationService::class => SignatureConfigurationServiceFactory::class,
            SignatureService::class              => SignatureServiceFactory::class,
            ProcessService::class                => ProcessServiceFactory::class,
        ],
    ],

    'controllers' => [
        'factories' => [
            AdministrationController::class     => AdministrationControllerFactory::class,
            InternalLetterfileController::class => InternalLetterfileControllerFactory::class,
            SignatureController::class          => SignatureControllerFactory::class,
            ProcessController::class            => ProcessControllerFactory::class,
        ],
    ],

    'form_elements' => [
        'factories' => [],
    ],

    'hydrators' => [
        'factories' => [],
    ],

    'view_helpers' => [
        'factories' => [
            'signatureUi'   => UiFactory::class,
            'signatureDate' => MomentFactory::class
            // 'calendrier' => CalendrierViewHelper::class,
        ],
    ]
];
