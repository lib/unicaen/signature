<?php

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;

return [
    'console_commands' => [
        'UnicaenSignature\Command\\' => realpath(__DIR__ . '/../src/Command'),
    ],

    'doctrine' => [
        'driver' => [
            'orm_default' => [
                'class' => MappingDriverChain::class,
                'drivers' => [
                    'UnicaenSignature\Entity\Db' => 'orm_default_annotation_driver',
                ],
            ],
            'orm_default_annotation_driver' => [
                'class' => AnnotationDriver::class,
                'paths' => [
                    __DIR__ . '/../src/Entity/Db/',
                ],
            ],
        ],

    ],

    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],

    'view_helpers' => [
        'aliases' => [
        ],
        'factories' => [
        ],
    ],

    'public_files' => [
        'stylesheets' => [
            '10000_signature_fontello' => 'css/unicaen-signature.css',
        ],
    ],

    'unicaen-signature' => [



        'get_recipients_methods' => [
            [   'key' => \UnicaenSignature\Utils\SignatureConstants::GET_RECIPIENTS_METHOD_FIXED,
                'label' => 'Destinataires fixes',
                'description' => 'Destinataires nommés explicitement',
                'methods' => 'fixed',
                'options' => [
                    [
                        'key' => 'recipients',
                        'label' => "Destinataires",
                        'type' => 'recipients',
                        'editables' => false,
                        'values' => [
                        ],
                        'defaultValue' => [
                            ['firstname' => 'Stéphane', 'lastname' => 'Bouvry', 'email' => 'stephane.bouvry@unicaen.fr']
                        ]
                    ]
                ],
                'methods_on_create' => null,
                'getRecipients' => null
            ]
        ],

        ////////////////////////////////////////////////////////////
        /// DEVELOPPEUR
        /// Note : Permet de gérer les logs et le niveau de log
        'logger' => [
            'enable' => false,
            'level' => \Monolog\Logger::ERROR,
            'file' => '/tmp/unicaen-signature.log',
            'stdout' => false
        ],

        'vite_mode' => 'prod',

        'documents_path' => '/tmp',
        'documents_mimes' => [
            'application/pdf' => 'pdf'
        ],

        ////////////////////////////////////////////////////////////
        /// Niveau de signatures disponibles (tous parapheurs confondus)
        ///
        /// [
        //      'label' => Intitulé affiché
        //      'key' => Clef unique utilisé dans la configuration des parapheurs
        //               pour indiquer si ce type de signature est disponible.
        //      'used' => Indique si ce type de signature est utilisable
        //      'description' => Description du type de signature (doit être claire
        //                       pour l'utilisateur
        //      'log_message' => Message loggué via la fonction sprintf. La chaîne
        //                       doit contenir 2 '%s', le permier sera les noms/prénom
        //                       de la personne signataire, le deuxième '%s' sera
        //                       l'identifiant du document.
        //      'letterfile_key' => ???
        //  ],
        ///
        ///
        ////////////////////////////////////////////////////////////
        'signature_levels' => [
            \UnicaenSignature\Utils\SignatureConstants::VISA_HIDDEN => [
                'label' => 'Visa caché',
                'used' => true,
                'description' => 'Visa applicatif, pas de trace dans le document',
                'log_message' => "La personne '%s' a validée le document '%s'",
                'letterfile_key' => 'internal'
            ],
            \UnicaenSignature\Utils\SignatureConstants::VISA_VISUAL => [
                'label' => 'Visa visuel',
                'used' => true,
                'description' => 'Visa visuel. Nom, prénom et date appliqués sur le document.',
                'log_message' => "La personne '%s' a marquée comme vu le document '%s'",
                'letterfile_key' => 'external'
            ],
            \UnicaenSignature\Utils\SignatureConstants::SIGN_VISUAL => [
                'label' => 'Signature calligraphique',
                'used' => true,
                'description' => 'Signature calligraphique. Signature au format image. Appliquée sur le document.',
                'log_message' => "La personne '%s' a signée le document '%s'",
                'letterfile_key' => 'external'
            ],
            \UnicaenSignature\Utils\SignatureConstants::SIGN_CERTIF => [
                'label' => 'Signature par certificat (PKCS12)',
                'used' => true,
                'description' => 'Signature certifiée avec un keystore/certificat via un magasin de clés au format PKCS12. Appliquée sur le document.',
                'log_message' => "La personne '%s' a signée et certifiée le document '%s'",
                'letterfile_key' => 'external'
            ],
            \UnicaenSignature\Utils\SignatureConstants::SIGN_EIDAS  => [
                'label' => 'Signature par clef cryptographique',
                'used' => true,
                'description' => "Signature par clef cryptographique via clé usb (eIDAS) qui contient un certificat. Certificat qui peut être personnel ou au titre de l'établissement (cachet établissement). Appliquée sur le document.",
                'log_message' => "La personne '%s' a signée et certifiée le document '%s'",
                'letterfile_key' => 'external'
            ],
        ],

        // Configuration des parapheurs numérique
        'letterfiles' => [
            // configuré dans l'application ./config/autoload/signature.local.php
        ]
    ]
];