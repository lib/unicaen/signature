![Test unitaires](https://git.unicaen.fr/lib/unicaen/signature/badges/master/pipeline.svg)

# Guide de développement

Le module signature permet d'intégrer à votre application un système de signature numérique. Cela se fait via une application tiers (un **parapheur numérique**).

## Installation de base

### Ajouter la dépendence vie composer

```bash
composer require unicaen/signature
```

### Configuration de base

Des fichiers de configuration `.dist` sont disponibles dans `vendor/unicaen/signature/config` avec quelques exemple de configuration : 

```bash
# Copier le modèle de base de configuration
cp vendor/unicaen/signature/config/unicaen-signature.local.php.dist config/autoload/unicaen-signature.local.php
```

Adapter la configuration selon votre usage, par défaut, le fichier de configuration propose une configuration avec le parapheur ESUP et le parapheur Interne.

 - [Configuration du parapheur ESUP](doc/config/parapheur-esup.md)
 - [Configuration du parapheur INTERNAL](doc/config/parapheur-interne.md)

> Vous pouvez également [Développer un nouveau parapheur](doc/dev/parapheur-dev.md)

### Base de données

Vous devez installer les tables utilisées par le module : 

![Tables du module](doc/dev/database.png)

#### Via les entitées Doctrine

Dans le fichier de configuration de votre application (normalement `config/autoload/global.php`), éditez le `paths` des entitées pour y ajouter les entitées du module **signature** : 

```php
<?php
return array(
    // ...
    'doctrine' => array(
        // ...
        'driver' => array(
            'my_entities' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(
                    // Emplacement des entitées de UnicaenSignature
                    __DIR__ . '/../../vendor/unicaen/signature/src/Entity/Db',
                ),
            ),
        ),
    ),
);
```

Puis mettez à jour le modèle de la base avec les commandes *Doctrine*.

#### Via SQL

Sinon, utiliser directement SQL [Script SQL pour créer les tables](doc/dev/database-install-sql.md) :

### Activer le module

Ajoutez **UnicaenSignature** dans `config/application.config.php` :

```php
<?php
$config = array(
    'modules' => array(
        // ...
        'UnicaenSignature'
    ),
    // ...
);
// ...
return $config;

```

### Dossier des documents

Vérifiez que le dossier d'écriture des documents à signer est bien accessible en écriture. C'est le dossier indiqué dans la clef `documents_path` (par défaut `./data/documents/signature`)

A cette étape, le module est opérationnel et permet d'utiliser ces services pour les documents de votre application.

## Utilisation du module

- [x] [Usage simple pour signer des documents](doc/dev/usage.md)
- [x] [Utilisation en ligne de commande](doc/dev/commands.md)
- [ ] [Processus de signature](doc/dev/process.md)
- [ ] [Interfaces du module](doc/dev/ui.md)
- [ ] [Développer un parapheur](doc/dev/parapheur-dev.md)

## TODO
- [ ] Documentation : Processus
- [ ] Documentation : Ui du module
- [ ] Documentation : Développer un parapheur
- [ ] Système de rappel des destinataires pour le parapheur interne
- [ ] Commande de rappel pour les observateurs
- [ ] Suppression d'un document signé (avec restauration du document d'origine)
